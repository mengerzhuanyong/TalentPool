'use strict';
import { action, observable, configure, runInAction, toJS } from 'mobx'

configure({ enforceActions: 'observed' });

export default class BaseStore {

    constructor() {
        this.loading = false;
        this.error = { isError: '', errorMsg: '' };
    }

    @observable loading = false;
    @observable error;

    @action
    getJSDataSources(dataSources) {
        return toJS(dataSources);
    }

    @action
    getRequest = async (url, query) => {
        this.loading = true
        const result = await Services.get(url, query)
        runInAction(() => {
            this.loading = false
        })
        return result
    }

    @action
    postRequest = async (url, data) => {
        this.loading = true
        const result = await Services.post(url, data)
        runInAction(() => {
            this.loading = false
        })
        return result
    }


}