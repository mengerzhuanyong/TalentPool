'use strict';
import { observable, action, computed, runInAction } from 'mobx'
import BaseStore from '../base/BaseStore'
import AppModel from '../models/AppModel';

/**
 * @app状态和信息
 */
export default class AppStore extends BaseStore {

    constructor(params) {
        super(params)
        this.appModel = new AppModel()
    }


    @action
    changeNetworkState = (state) => {
        this.appModel.networkState = state
    }

    @action
    changeAppState = (state) => {
        this.appModel.appState = state
    }

    @action
    setDeviceInfo = (info) => {
        this.appModel.deviceInfo = info
    }

}

