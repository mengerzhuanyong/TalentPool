'use strict';
import { autorun, observable, action, computed, runInAction, toJS } from 'mobx'
import BaseStore from '../base/BaseStore'
import ExModel from '../models/ExModel';

export default class ExStore extends BaseStore {

    constructor(params) {
        super(params)
        global.loginState = 0
        this.dataSource = []
        this.exModel = new ExModel()
    }

    @observable dataSource = []

    @computed
    get getDataSource() {
        return toJS(this.dataSource) // 将Mobx对象转换为js对象
    }

    @action
    requestDataSource = async (url, query) => {
        const result = await this.getRequest(url, query)
        if (result.success) {
            runInAction(() => {
                if (query.page === 1) {
                    let data = []
                    result.data.forEach(element => {
                        element['sel'] = false
                        data.push(element)
                    });
                    this.dataSource = data
                } else if (query.page > 1) {
                    this.dataSource = this.dataSource.concat(result.data)
                }
            })
        }
        return result
    }

    @action
    setNavTitle = (title) => {
        this.exModel.title = title
    }

    @action
    setDataBackColor = (index) => {
        // let data = this.dataSource.slice()
        // data[index]['sel'] = true
        // this.dataSource = data

        this.dataSource[index]['sel'] = true
    }

}


