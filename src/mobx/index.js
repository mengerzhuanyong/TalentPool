
import AppStore from './stores/AppStore';
import ExStore from './stores/ExStore'

export default {
    appStore: new AppStore(),
    exStore: new ExStore()
};