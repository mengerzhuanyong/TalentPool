'use strict';
import { observable, action, computed, runInAction } from 'mobx'
import BaseModel from '../base/BaseModel'

export default class AppModel extends BaseModel {

    constructor(params) {
        super(params)
        this.networkState = null // 网络状态,none,wifi,cellular
        this.appState = null // 前后 后台
        this.deviceInfo = null  // 设备的所有信息
    }

    @observable appState
    @observable networkState
    @observable deviceInfo


}
