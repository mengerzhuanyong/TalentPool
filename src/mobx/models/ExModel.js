'use strict';
import { observable, action, computed, runInAction } from 'mobx'
import BaseModel from '../base/BaseModel'

export default class ExModel extends BaseModel {

    constructor(params) {
        super(params)
        this.title = '导航栏';
    }

    @observable title

}

