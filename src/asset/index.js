'use strict';

export default {

    /**
     * @JSON [动画的JSON文件]
     */
    json_loading: require('./json/loading.json'),


    /**
    * @Common [通用]
    */
    icon_network_err: require('./icon/icon_network_err.png'),
    icon_no_record: require('./icon/icon_no_record.png'),
    icon_nav_left: require('./icon/icon_nav_left.png'),
    icon_arrow_right: require('./icon/icon_arrow_right.png'),

    /**
     * @Toast [轻提示]
     */
    icon_toast_fail: require('./icon/icon_toast_fail.png'),
    icon_toast_loading: require('./icon/icon_toast_loading.png'),
    icon_toast_success: require('./icon/icon_toast_success.png'),
    icon_toast_warn: require('./icon/icon_toast_warn.png'),

    /**
     * @Share [分享]
     */
    icon_share_timeline: require('./icon/icon_share_timeline.png'), // 朋友圈
    icon_share_wechat: require('./icon/icon_share_wechat.png'), // 微信
    icon_share_link: require('./icon/icon_share_link.png'),

    /**
     * @CHAT [聊天组件]
     */
    icon_chat_gift: require('./icon/icon_chat_gift.png'),
    icon_chat_voice: require('./icon/icon_chat_voice.png'),
    json_voice: require('./json/chat_voice.json'),

    /**
    * @TABBAR [Tabbar]
    */
    icon_tabbar_work_bench: require('./tabbar/icon_tabbar_work_bench.png'),
    icon_tabbar_talent_pool: require('./tabbar/icon_tabbar_talent_pool.png'),
    icon_tabbar_work_bench_cur: require('./tabbar/icon_tabbar_work_bench_cur.png'),
    icon_tabbar_talent_pool_cur: require('./tabbar/icon_tabbar_talent_pool_cur.png'),

    /**
     * @ICONS [项目Icon]
     */
    icon_center_play: require('./icon/icon_center_paly.png'),
    icon_center_pause: require('./icon/icon_center_pause.png'),
    icon_bottom_play: require('./icon/icon_bottom_play.png'),
    icon_bottom_pause: require('./icon/icon_bottom_pause.png'),
    icon_enlarge: require('./icon/icon_enlarge.png'),
    icon_micrify: require('./icon/icon_micrify.png'),
    icon_live_refresh: require('./icon/icon_live_refresh.png'),
    icon_live_text: require('./icon/icon_live_text.png'),
    icon_bar_recharge: require('./icon/icon_bar_recharge.png'),
    icon_bar_gift: require('./icon/icon_bar_gift.png'),
    icon_danmu: require('./icon/icon_danmu.png'),
    icon_danmu_no: require('./icon/icon_danmu_no.png'),
    icon_live_up: require('./icon/icon_live_up.png'),

    icon_user_card_yes: require("./icon/icon_user_card_yes.png"),
    icon_user_card_no: require("./icon/icon_user_card_no.png"),
    icon_shop_card: require("./icon/icon_shop_card.png"),

    icon_btn_camera: require('./icon/icon_btn_camera.png'),
    icon_btn_edit: require('./icon/icon_btn_edit.png'),
    icon_btn_qr_code: require('./icon/icon_btn_qr_code.png'),
    icon_btn_scan: require('./icon/icon_btn_scan.png'),
    icon_calendar: require('./icon/icon_calendar.png'),
    icon_camera: require('./icon/icon_camera.png'),
    icon_card_front: require('./icon/icon_card_front.png'),
    icon_card_reverse: require('./icon/icon_card_reverse.png'),
    icon_check_circle: require('./icon/icon_check_circle.png'),
    icon_checked_circle: require('./icon/icon_checked_circle.png'),
    icon_checked: require('./icon/icon_checked.png'),
    icon_close_red: require('./icon/icon_close_red.png'),
    icon_close_white: require('./icon/icon_close_white.png'),
    icon_edit: require('./icon/icon_edit.png'),
    icon_exchange: require('./icon/icon_exchange.png'),
    icon_filter: require('./icon/icon_filter.png'),
    icon_plus: require('./icon/icon_plus.png'),
    icon_present: require('./icon/icon_present.png'),
    icon_qr_code: require('./icon/icon_qr_code.png'),
    icon_recommend: require('./icon/icon_recommend.png'),
    icon_success: require('./icon/icon_success.png'),
    icon_tips_success: require('./icon/icon_tips_success.png'),
    icon_trash: require('./icon/icon_trash.png'),
    icon_upload_camera: require('./icon/icon_upload_camera.png'),
    icon_user_card: require('./icon/icon_user_card.png'),
    icon_user_close: require('./icon/icon_user_close.png'),
    icon_user_exit: require('./icon/icon_user_exit.png'),
    icon_user_plane: require('./icon/icon_user_plane.png'),
    icon_user_plus: require('./icon/icon_user_plus.png'),
    icon_user_recommend: require('./icon/icon_user_recommend.png'),
    icon_work_bench_nav1: require('./icon/icon_work_bench_nav1.png'),
    icon_work_bench_nav2: require('./icon/icon_work_bench_nav2.png'),
    icon_work_bench_nav3: require('./icon/icon_work_bench_nav3.png'),
    icon_work_bench_nav4: require('./icon/icon_work_bench_nav4.png'),
    icon_work_bench_nav5: require('./icon/icon_work_bench_nav5.png'),
    icon_work_bench_nav6: require('./icon/icon_work_bench_nav6.png'),
    icon_work_bench_nav7: require('./icon/icon_work_bench_nav7.png'),
    icon_work_bench_nav8: require('./icon/icon_work_bench_nav8.png'),
    icon_work_bench_nav9: require('./icon/icon_work_bench_nav9.png'),
    icon_work_bench_nav10: require('./icon/icon_work_bench_nav10.png'),
    icon_work_bench_nav11: require('./icon/icon_work_bench_nav11.png'),
    icon_work_bench_nav12: require('./icon/icon_work_bench_nav12.png'),
    icon_work_bench_nav13: require('./icon/icon_work_bench_nav13.png'),




    /**
     * @IMAGES [项目Image]
     */
    img_bg_navbar: require('./img/img_bg_navbar.png'),
    img_mine_back: require('./img/img_mine_back.png'),
    img_mine_bg: require('./img/img_mine_bg.png'),
    img_button_bg1: require("./img/img_button_bg1.png"),

    img_bg_btn: require('./img/img_bg_btn.png'),
    img_bg_message: require('./img/img_bg_message.png'),
    img_bg_popup_title: require('./img/img_bg_popup_title.png'),
    img_bg_talent: require('./img/img_bg_talent.png'),
    img_logo: require('./img/img_logo.png'),
    img_logo_white: require('./img/img_logo_white.png'),


}
