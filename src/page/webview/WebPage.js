/*
 * @author: jiasong 
 * @creation time: 2018-08-03 16:04:58
 */

'use strict';
import React from 'react';
import { View, Text, StyleSheet, WebView } from 'react-native';
import NavigationBar from '../../component/Navigation/NavigationBar';
import Container from '../../component/Container/Container';

export default class WebPage extends React.PureComponent {

    constructor(props) {
        super(props)

    }

    render() {
        const { title, uri } = this.props.navigation.state.params
        return (
            <Container>
                <NavigationBar title={title} />
                <WebView
                    style={styles.webView}
                    source={{ uri }}
                />
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    webView: {

    }
});

