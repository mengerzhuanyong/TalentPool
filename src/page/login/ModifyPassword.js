/**
 * @author jiasong
 * @creationTime 2019-02-19 17:41:28
 */

'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import NavigationBar from '../../component/Navigation/NavigationBar';
import Container from '../../component/Container/Container';
import ImageView from '../../component/Image/ImageView';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Theme from '../../config/themes/Theme';
import Button from '../../component/Touchable/Button'
import LoginRow from '../../component/Form/LoginRow';
import { checkMobile, checkEmpty } from '../../util/Tool';

export default class ModifyPassword extends React.PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            oldPassword: '',
            password: '',
            passwordTwo: '',
        }
    }

    _onPressSubmit = async () => {
        if (!this.cheackDataSource()) {
            return;
        }
        const { oldPassword, password, passwordTwo } = this.state
        alert('注册')
    }

    setDataSource = (key, data, refresh) => {
        if (refresh) {
            let newDataSource = { ...this.state }
            newDataSource[key] = data
            Log.print('标记', newDataSource)
            this.setState({ ...newDataSource })
        } else {
            this.state[key] = data
        }
    }

    cheackDataSource = () => {
        const { oldPassword, password, passwordTwo } = this.state
        if (checkEmpty(oldPassword)) {
            ToastManager.message('原密码填写不正确')
            return false
        }
        if (checkEmpty(password)) {
            ToastManager.message('密码填写不正确')
            return false
        }
        if (password != passwordTwo) {
            ToastManager.message('两次密码填写不正确')
            return false
        }
        return
    }

    render() {

        return (
            <Container style={styles.container}>
                <NavigationBar title={'修改密码'} />
                <KeyboardAwareScrollView
                    style={styles.contentContainer}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                    keyboardShouldPersistTaps={'handled'}>
                    <ImageView
                        style={styles.backImage}
                        source={Images.icon_bar_gift}
                    />
                    <LoginRow
                        placeholder={'原密码'}
                        icon={Images.icon_bar_gift}
                        onChangeText={(text) => this.setDataSource('oldPassword', text)}
                    />
                    <LoginRow
                        placeholder={'新密码，字母+数字组合，6-20位'}
                        icon={Images.icon_bar_gift}
                        onChangeText={(text) => this.setDataSource('password', text)}
                    />
                    <LoginRow
                        placeholder={'再次输入新密码'}
                        icon={Images.icon_bar_gift}
                        onChangeText={(text) => this.setDataSource('passwordTwo', text)}
                    />
                    <Button
                        style={styles.registerButton}
                        title={'确认'}
                        onPress={this._onPressSubmit}
                    />
                </KeyboardAwareScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    contentContainer: {

    },
    backImage: {
        width: 105,
        height: 114,
        marginBottom: 25,
        alignSelf: 'flex-end',
    },
    registerButton: {
        marginHorizontal: 30,
        marginVertical: 30,
    },

});