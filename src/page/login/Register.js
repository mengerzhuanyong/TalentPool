/**
 * @author jiasong
 * @creationTime 2019-02-19 17:41:28
 */

'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import NavigationBar from '../../component/Navigation/NavigationBar';
import Container from '../../component/Container/Container';
import ImageView from '../../component/Image/ImageView';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Theme from '../../config/themes/Theme';
import Button from '../../component/Touchable/Button'
import LoginRow from '../../component/Form/LoginRow';
import { checkMobile, checkEmpty } from '../../util/Tool';

export default class Register extends React.PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            mobile: '',
            verification: '',
            password: '',
            passwordTwo: '',
            openVerification: false
        }
    }

    _onPressSubmit = async () => {
        if (!this.cheackDataSource()) {
            return;
        }
        const { mobile, password, verification, passwordTwo } = this.state
        alert('注册')
    }

    setDataSource = (key, data, refresh) => {
        if (refresh) {
            let newDataSource = { ...this.state }
            newDataSource[key] = data
            Log.print('标记', newDataSource)
            this.setState({ ...newDataSource })
        } else {
            this.state[key] = data
        }
    }

    cheackDataSource = () => {
        const { mobile, password, verification, passwordTwo } = this.state
        if (!checkMobile(mobile)) {
            ToastManager.message('手机号填写不正确')
            return false
        }
        if (checkEmpty(verification)) {
            ToastManager.message('验证码填写不正确')
            return false
        }
        if (checkEmpty(password)) {
            ToastManager.message('密码填写不正确')
            return false
        }
        if (password != passwordTwo) {
            ToastManager.message('两次密码填写不正确')
            return false
        }
        return
    }

    _onStopInterval = () => {
        this.setState({ openVerification: false })
    }

    _onPressVerification = async () => {
        setTimeout(() => {
            this.setDataSource('openVerification', true, true)
        }, 200);
    }

    _onPressProtocol = () => {

    }

    render() {
        const { openVerification } = this.state
        return (
            <Container style={styles.container}>
                <NavigationBar title={'注册'} />
                <KeyboardAwareScrollView
                    style={styles.contentContainer}
                    bounces={false}
                    showsVerticalScrollIndicator={false}
                    keyboardShouldPersistTaps={'handled'}>
                    <ImageView
                        style={styles.backImage}
                        source={Images.icon_bar_gift}
                    />
                    <LoginRow
                        placeholder={'手机号码'}
                        icon={Images.icon_bar_gift}
                        onChangeText={(text) => this.setDataSource('mobile', text)}
                    />
                    <LoginRow
                        type={'verification'}
                        placeholder={'短信验证码'}
                        icon={Images.icon_bar_gift}
                        open={openVerification}
                        onStopInterval={this._onStopInterval}
                        onPressVerification={this._onPressVerification}
                        onChangeText={(text) => this.setDataSource('verification', text)}
                    />
                    <LoginRow
                        placeholder={'字母+数字组合，6-20位'}
                        icon={Images.icon_bar_gift}
                        onChangeText={(text) => this.setDataSource('password', text)}
                    />
                    <LoginRow
                        placeholder={'再次输入密码'}
                        icon={Images.icon_bar_gift}
                        onChangeText={(text) => this.setDataSource('passwordTwo', text)}
                    />
                    <Text style={styles.protocolMark}>注册代表您已阅读并同意
                        <Text
                            style={styles.protocol}
                            onPress={this._onPressProtocol}>
                            《样例APP的协议》
                        </Text>
                    </Text>
                    <Button
                        style={styles.registerButton}
                        title={'注册'}
                        onPress={this._onPressSubmit}
                    />
                </KeyboardAwareScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    contentContainer: {

    },
    backImage: {
        width: 105,
        height: 114,
        marginBottom: 25,
        alignSelf: 'flex-end',
    },
    registerButton: {
        marginHorizontal: 30,
        marginVertical: 30,
    },
    protocolMark: {
        marginTop: 10,
        marginHorizontal: 30,
        fontSize: 12,
        color: "#9b9b9b"
    },
    protocol: {
        fontSize: 12,
        color: "rgb(12,217,250)"
    },
});