/**
 * @author jiasong
 * @creationTime 2019-02-19 17:42:06
 */

'use strict';
import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import NavigationBar from '../../component/Navigation/NavigationBar';
import Container from '../../component/Container/Container';
import ImageView from '../../component/Image/ImageView';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Theme from '../../config/themes/Theme';
import Button from '../../component/Touchable/Button'
import LoginRow from '../../component/Form/LoginRow';
import { checkMobile, checkEmpty } from '../../util/Tool';

export default class Login extends React.PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            mobile: '',
            password: '',
        }
    }

    _onPressSubmit = async () => {
        if (!this.cheackDataSource()) {
            return;
        }
        const { mobile, password } = this.state
        alert('登录')
    }

    _onPressOtherLogin = async () => {

    }

    setDataSource = (key, data, refresh) => {
        if (refresh) {
            let newDataSource = { ...this.state }
            newDataSource[key] = data
            this.setState({ ...newDataSource })
        } else {
            this.state[key] = data
        }
    }

    cheackDataSource = () => {
        const { mobile, password } = this.state
        if (!checkMobile(mobile)) {
            ToastManager.message('手机号填写不正确')
            return false
        }
        if (checkEmpty(password)) {
            ToastManager.message('密码填写不正确')
            return false
        }
        return
    }

    render() {
        return (
            <Container style={styles.container}>
                <NavigationBar style={styles.nav} title={'登录'} />
                <KeyboardAwareScrollView
                    contentContainerStyle={styles.contentContainer}
                    bounces={false}
                    showsVerticalScrollIndicator={false}
                    keyboardShouldPersistTaps={'handled'}>
                    <ImageView
                        style={styles.backImage}
                        source={Images.icon_bar_gift}
                    />
                    <LoginRow
                        placeholder={'请输入注册手机号'}
                        icon={Images.icon_bar_gift}
                        onChangeText={(text) => this.setDataSource('mobile', text)}
                    />
                    <LoginRow
                        placeholder={'请输入密码'}
                        icon={Images.icon_bar_gift}
                        onChangeText={(text) => this.setDataSource('password', text)}
                    />
                    <Button
                        style={styles.loginButton}
                        title={'登录'}
                        onPress={this._onPressSubmit}
                    />
                    <View style={Theme.RCC}>
                        <Button
                            style={Theme.buttonStyle}
                            title={'忘记密码'}
                            titleStyle={Theme.FC16333}
                            onPress={() => RouterHelper.navigate('', 'ResetPassword')}
                        />
                        <View style={styles.sep} />
                        <Button
                            style={Theme.buttonStyle}
                            title={'立即注册'}
                            titleStyle={styles.registereTitle}
                            onPress={() => RouterHelper.navigate('', 'Register')}
                        />
                    </View>
                    <View style={styles.otherLoginContainer}>
                        <Text style={styles.otherLoginTitle}>——————   第三方登录  ——————</Text>
                        <View style={Theme.RCC}>
                            <Button
                                style={styles.otherLoginButton}
                                icon={Images.icon_bar_gift}
                                iconStyle={styles.otherLoginIcon}
                                title={'QQ'}
                                titleStyle={Theme.FC12333}
                                onPress={() => this._onPressOtherLogin('qq')}
                            />
                            <Button
                                style={styles.otherLoginButton}
                                icon={Images.icon_bar_gift}
                                iconStyle={styles.otherLoginIcon}
                                title={'微信'}
                                titleStyle={Theme.FC12333}
                                onPress={() => this._onPressOtherLogin('weixin')}
                            />
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    contentContainer: {
        flex: 1,

    },
    backImage: {
        width: 105,
        height: 114,
        marginBottom: 25,
        alignSelf: 'flex-end',
    },
    loginButton: {
        marginHorizontal: 30,
        marginTop: 25,
        marginBottom: 15,
    },
    sep: {
        width: 1,
        height: 20,
        backgroundColor: "#dbdbdb",
        marginHorizontal: 25
    },
    registereTitle: {
        fontSize: 15,
        color: "#29d9f8"
    },
    otherLoginContainer: {
        position: 'absolute',
        bottom: 20,
        alignSelf: 'center',

    },
    otherLoginTitle: {
        fontSize: 12,
        color: "#999999",
        marginBottom: 15
    },
    otherLoginButton: {
        flexDirection: 'column',
        paddingHorizontal: 0,
        paddingVertical: 0,
        backgroundColor: 'transparent',
        marginHorizontal: 20,
    },
    otherLoginIcon: {
        width: 30,
        height: 30,
        marginBottom: 5
    }

});