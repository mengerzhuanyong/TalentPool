/**
 * 人才库 - 人才库
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'

import NavigationBar from '../../component/Navigation/NavigationBar'
import Container from '../../component/Container/Container'
import SegmentedView from '../../component/Segmented/SegmentedView'
import TalentPoolTabList from '../../component/TalentPool/TalentPoolTabList'


export default class TalentPool extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            navArray: [
                {id: 1, title: '人才库', api: ServicesApi.personnelList},
                {id: 2, title: '候选', api: ServicesApi.filterList},
                {id: 3, title: '面试中', api: ServicesApi.auditionList},
                {id: 4, title: '体检中', api: ServicesApi.peList},
                {id: 5, title: '待入职', api: ServicesApi.prepareList},
                {id: 6, title: '在职', api: ServicesApi.inServiceList},
                {id: 7, title: '离职', api: ServicesApi.leaveCompanylist},
                {id: 8, title: '未合作', api: ServicesApi.noCooperationList},
                {id: 9, title: '不可用', api: ServicesApi.delList},
            ],
        };
    }

    componentDidMount() {
        this.requestDataSources();
    }

    componentWillMount() {
    }

    requestDataSources = async () => {

    };

    renderSegmentedTabContent = (navArray = []) => {
        if (navArray.length < 1) {
            return;
        }
        let content = navArray.map((item, index) => {
            return (
                <View
                    key={index}
                    title={item.title}
                    style={styles.segmentedItemView}
                >
                    <TalentPoolTabList
                        item={item}
                    />
                </View>
            );
        });
        return content;
    };

    render() {
        let pageTitle = '人才库';
        let {navArray} = this.state;
        return (
            <Container
                fitIPhoneX={false}
                style={styles.container}
            >
                <NavigationBar
                    title={pageTitle}
                    renderLeftAction={null}
                    renderRightAction={[
                        {icon: Images.icon_plus, onPress: () => RouterHelper.navigate('添加人才', 'TalentAddManual')},
                        {icon: Images.icon_filter, onPress: () => alert('筛选')}
                    ]}
                />
                {navArray.length > 0 ?
                    <SegmentedView
                        autoScroll={true}
                        indicatorType={'itemWidth'}
                        justifyItem={navArray.length > 5 ? 'scrollable' : 'fixed'}
                        indicatorWidth={SCREEN_WIDTH / (navArray.length > 5 ? 5 : navArray.length)}
                        style={styles.SegmentedView}
                    >
                        {this.renderSegmentedTabContent(navArray)}
                    </SegmentedView>
                    : null
                }
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {},
    segmentedItemView: {
        flex: 1,
    },
});