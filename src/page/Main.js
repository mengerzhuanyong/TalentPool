'use strict';
import React from 'react';
import { View, StyleSheet, Linking, AppState, DeviceEventEmitter, BackHandler, ToastAndroid } from 'react-native';
import SplashScreen from 'react-native-splash-screen'
import DeviceInfo from 'react-native-device-info';
import JPushModule from 'jpush-react-native'
import JShareModule from 'jshare-react-native';
import JanalyticsModule from 'janalytics-react-native';
import XPay from 'react-native-puti-pay';
import Navigation from '../router/Navigation';
import Orientation from 'react-native-orientation'
import NetInfo from "@react-native-community/netinfo";
import { getDeviceInfo } from '../util/Adaptation';
import { observer, inject } from 'mobx-react';

@inject('appStore')
@observer
export default class Main extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this._initSetting() // 初始化设置，先调用
        this._addPushListener(); // 通知,先调用
        this._handleLoginState() // 处理登陆状态
        this._addDeviceInfo() // 设备信息
        this._addLinkingListener() // 处理外部跳转链接
        this._addNetworkListener() // 网络状态
        this._addAppStateListener() // app前台还是后台
        this._addBackHandler()
    };

    _initSetting = () => {
        /**
         * @极光分享配置
         */
        if (__IOS__) {
            JShareModule.setup()
            JShareModule.setDebug({ enable: __DEV__ })
        }
        /**
         * @极光统计配置
         */
        if (__IOS__) {
            JanalyticsModule.setup({});
            JanalyticsModule.setDebug({ enable: true });
            !__DEV__ && JanalyticsModule.rnCrashLogON()
        }
        JanalyticsModule.setAnalyticsReportPeriod({ period: 0 })
        !__DEV__ && JanalyticsModule.crashLogON();
        /**
         * @微信和支付宝配置 
         */
        //设置微信ID
        XPay.setWxId(Constants.WEI_XIN_APPID)
        //设置支付宝URL Schemes
        XPay.setAlipayScheme(Constants.ALIPAY_SCHME)
        /**
         * @其他配置 
         */
        // 显示竖屏
        Orientation.lockToPortrait()
    };

    _handleLoginState = async () => {
        SplashScreen.hide()
    };

    _addPushListener = () => {
        if (__ANDROID__) {
            JPushModule.notifyJSDidLoad(resultCode => {
                if (resultCode === 0) {
                }
            })
            JPushModule.addReceiveCustomMsgListener(map => {
                // 自定义消息
                map.extras = JSON.parse(map.extras)
                Log.print('addReceiveCustomMsgListener', map.extras)
            })
            JPushModule.addGetRegistrationIdListener(registrationId => {
                // 设备注册成功
                Log.print('addGetRegistrationIdListener', registrationId)
            })
        }
        JPushModule.addReceiveNotificationListener(map => {
            // 接收推送事件     并且点击推送（ios9及以下）需要判断系统版本
            if (__ANDROID__) {
                map.extras = JSON.parse(map.extras)
            }
            Log.print('addReceiveNotificationListener', map.extras)
        })
        JPushModule.addReceiveOpenNotificationListener(map => {
            // 点击推送事件，安卓，iOS10及以上
            if (__ANDROID__) {
                map.extras = JSON.parse(map.extras)
            }
            Log.print('addReceiveOpenNotificationListener', map.extras)
        })

    };

    _addAppStateListener = () => {
        const { appStore } = this.props
        AppState.addEventListener('change', (state) => {
            appStore.changeAppState(state)
        })
    };

    _addLinkingListener = () => {
        Linking.addEventListener('url', (data) => {
            Log.print(data)
        })
    };

    _addNetworkListener = () => {
        const { appStore } = this.props
        NetInfo.addEventListener(state => {
            appStore.changeNetworkState(state.type)
        });
    };

    _addDeviceInfo = async () => {
        const { appStore } = this.props
        const info = getDeviceInfo()
        // const ip = await DeviceInfo.getIPAddress() ios上不可用，有可能是私有方法
        // const batteryLevel = await DeviceInfo.getBatteryLevel() // 电池的电量，模拟器返回-1
        appStore.setDeviceInfo({ ...info })
    };

    _addBackHandler = () => {
        if (__IOS__) {
            return;
        }
        BackHandler.addEventListener('hardwareBackPress', (data) => {
            const nowTime = Moment().format('x')
            const delay = 2000
            const routerStacks = RouterHelper.routerStacks
            const router = routerStacks[routerStacks.length - 1]
            if (router && router.routeName === 'Tab') {
                if (this.lastActionTime && (nowTime - this.lastActionTime) < delay) {
                    BackHandler.exitApp()
                    return false
                } else {
                    this.lastActionTime = nowTime
                    ToastAndroid.show('再按一次退出应用', delay - 1000)
                    return true
                }
            } else {
                RouterHelper.goBack()
                return true
            }
        })
    }

    render() {

        return (
            <View style={styles.container} >
                <Navigation />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

