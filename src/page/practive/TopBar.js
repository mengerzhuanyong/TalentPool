'use strict';
import React from 'react';
import { View, Text, StyleSheet, Animated } from 'react-native';
import PropTypes from 'prop-types'

export default class TopBar extends React.PureComponent {

    render() {
        const { scrollX } = this.props
        console.log('scrollX', scrollX)

        return (
            <View>
                <Animated.Text style={{
                    transform: [{
                        translateX: scrollX.interpolate({
                            outputRange: [0, 10, 20],
                            inputRange: [0, 100, 130]
                        }),
                    }]
                }} >{'1111'}</Animated.Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({

});