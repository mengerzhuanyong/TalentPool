/**
 * @author jiasong
 * @creationTime 2019-04-08 14:25:44
 */

'use strict';
import React from 'react';
import { View, Text, StyleSheet, ScrollView, Animated } from 'react-native';
import NavigationBar from '../../component/Navigation/NavigationBar';
import Container from '../../component/Container/Container';
import TopBar from './TopBar';

export default class Practice extends React.PureComponent {

    constructor(props) {
        super(props)
        this.scrollX = new Animated.Value(0)
    }

    _onScroll = () => {
        console.log('_onScroll')
    }

    render() {

        return (
            <Container>
                <NavigationBar title={'Practice'} />
                <TopBar scrollX={this.scrollX} />
                <Animated.ScrollView
                    horizontal={true}
                    pagingEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    bounces={false}
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { x: this.scrollX } } }],
                        { listener: this._onScroll, useNativeDriver: true }
                    )}
                    scrollEventThrottle={16}>
                    <Text style={{ width: 375 }}>123</Text>
                    <Text style={{ width: 375 }}>456</Text>
                </Animated.ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

});