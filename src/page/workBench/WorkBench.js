/**
 * 人才库 - 工作台
 * http://menger.me
 * @大梦
 */


'use strict';

import React from 'react'
import {ImageBackground, ScrollView, StyleSheet, Text, View,} from 'react-native'

import NavigationBar from '../../component/Navigation/NavigationBar'
import Container from '../../component/Container/Container'
import {VerticalLine} from "../../component/Common/CommonLine";
import ImageView from "../../component/Image/ImageView";
import Button from "../../component/Touchable/Button";


export default class WorkBench extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            message: {
                sys_msg: '',
                new_msg: '',
                task_msg: '',
            },
            navArray: [{
                title: '人才管理',
                data: [{
                    title: '添加人才',
                    icon: Images.icon_work_bench_nav1,
                    component: 'TalentAddManual',
                    type: 'navigate'
                },
                    {
                        title: '总人才库',
                        icon: Images.icon_work_bench_nav2,
                        component: 'TalentPool',
                        type: 'navigate'
                    },
                    {
                        title: '数据统计',
                        icon: Images.icon_work_bench_nav3,
                        component: 'DataStatistics',
                        type: 'navigate'
                    },
                    {
                        title: '简历来源',
                        icon: Images.icon_work_bench_nav4,
                        component: 'TalentResume',
                        type: 'navigate'
                    },
                    {
                        title: '招聘管理',
                        icon: Images.icon_work_bench_nav5,
                        component: 'RecruitmentManager',
                        type: 'navigate'
                    },
                ]
            },
                {
                    title: '招聘管理',
                    data: [{
                        title: '招聘小组',
                        icon: Images.icon_work_bench_nav6,
                        component: 'RecruitingTeamLeader',
                        type: 'navigate'
                    },
                        {
                            title: '招聘任务',
                            icon: Images.icon_work_bench_nav7,
                            component: 'RecruitmentTaskDetail',
                            type: 'navigate'
                        },
                        {
                            title: '我招聘的',
                            icon: Images.icon_work_bench_nav8,
                            component: 'RecruitmentMine',
                            type: 'navigate'
                        },
                    ]
                },
                {
                    title: '驻厂管理',
                    data: [{
                        title: '我的驻厂',
                        icon: Images.icon_work_bench_nav9,
                        component: 'ResidentMine',
                        type: 'navigate'
                    },
                        {
                            title: '我管理的',
                            icon: Images.icon_work_bench_nav10,
                            component: 'ResidentManager',
                            type: 'navigate'
                        },
                    ]
                },
                {
                    title: '设置',
                    data: [{
                        title: '修改密码',
                        icon: Images.icon_work_bench_nav11,
                        component: 'ModifyPassword',
                        type: 'navigate'
                    },
                        {
                            title: '清理缓存',
                            icon: Images.icon_work_bench_nav12,
                            component: 'onClearStorage',
                            type: 'foo'
                        },
                        {
                            title: '退出登录',
                            icon: Images.icon_work_bench_nav13,
                            component: 'onLogout',
                            type: 'foo'
                        },
                    ]
                }
            ]
        };
    }

    componentDidMount() {
        this.requestDataSources();
    }

    componentWillMount() {
    }

    requestDataSources = async () => {

    };

    renderNavigationContent = () => {
        let {navArray} = this.state;

        let content = navArray.map((item, index) => {
            return (
                <View key={index} style={styles.navArrayItemContent}>
                    <View style={[Theme.RCS, styles.itemConTitleWrap]}>
                        <View style={styles.conTitleLine}/>
                        <Text style={styles.itemConTitle}>{item.title}</Text>
                    </View>
                    <View style={[Theme.RCS, styles.subNavContent]}>
                        {this.renderNavigationItemContent(item, index)}
                    </View>
                </View>
            );
        });

        return (
            <View style={styles.navigationContent}>
                {content}
            </View>
        );
    };

    renderNavigationItemContent = (item, index) => {
        if (!item || item.data.length < 1) return null;
        let content = item.data.map((obj, _index) => {
            return (
                <Button
                    key={_index}
                    title={obj.title}
                    icon={obj.icon}
                    style={[Theme.CCC, styles.navBtnItemStyle]}
                    titleStyle={styles.navBtnTitleStyle}
                    iconStyle={styles.navBtnIconStyle}
                    onPress={() => this.onPressNavItem(obj)}
                />
            );
        });
        return content;
    };

    onPressNavItem = (item) => {
        console.log('item---->', item);
        if (item.type === 'navigate' && item.component !== '') {
            RouterHelper.navigate(item.title, item.component);
        } else if (item.type === 'foo') {
            switch (item.component) {
                case 'onClearStorage':
                    this.onClearStorage();
                    break;
                case 'onLogout':
                    this.onLogout();
                    break;
                default:
                    break;
            }
        }
    };

    onClearStorage = () => {

    };

    onLogout = () => {

    };

    render() {
        let {message} = this.state;
        let pageTitle = '工作台';
        return (
            <Container
                fitIPhoneX={false}
                style={styles.container}
            >
                <NavigationBar
                    title={pageTitle}
                    renderLeftAction={null}
                />
                <ScrollView style={styles.content}>
                    <ImageBackground
                        style={[Theme.RCB, styles.messageContent]}
                        source={Images.img_bg_message}
                    >
                        <View style={[Theme.CCC, styles.messageItem]}>
                            <Text style={styles.messageCon}>{message.new_msg || 0}</Text>
                            <Text style={styles.messageTitle}>待阅消息</Text>
                        </View>
                        <View style={[Theme.CCC, styles.messageItem]}>
                            <Text style={styles.messageCon}>{message.task_msg || 0}</Text>
                            <Text style={styles.messageTitle}>任务消息</Text>
                        </View>
                        <View style={[Theme.CCC, styles.messageItem]}>
                            <Text style={styles.messageCon}>{message.sys_msg || 0}</Text>
                            <Text style={styles.messageTitle}>系统消息</Text>
                        </View>
                        <VerticalLine style={styles.messageLine}/>
                        <View style={[Theme.CCC, styles.messageItem]}>
                            <ImageView
                                style={styles.messageIcon}
                                source={Images.img_logo_white}
                            />
                        </View>
                    </ImageBackground>
                    {this.renderNavigationContent()}
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    messageContent: {
        borderRadius: 5,
        overflow: 'hidden',
        marginVertical: 20,
        paddingVertical: 30,
        marginHorizontal: 15,
        paddingHorizontal: 15,
    },
    messageItem: {
        flex: 1,
    },
    messageCon: {
        fontSize: 20,
        color: '#fff',
        marginBottom: 10,
        fontWeight: '600',
    },
    messageTitle: {
        fontSize: 15,
        color: '#fff',
    },
    messageLine: {
        width: 1,
        height: 60,
        backgroundColor: '#fff',
    },
    messageIcon: {
        width: 50,
        height: 50,
    },

    content: {
        flex: 1,
    },
    navArrayItemContent: {
        marginBottom: 10,
        paddingHorizontal: 15,
    },
    itemConTitleWrap: {},
    conTitleLine: {
        width: 3,
        height: 18,
        marginRight: 5,
        backgroundColor: Theme.themeColor,
    },
    itemConTitle: {
        fontSize: 18,
        color: '#333',
    },
    subNavContent: {
        flexWrap: 'wrap',
    },
    navBtnItemStyle: {
        marginVertical: 5,
        width: (SCREEN_WIDTH - 30) / 3,
        backgroundColor: 'transparent',
    },
    navBtnTitleStyle: {
        fontSize: 15,
        color: '#333',
    },
    navBtnIconStyle: {
        width: 50,
        height: 50,
        marginBottom: 10,
    },
});