/**
 * 人才库 - 手动添加人才
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'

import NavigationBar from '../../component/Navigation/NavigationBar'
import Container from '../../component/Container/Container'
import SegmentedView from '../../component/Segmented/SegmentedView'
import TalentPoolTabList from '../../component/TalentPool/TalentPoolTabList'


export default class TalentAddManual extends React.PureComponent {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            navArray: [
                {id: 1, title: '人才库'},
                {id: 2, title: '候选'},
                {id: 3, title: '面试中'},
                {id: 4, title: '体检中'},
                {id: 5, title: '待入职'},
                {id: 6, title: '在职'},
                {id: 7, title: '离职'},
                {id: 8, title: '未合作'},
                {id: 9, title: '不可用'},
            ],
        };
    }

    componentDidMount() {
        this.requestDataSources();
    }

    componentWillMount() {
    }

    requestDataSources = async () => {

    };

    render() {
        let pageTitle = this.params.pageTitle || '添加人才';
        let {navArray} = this.state;
        return (
            <Container
                fitIPhoneX={false}
                style={styles.container}
            >
                <NavigationBar
                    title={pageTitle}
                />
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {},
    segmentedItemView: {
        flex: 1,
    },
});