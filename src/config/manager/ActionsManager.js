'use strict';
import React from 'react';
import { View, Text, Clipboard } from 'react-native';
import { Overlay } from 'teaset';
import { bouncerEmpty } from '../../util/Tool';
import AreaContent from '../../component/AreaPicker/AreaContent';
import ShareContent from '../../component/ActionSheet/ShareContent';
import ActionContent from '../../component/ActionSheet/ActionContent'
import WheelDay from '../../component/DatePicker/WheelDay'
import JShareModule from 'jshare-react-native';

export default class ActionsManager {

    static pullViewRefs = []

    static show(props = {}) {
        const { option, ...others } = props
        const component = (
            <ActionContent {...others} />
        )
        this.showPullView(component, option)
    }

    static showArea(props = {}) {
        const { onPress, option, ...others } = props
        const component = (
            <AreaContent onPress={onPress} {...others} />
        )
        this.showPullView(component, option)
    }

    static showDate(props = {}) {
        const { onPress, defaultValue, option, ...others } = props
        const component = (
            <WheelDay
                onPress={onPress}
                defaultValue={defaultValue}
                {...others}
            />
        )
        this.showPullView(component, option)
    }

    static showShare(shareParams = {}) {
        const { type, url, title, text } = shareParams
        const component = (
            <ShareContent
                onPress={(shareType) => {
                    let platform;
                    switch (shareType) {
                        case 'weixin':
                            platform = 'wechat_session'
                            break;
                        case 'timeline':
                            platform = 'wechat_timeLine'
                            break;
                        case 'copy':
                            Clipboard.setString(url)
                            ToastManager.message('已复制到剪切板')
                            return;
                        default:
                            break;
                    }
                    const params = { type, platform, url, title, text, imageUrl: ServicesApi.ICON_APP }
                    JShareModule.share(params, (result) => {
                        if (result.state == 'success') {
                            ToastManager.message('分享成功！')
                        } else {
                            ToastManager.message('分享失败！')
                        }
                    }, (error) => {
                        ToastManager.message('分享失败！')
                    })
                }}
            />
        )
        this.showPullView(component, {})
    }

    static showPullView(component, option = {}) {
        this.pullViewRefs = bouncerEmpty(this.pullViewRefs) // 过滤
        if (this.pullViewRefs.length === 0) {
            Overlay.show(
                <Overlay.PullView
                    ref={v => this.pullViewRefs.push(v)}
                    side={'bottom'}
                    rootTransform={'none'}
                    modal={false}
                    containerStyle={{ backgroundColor: 'transparent' }}
                    onCloseRequest={() => this.hide()}
                    {...option}
                >
                    {component}
                </Overlay.PullView>
            )
        }
    }

    static hide() {
        this.pullViewRefs = bouncerEmpty(this.pullViewRefs) // 过滤
        if (this.pullViewRefs.length > 0) {
            const lastRef = this.pullViewRefs.pop()
            lastRef.close()
        }
    }

}

