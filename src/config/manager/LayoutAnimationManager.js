'use strict';
import { LayoutAnimation } from 'react-native';

const defaultOption = {
    duration: 300,
    property: LayoutAnimation.Properties.scaleXY,
    type: LayoutAnimation.Types.linear,
}

export default class LayoutAnimationManager {

    static createWithScaleXY(option) {
        option = { ...defaultOption, ...option }
        LayoutAnimation.configureNext({
            duration: option.duration,
            create: { ...option }
        })
    }

    static updateWithScaleXY(option) {
        option = { ...defaultOption, ...option }
        LayoutAnimation.configureNext({
            duration: option.duration,
            update: { ...option }
        })
    }

    static deleteWithScaleXY(option) {
        option = { ...defaultOption, ...option }
        LayoutAnimation.configureNext({
            duration: option.duration,
            delete: { ...option }
        })
    }

}
