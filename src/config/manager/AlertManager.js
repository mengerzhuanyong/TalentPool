'use strict';
import React from 'react';
import AlertContent from '../../component/Alert/AlertContent';
import { bouncerEmpty } from '../../util/Tool';
import { Overlay } from 'teaset';

export default class AlertManager {

    static popViewRefs = []

    static show(props = {}) {
        const { option, ...others } = props
        const component = (
            <AlertContent {...others} />
        )
        this.showPopView(component, option)
    }

    static showPopView(component, option = {}) {
        this.popViewRefs = bouncerEmpty(this.popViewRefs) // 过滤
        if (this.popViewRefs.length === 0) {
            Overlay.show(
                <Overlay.PopView
                    ref={v => this.popViewRefs.push(v)}
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                    type={'zoomOut'}
                    modal={false}
                    onCloseRequest={() => this.hide()}
                    {...option}
                >
                    {component}
                </Overlay.PopView>
            )
        }
    }

    static hide() {
        this.popViewRefs = bouncerEmpty(this.popViewRefs) // 过滤
        if (this.popViewRefs.length > 0) {
            const lastRef = this.popViewRefs.pop()
            lastRef.close()
        }
    }

}

