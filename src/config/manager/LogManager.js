'use strict';
import { toJS } from 'mobx';

const style = 'color:#2b73af;font-size:12px'

export default class LogManager {

    static print(...params) {
        if (!__DEV__) { // 生产环境
            return;
        }
        try {
            // console.trace();
            if (typeof arguments[0] === 'string' || typeof arguments[0] === 'number') {
                console.group(`%c${arguments[0]}`, style);
            } else {
                console.group(`%c${JSON.stringify(arguments[0])}`, style);
            }
            for (const key in arguments) {
                if (arguments.hasOwnProperty(key)) {
                    if (key > 0) {
                        const arg = arguments[key];
                        console.log(`%c数据-${key}`, style, toJS(arg));
                    }
                }
            }
            console.groupEnd();
        } catch (error) {

        }
    }

}

