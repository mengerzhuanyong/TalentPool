'use strict';
import Storage from 'react-native-storage'
import AsyncStorage from '@react-native-community/async-storage';

const storageInstance = new Storage({
    size: 1000,
    storageBackend: AsyncStorage,
    defaultExpires: null,
    enableCache: true,
});

export default class StorageManager {

    static load(key) {
        return new Promise((resolve, reject) => {
            storageInstance.load({ key }).then((data) => {
                resolve({ code: StatusCode.SUCCESS_CODE, data: data })
            }).catch((error) => {
                resolve({ code: StatusCode.FAIL_CODE, data: null })
            })
        });
    }

    static save = async (key, data) => {
        return new Promise((resolve, reject) => {
            storageInstance.save({ key, data }).then((data) => {
                resolve({ code: StatusCode.SUCCESS_CODE, data: data })
            }).catch((error) => {
                resolve({ code: StatusCode.FAIL_CODE, data: null })
            })
        });
    }

    static remove = async (key) => {
        return new Promise((resolve, reject) => {
            storageInstance.remove({ key }).then((data) => {
                resolve({ code: StatusCode.SUCCESS_CODE, data: data })
            }).catch((error) => {
                resolve({ code: StatusCode.FAIL_CODE, data: null })
            })
        });
    }
}

