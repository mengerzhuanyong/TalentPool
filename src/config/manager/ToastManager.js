'use strict';

import React from 'react'
import { Toast } from 'teaset';
import ToastView from '../../component/Toast/ToastView';


export default class ToastManager {

    static toastKey = null;

    static message(text, options) {
        if (text === 'error') {
            this.warn(text)
        } else {
            this.showToastView(<ToastView type={'message'} text={text} />, options);
        }
    };

    static success(text, options) {
        this.showToastView(<ToastView type={'success'} text={text} />, options);
    };

    static fail(text, options) {
        this.showToastView(<ToastView type={'fail'} text={text} />, options);
    };

    static warn(text, options) {
        this.showToastView(<ToastView type={'warn'} text={text} />, options);
    };

    static loading(text, options) {
        this.showToastView(<ToastView type={'loading'} text={text} />, {
            duration: 60000,
            modal: true,
            onCloseRequest: null,
            ...options
        });
    }

    static showToastView(component, option = {}) {
        this.hide()
        this.toastKey = Toast.show({
            text: component,
            ...Theme.toastOptions,
            ...option
        });
    }

    static hide() {
        if (this.toastKey) {
            Toast.hide(this.toastKey);
            this.toastKey = null
        }
    };

}