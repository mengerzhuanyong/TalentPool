'use strict';
import { AudioRecorder, AudioUtils } from 'react-native-audio';
import { PermissionsAndroid } from 'react-native';

const option = {
    SampleRate: 22050,
    Channels: 1,
    AudioQuality: "Low",
    AudioEncoding: "aac",
    AudioEncodingBitRate: 32000,
    OutputFormat: 'aac_adts'
}

const defaultAudioPath = AudioUtils.CachesDirectoryPath + '/tmp.aac'
console.log('defaultAudioPath', defaultAudioPath)

let onProgressCallBack = null, onFinishedCallBack = null

export default class RecordManager {

    static option = option

    static audioPath = defaultAudioPath

    static async prepareRecording(onProgress, onFinished) {
        const result = await AudioRecorder.requestAuthorization()
        if (result) {
            AudioRecorder.removeListeners()
            AudioRecorder.prepareRecordingAtPath(this.audioPath, this.option);
            AudioRecorder.onProgress = onProgress
            AudioRecorder.onFinished = onFinished
        }
        return result
    }

    static onProgress(data) {
        onProgressCallBack && onProgressCallBack(data)
    }

    static onFinished(data) {
        onFinishedCallBack && onFinishedCallBack(data)
        AudioRecorder.removeListeners()
        onProgressCallBack = null
        onFinishedCallBack = null
    }

    // 开始录音
    static async startRecord(callBack) {
        onProgressCallBack = callBack
        const result = await this.prepareRecording(this.onProgress, this.onFinished)
        if (result) {
            AudioRecorder.startRecording();
        }
        return result
    }

    // 停止录音
    static stopRecord = (callBack) => {
        onFinishedCallBack = callBack
        AudioRecorder.stopRecording();
    }
}
