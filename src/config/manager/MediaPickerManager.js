'use strict';
import SyanImagePicker from 'react-native-syan-image-picker';
import ImagePicker from 'react-native-image-picker';
import { PermissionsAndroid } from 'react-native'

export default class MediaPickerManager {

    static async checkPermission(permission) {
        if (__IOS__) {
            return Promise.resolve(true);
        }
        const result = await PermissionsAndroid.request(permission, null)

        return (result === true || result === PermissionsAndroid.RESULTS.GRANTED);
    }

    static showChooseMediaPicker(mediaType, option = {}) {
        return new Promise((resolve, reject) => {
            let showPickerFunc = null, imageCount = 1
            if (mediaType === 'video') {
                showPickerFunc = SyanImagePicker.openVideoPicker;
                imageCount = 1
            } else {
                showPickerFunc = SyanImagePicker.showImagePicker;
                imageCount = option.imageCount || 1
            }
            showPickerFunc({ quality: 50, ...option, imageCount, videoCount: 1 }, (error, selectedData) => {
                let data = []
                if (error) {
                    resolve({ code: StatusCode.FAIL_CODE, data, msg: '取消选择' })
                } else {
                    selectedData.forEach(item => {
                        if (mediaType === 'video') {
                            data.push({
                                type: 'video',
                                duration: parseFloat(item.duration),
                                fileName: item.fileName,
                                size: parseFloat(item.size),
                                path: item.uri.startsWith('file://') ? item.uri : `file://${item.uri}`,
                            })
                        } else {
                            data.push({
                                type: 'image',
                                size: item.size,
                                width: item.width,
                                height: item.height,
                                path: item.uri.startsWith('file://') ? item.uri : `file://${item.uri}`,
                                base64: item.base64 ? item.base64.slice(23) : null
                            })
                        }
                    });
                    resolve({ code: StatusCode.SUCCESS_CODE, data, msg: '' })
                }
            })
        })
    }

    static showLaunchCamera(option = {}) {
        return new Promise(async (resolve, reject) => {
            const checkRes = await this.checkPermission(PermissionsAndroid.PERMISSIONS.CAMERA)
            if (checkRes) {
                ImagePicker.launchCamera({ quality: 0.7, ...option }, (response) => {
                    let data = null
                    if (response.didCancel) {
                        // 取消
                        resolve({ code: StatusCode.FAIL_CODE, data, msg: '取消选择' })
                    } else if (response.error) {
                        resolve({ code: StatusCode.FAIL_CODE, data: null, msg: response.error })
                    } else if (response.customButton) {
                        resolve({ code: StatusCode.FAIL_CODE, data: null, msg: response.customButton })
                    } else {
                        data = {
                            size: response.fileSize,
                            width: response.width,
                            height: response.height,
                            path: __IOS__ ? response.uri : response.path,
                            base64: response.data
                        }
                        resolve({ code: StatusCode.SUCCESS_CODE, data, msg: '' })
                    }
                });
            } else {
                resolve({ code: StatusCode.FAIL_CODE, data: null, msg: '没有获取到权限' })
            }
        })
    }


    static showImagePicker_old(option = {}) {
        return new Promise((resolve, reject) => {
            ImagePicker.launchImageLibrary({ ...option, quality: 0.7 }, (response) => {
                let data = null
                if (response.didCancel) {
                    // 取消
                    resolve({ code: StatusCode.FAIL_CODE, data, msg: '取消选择' })
                } else if (response.error) {
                    resolve({ code: StatusCode.FAIL_CODE, data: null, msg: response.error })
                } else if (response.customButton) {
                    resolve({ code: StatusCode.FAIL_CODE, data: null, msg: response.customButton })
                } else {
                    data = {
                        size: response.fileSize,
                        width: response.width,
                        height: response.height,
                        path: __IOS__ ? response.uri : response.path,
                        base64: response.data
                    }
                    resolve({ code: StatusCode.SUCCESS_CODE, data, msg: '' })
                }
            });
        });
    }

}

