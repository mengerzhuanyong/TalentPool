'use strict';
import Sound from 'react-native-sound';

export default class SoundManager {

    static soundInstance = null

    static createSound(fileName, basePath, errFunc) {
        this.releaseSound()
        this.soundInstance = new Sound(fileName, basePath, errFunc);
    }

    static startSound(uri, callBack) {
        if (!uri) {
            callBack && callBack({ codo: StatusCode.FAIL_CODE })
            return;
        }
        let fileName, basePath;
        if (uri.startsWith('http')) {
            fileName = uri
            basePath = null
        } else {
            let tmpArr = uri.split('/')
            fileName = tmpArr.pop()
            basePath = tmpArr.join('/')
        }
        Log.print('startSound', fileName, basePath)
        this.createSound(fileName, basePath, (error) => {
            Log.print('error', error)
            if (!error) {
                this.soundInstance.play((success) => {
                    callBack && callBack(success)
                    this.releaseSound()
                    Log.print('play', success)
                })
            } else {
                callBack && callBack(error)
            }
        })
    }

    static stopSound() {
        if (this.soundInstance && this.soundInstance.isPlaying()) {
            this.soundInstance.stop()
        }
    }

    static releaseSound() {
        if (this.soundInstance) {
            this.soundInstance.release();
            this.soundInstance = null
        }
    }

}
