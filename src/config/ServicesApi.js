'use strict'

const QI_NIU_HOST = 'http://rencaiku.renkeyun.cn';
const HOST = 'http://rencaiku.renkeyun.cn';
const API_HOST = HOST + '/api/';

export default {
    /**
     * @系统 [HOST]
     */
    HOST: HOST,
    QI_NIU_HOST: QI_NIU_HOST,
    API_HOST: API_HOST,

    /**
    * @资源 [APP图标]
    */
    ICON_APP: QI_NIU_HOST + '/icon_app.png',

    /**
     * @七牛云 
     */
    // 上传地址
    QI_NIU_UPLOAD: 'http://up-z0.qiniup.com',
    // 图片前缀
    QI_NIU_LINKE: QI_NIU_HOST + '/',
    // 获取token
    GET_OSS_TOKEN: API_HOST + '/file/getOSSToken',

    // 登录
    login: API_HOST + 'login/checklogin',
    // 登出
    logOut: API_HOST + 'login/LogOut',
    // 根据省份查询所有的城市
    getCityByPid: API_HOST + 'Area/getCityByPid',
    // 修改密码
    changepassword: API_HOST + 'user/changepassword',
    // 人才简历
    resume: API_HOST + 'personnel/resume',
    // 人才库查询条件
    selectWhereList: API_HOST + 'personnel/selectWhereList',
    // 查询全部人才总数
    countAll: API_HOST + 'personnel/countAll',
    // 查询未入职人才总数
    countNotJob: API_HOST + 'personnel/countNotJob',
    // 查询待入职人才总数
    countPrepare: API_HOST + 'personnel/countPrepare',
    // 获取平台所有招聘小组列表-二维
    getRecruitTeamList: API_HOST + 'Recruit_Team/getRecruitTeamList',
    // 获取平台所有招聘小组列表-多维
    getRecruitTeam: API_HOST + 'Recruit_Team/getRecruitTeam',
    // 人才库列表和查询-人才库
    personnelList: API_HOST + 'Process_List/personnelList',
    // 人才库列表和查询-候选中
    filterList: API_HOST + 'Process_List/filterList',
    // 人才库列表和查询-面试中
    auditionList: API_HOST + 'Process_List/auditionList',
    // 人才库列表和查询-体检中
    peList: API_HOST + 'Process_List/peList',
    // 人才库列表和查询-待入职
    prepareList: API_HOST + 'Process_List/prepareList',
    // 人才库列表和查询-在职
    inServiceList: API_HOST + 'Process_List/inServiceList',
    // 人才库列表和查询-离职
    leaveCompanylist: API_HOST + 'Process_List/leaveCompanylist',
    // 人才库列表和查询-未合作
    noCooperationList: API_HOST + 'Process_List/noCooperationList',
    // 人才库列表和查询-不可用
    delList: API_HOST +'Process_List/delList',
    // 人才头部统计
    countTopList: API_HOST + 'Count/countTopList',
    // 简历来源渠道列表
    resumeChannelList: API_HOST + 'Resume_Channel/resumeChannelList',
    // 添加简历来源渠道
    addNewResumeChannel: API_HOST + 'Resume_Channel/addNewResumeChannel',
    // 人才添加所用参数
    personneloption: API_HOST + 'personnel/personneloption/',
    // 招聘任务岗位查找筛选列表
    getPositionTaskList: API_HOST + 'Position/getPositionTaskList',
    // 取消候选
    filterCancel: API_HOST + 'Flow_Control/filterCancel',
    // 人才添加
    personnelEdit: API_HOST + 'personnel/personnelEdit',
    // 候选通过
    filterThrough: API_HOST + 'Flow_Control/filterThrough',
    // 面试不通过
    auditionPass: API_HOST + 'Flow_Control/auditionPass',
    // 通过面试
    auditionThrough: API_HOST + 'Flow_Control/auditionThrough',
    // 体检不通过
    pePass: API_HOST + 'Flow_Control/pePass',
    // 体检通过
    peThrough: API_HOST + 'Flow_Control/peThrough',
    // 确认入职   ========>有问题
    prepareThrough: API_HOST + 'Flow_Control/prepareThrough',
    // 放弃入职
    prepareCancel: API_HOST + 'Flow_Control/prepareCancel',
    // 调换岗位
    changePosition: API_HOST + 'Flow_Control/changePosition',
    // 离职
    leaveCompany: API_HOST + 'Flow_Control/leaveCompany',
    // 恢复可用
    recovery: API_HOST + 'personnel/recovery',
    // 设置不可用
    del: API_HOST + 'personnel/del',
    // 人才库-人才库-分配按钮
    distribution: API_HOST + 'personnel/distribution',
    // 驻企用户列表
    userList: API_HOST + 'User/userList',
    // 企业管理-渠道来源列表
    getList: API_HOST + 'Company_Channel/getList',
    // 新增或修改渠道来源
    createEdit: API_HOST + 'Company_Channel/createEdit',
    // 删除来源渠道
    delCompanyChannel: API_HOST + 'Company_Channel/delCompanyChannel',
    // 驻企管理
    stationIndex: API_HOST + 'Station/index',
    // 企业列表
    companyList: API_HOST + 'Company/companyList',
    // 驻企修改或新增
    editStation: API_HOST + 'Station/editStation',
    // 驻企删除
    delStation: API_HOST + 'Station/delStation',
    // 企业管理-行业类型列表
    BusinessType: API_HOST + 'Business_Type/getList/',
    // 新增或修改行业类型
    businessTypeCreateEdit: API_HOST + 'Business_Type/createEdit',
    // 删除行业类型
    delBusinessType: API_HOST + 'Business_Type/delBusinessType',
    // 企业管理-客户级别列表
    companylevelGetList: API_HOST + 'Company_level/getList/',
    // 新增或修改客户级别
    companyLevel: API_HOST + 'Company_Level/createEdit/',
    // 删除客户级别
    delCompanyLevel: API_HOST + 'Company_Level/delCompanyLevel',
    // 企业管理-客户状态列表
    companyTypeList: API_HOST + 'Company_Type/getList',
    // 新增或修改客户状态
    companyTypeCreateEdit: API_HOST + 'Company_Type/createEdit',
    // 删除客户状态
    delCompanyType: API_HOST + 'Company_Type/delCompanyType',
    // 企业搜索列表
    companySearchList: API_HOST + 'Company/companySearchList/',
    // 企业列表筛选条件
    searchWhere: API_HOST + 'Company/searchWhere',
    // 新增或修改企业
    companyCreateEdit: API_HOST + 'Company/createEdit/',
    // 禁用启用企业
    setInvalid: API_HOST + 'Company/setInvalid/',
    // 搜索招聘小组列表-多维
    recruitTeamList: API_HOST + 'Recruit_Team/RecruitTeamList',
    // 新增或修改招聘小组
    createEditRecruitTeam: API_HOST + 'Recruit_Team/createEditRecruitTeam',
    // 删除招聘小组
    delRecruitTeam: API_HOST + 'Recruit_Team/delRecruitTeam',
    // 招聘小组详情
    recruitDetail: API_HOST + 'Recruit_Team/recruitDetail',
    // 招聘岗位类型列表
    positionTypeList: API_HOST + 'Position_Type/PositionTypeList',
    // 添加修改招聘岗位类型
    addPositionType: API_HOST + 'Position_Type/createEdit',
    // 删除招聘岗位类型
    delPositionType: API_HOST + 'Position_Type/delPositionType',
    // 额外补贴列表
    subsidyList: API_HOST + 'Subsidy/SubsidyList',
    // 新增或修改额外补贴
    addSubsidy: API_HOST + 'Subsidy/createEdit',
    // 删除额外补贴
    delSubsidy: API_HOST + 'Subsidy/delSubsidy',
    // 发布招聘任务岗位条件
    positionOption: API_HOST + 'position/positionOption',
    // 合同添加相关条件
    contractOption: API_HOST + 'Contract/contractOption',


}