/**
 * 人才库 - 全局的颜色
 * http://menger.me
 * @大梦
 */

'use strict';

export default {
    /**
     * @全局的颜色
     */
    overallColor: '#333',
    themeColor: '#1890ff',
    themeDeepColor: '#3481dc',

    borderColor: '#d8d8d8',

    redColor: '#f00',
    grayColor: '#e3e3e3',
    blueColor: '#1890ff',


}