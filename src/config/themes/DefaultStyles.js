'use strict'
import { StyleSheet } from 'react-native'
import DefaultColor from './DefaultColor';
import { addFontSizeWithStyleSheet } from '../../util/Adaptation'

addFontSizeWithStyleSheet();

const minPixel = StyleSheet.hairlineWidth;

export default StyleSheet.create({
    /**
     * @全局的通用样式
     */
    WRAP: {
        flexWrap: 'wrap',
    },
    RCA: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    RCB: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    RCC: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    RCE: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    RCS: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    RSS: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    RSB: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
    },
    CSC: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    CCA: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    CCB: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    CSA: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'space-around',
    },
    CSB: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
    },
    CSS: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    CCC: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    CCE: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    CEE: {
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    CAE: {
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'flex-end',
    },
    CBE: {
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'flex-end',
    },
    CCS: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    transparentNavBar: {
        borderBottomWidth: 0,
        backgroundColor: 'rgba(0,0,0,0)',
    },
    headerIconStyle: {
        width: 18,
        height: 18,
        marginRight: 5,
    },
    MT7: {
        marginTop: 7,
    },
    MB5: {
        marginBottom: 5,
    },
    MB7: {
        marginBottom: 7,
    },
    ML7: {
        marginLeft: 7,
    },
    ML5: {
        marginLeft: 5,
    },
    MR7: {
        marginRight: 7,
    },
    MT5: {
        marginTop: 5,
    },
    MT15: {
        marginTop: 15,
    },
    MT10: {
        marginTop: 10,
    },
    MB10: {
        marginBottom: 10,
    },
    ML10: {
        marginLeft: 10,
    },
    MR10: {
        marginRight: 10,
    },
    MR5: {
        marginRight: 5,
    },
    SEP: {
        height: minPixel,
        backgroundColor: "#f3f3f3"
    },
    SEP10: {
        height: 10,
        backgroundColor: "#f3f3f3"
    },
    /**
     * @字体大小和颜色
     */
    FC16333: {
        fontSize: 16,
        color: '#333'
    },
    FC15333: {
        fontSize: 15,
        color: '#333'
    },
    FC15666: {
        fontSize: 15,
        color: '#666'
    },
    FC14333: {
        fontSize: 14,
        color: '#333'
    },
    FC13333: {
        fontSize: 13,
        color: '#333'
    },
    FC14666: {
        fontSize: 14,
        color: '#666'
    },
    FC13666: {
        fontSize: 13,
        color: '#666'
    },
    FC12333: {
        fontSize: 12,
        color: '#666'
    },
    FC12fff: {
        fontSize: 12,
        color: '#666'
    },
    FC10fff: {
        fontSize: 10,
        color: '#fff'
    },
    BG_FFF: {
        backgroundColor: '#fff',
    },
    /**
     * @button的通用样式一 左边是icon，右边是标题
     */
    buttonStyle: {
        backgroundColor: 'transparent',
        borderRadius: 0,
        paddingVertical: 0,
        paddingHorizontal: 0,
    },
    /**
     * @SegmentedView的通用样式一
     */
    segmentedBarScrollableStyle: {
        height: 50,
        backgroundColor: '#f3f3f3',
    },
    segmentedBarStyle: {
        height: 60,
        alignItems: 'center',
        paddingHorizontal: 5,
        justifyContent: 'center',
        backgroundColor: '#f3f3f3',
    },
    tabStyle: {
        borderRadius: 35,
        backgroundColor: '#fff',
    },
    activeTabStyle: {
        borderRadius: 35,
        // backgroundColor: DefaultColor.themeColor,
        // borderColor: DefaultColor.themeDeepColor,
    },
    activeTitleStyle: {
        color: DefaultColor.themeColor,
    },
    segmentedItemView: {
        flex: 1,
    },
    /**
     * @SegmentedControlTab的通用样式一
     */
    controlStyle: {
        backgroundColor: 'transparent',
        borderWidth: 0,
    },
    controlTabStyle: {
        borderRadius: 35,
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: '#fff',
        borderColor: '#fff',
    },
    controlActiveTabStyle: {
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 35,
        backgroundColor: DefaultColor.overallColor,
        borderColor: '#fff'
    },
    controlTitleStyle: {
        color: '#666'
    },
    controlActiveTitleStyle: {
        color: '#fff'
    },

    /**
     * @Table样式
     */
    tableStyle: {},
    tableRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    tableCell: {
        flex: 1,
        minHeight: 45,
        paddingVertical: 8,
        paddingHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 0.5 * minPixel,
        marginHorizontal: 0.5 * minPixel,
    },
    tableCellText: {
        fontSize: 14,
        color: '#333',
        textAlign: 'center',
    },

    /**
     * @flex
     */
    flex1: {
        flex: 1,
    },
    flex015: {
        flex: 1.5,
    },
    flex2: {
        flex: 2,
    },
    flex3: {
        flex: 3,
    },

    /**
     * @TextAlign
     */
    tal: {
        textAlign: 'left',
    },
    tac: {
        textAlign: 'center',
    },
    tar: {
        textAlign: 'right',
    },
    taj: {
        textAlign: 'justify',
    },

    /**
     * @分割线
     */
    separatorStyle: {
        marginVertical: 10,
        backgroundColor: '#ccc',
    },

    /**
     * @验证码
     */
    sendSMSLine: {
        backgroundColor: DefaultColor.themeColor,
    },
    sendSMSTitle: {
        fontSize: 13,
        color: DefaultColor.themeColor,
    },

    minPixel: minPixel,

});