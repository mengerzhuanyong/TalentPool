'use strict'
import { Dimensions, Platform, Text, StyleSheet, StatusBar, TextInput, TouchableOpacity } from 'react-native'
import { Theme } from 'teaset'
import DefaultTheme from './DefaultTheme';
import DefaultColor from './DefaultColor';
import DefaultStyles from './DefaultStyles';
import { addCustomProps } from '../../util/Adaptation'

// 更改三个文件控件字体大小随系统改变的属性,如果想更改其它第三方的默认属性也可以这样改
addCustomProps(Text, { allowFontScaling: false });
addCustomProps(TextInput, { allowFontScaling: false, selectionColor: DefaultColor.overallColor });
addCustomProps(TouchableOpacity, { activeOpacity: 0.9 });

/**
 * @配置全局的teaset的Theme
 */
Theme.set({
    /**
     * @开启iphoneX适配
     */
    fitIPhoneX: true,

    /**
     * @设置toastManager的初始化配置 
     */
    toastColor: 'transparent',
    toastPaddingLeft: 0,
    toastPaddingRight: 0,
    toastPaddingTop: 0,
    toastPaddingBottom: 0,
    toastBorderRadius: 0,
    toastScreenPaddingLeft: 0,
    toastScreenPaddingRight: 0,
    toastScreenPaddingTop: 0,
    toastScreenPaddingBottom: 0,
})

export default {

    // 设置
    set: function (object) {
        Object.assign(this, object)
    },

    ...DefaultTheme,
    ...DefaultColor,
    ...DefaultStyles,

    get statusBarHeight() {
        if (Platform.OS === 'ios') {
            if (DefaultTheme.isIPhoneX) {
                return DefaultTheme.fitIPhoneXTop
            } else {
                return 20
            }
        } else if (Platform.OS === 'android') {
            if (Platform.Version > 20) {
                // console.log('StatusBar.currentHeight', Platform.Version)
                return StatusBar.currentHeight
            }
            return 0;
        }
        return 0
    },
    get screenWidth() {
        return Dimensions.get('screen').width
    },
    get screenHeight() {
        return Dimensions.get('screen').height
    },
    get windowWidth() {
        return Dimensions.get('window').width
    },
    get windowHeight() {
        return Dimensions.get('window').height
    },
    get screenInset() {
        return Theme.screenInset
    },
    get isLandscape() {
        return Dimensions.get('screen').width > Dimensions.get('screen').height
    },

}