'use strict'
import { Platform, YellowBox, UIManager } from 'react-native'
import { fontSize, scaleSize } from '../util/Adaptation'
import Images from '../asset/index'
import * as Services from '../util/Services'
import StorageManager from './manager/StorageManager';
import LogManager from './manager/LogManager';
import MenuManager from './manager/MenuManager'
import ToastManager from './manager/ToastManager'
import ActionsManager from './manager/ActionsManager'
import LayoutAnimationManager from './manager/LayoutAnimationManager';
import AlertManager from './manager/AlertManager';
import InteractionManager from './manager/InteractionManager';
import RouterHelper from '../router/RouterHelper'
import Constants from './Constants'
import ServicesApi from './ServicesApi';
import StatusCode from './StatusCode'
import Theme from './themes/Theme'
import moment from 'moment'
import 'moment/locale/zh-cn'

/**
 * @开启安卓的布局动画
 */
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
/**
 * @时间管理本地化
 */
moment.locale('zh-cn');
/**
 * @屏蔽调试警告
 */
YellowBox.ignoreWarnings([
    'Module',
    'It looks',
    'Require cycle:',
    'Remote debugger is in',
    'Required dispatch_sync',
    'Warning: isMounted(...)',
    /**
     * @暂时屏蔽 [后续更新]
     */
    'Warning: Slider',
    'Warning: ViewPagerAndroid',
    'Trying to subscribe to unknown event',
    'Accessing view manager configs directly off UIManager',
]);
/**
 * @屏蔽输出
 */
if (!__DEV__) {
    global.console = {
        info: () => { },
        log: () => { },
        warn: () => { },
        debug: () => { },
        error: () => { }
    };
}
/**
 * @Log管理
 */
global.Log = LogManager
/**
 * @系统是iOS
 */
global.__IOS__ = (Platform.OS === 'ios');
/**
 * @系统是安卓
 */
global.__ANDROID__ = (Platform.OS === 'android');
/**
 * @获取屏幕宽度
 */
global.SCREEN_WIDTH = Theme.screenWidth;
/**
 * @获取屏幕高度
 */
global.SCREEN_HEIGHT = Theme.screenHeight;
/**
 * @适配字体
 */
global.FontSize = fontSize;
/**
 * @屏幕适配
 */
global.ScaleSize = scaleSize;
/**
 * @图片资源管理
 */
global.Images = Images;
/**
 * @存储
 */
global.StorageManager = StorageManager;
/**
 * @网络请求
 */
global.Services = Services;
/**
 * @api
 */
global.ServicesApi = ServicesApi;
/**
 * @状态码
 */
global.StatusCode = StatusCode
/**
 * @时间处理
 */
global.Moment = moment;
/**
 * @路由管理
 */
global.RouterHelper = RouterHelper;
/**
 * @菜单管理
 */
global.MenuManager = MenuManager;
/**
 * @轻提示
 */
global.ToastManager = ToastManager;
/**
 * @常量
 */
global.Constants = Constants;
/**
 * @操作管理
 */
global.ActionsManager = ActionsManager
/**
 * @布局动画管理
 */
global.LayoutAnimationManager = LayoutAnimationManager
/**
 * @弹窗
 */
global.AlertManager = AlertManager
/**
 * @交互管理 系统的有bug,https://github.com/facebook/react-native/issues/8624
 */
global.InteractionManager = InteractionManager
/**
 * @全局的主题和控件的配置以及样式
 */
global.Theme = Theme

