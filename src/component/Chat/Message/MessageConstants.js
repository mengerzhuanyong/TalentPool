
'use strict';
/**
 * @消息的方向 []
 */
export const Direction = {
    Send: 'Send',
    Receive: 'Receive',
}
/**
 * @聊天类型 []
 */
export const ChatType = {
    Single: 'Chat',
    Group: 'GroupChat',
    Room: 'ChatRoom',
}
/**
 * @消息状态 []
 */
export const Status = {
    Pending: 'Pending',
    Delivering: 'Delivering',
    Succeed: 'Succeed',
    Failed: 'Failed',
}

/**
 * @消息状态 []
 */
export const MessageType = {
    Text: 'Text',
    Image: 'Image',
    Video: 'Video',
    Location: 'Location',
    Voice: 'Voice',
    File: 'File',
    Cmd: 'Cmd',
}

