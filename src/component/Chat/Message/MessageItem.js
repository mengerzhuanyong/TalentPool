'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types'
import { ChatType, Direction, Status, MessageType } from './MessageConstants'
import MessageAvatar from './MessageAvatar';
import MessageText from './MessageText';
import MessageBubble from './MessageBubble';
import MessageTime from './MessageTime'
import MessageName from './MessageName';
import MessageVoice from './MessageVoice';

export default class MessageItem extends React.PureComponent {

    static propTypes = {
        chatType: PropTypes.oneOf([ChatType.Single, ChatType.Group, ChatType.Room]),// 聊天类型
        status: PropTypes.oneOf([
            Status.Delivering,
            Status.Pending,
            Status.Succeed,
            Status.Failed,
        ]),
        body: PropTypes.shape({
            type: PropTypes.oneOf([
                MessageType.Text,
                MessageType.Image,
                MessageType.Video,
                MessageType.Location,
                MessageType.Voice,
                MessageType.File,
                MessageType.Cmd
            ]),
            data: PropTypes.oneOfType([
                PropTypes.shape({ content: PropTypes.string }),
                PropTypes.shape({
                    remotePath: PropTypes.string,
                    localPath: PropTypes.string,
                    fileLength: PropTypes.number,
                    duration: PropTypes.number,
                    displayName: PropTypes.string,
                })
            ])
        }),
        messageId: PropTypes.string,
        conversationId: PropTypes.string,
        from: PropTypes.string,
        to: PropTypes.string,
        timestamp: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        localTime: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        direction: PropTypes.oneOf([Direction.Send, Direction.Receive]),
        isReadAcked: PropTypes.bool,
        isDeliverAcked: PropTypes.bool,
        isRead: PropTypes.bool,
        ext: PropTypes.object,

        onPressAvatar: PropTypes.func,
        onLongPressAvatar: PropTypes.func,
    }

    static defaultProps = {
        chatType: ChatType.Single,
        body: { type: MessageType.Text, body: '' },
        direction: Direction.Send,
    }

    constructor(props) {
        super(props);
    }

    _onPressAvatar = () => {
        const { onPressAvatar, onLongPressAvatar, ...others } = this.props
        onPressAvatar && onPressAvatar({ ...others })
    }

    _onLongPressAvatar = () => {
        const { onPressAvatar, onLongPressAvatar, ...others } = this.props
        onLongPressAvatar && onLongPressAvatar({ ...others })
    }

    /**
     * @TODO与聊天组件无关 [不需要时删除] 
     */
    renderGiftMessage = () => {
        const { direction, body } = this.props
        let bubbleStyle = null
        if (direction === Direction.Send) {
            bubbleStyle = styles.messageSend
        } else {
            bubbleStyle = styles.messageReceive
        }
        return (
            <MessageBubble
                style={[bubbleStyle, { backgroundColor: 'transparent', marginTop: 10, paddingHorizontal: 20, paddingVertical: 5 }]}
                resizeMode={'stretch'}
                source={Images.icon_chat_gift_bg}
            >
                <MessageText style={{ color: '#fff' }} content={body.data.content} />
            </MessageBubble>
        )
    }

    renderMessageType = () => {
        const { direction, ext, body } = this.props
        /**
         * @TODO与聊天组件无关 [不需要时删除] 
         */
        if (ext.gift) {
            return this.renderGiftMessage()
        }

        // 以下是渲染聊天组件ITEM
        let messageChild = null, bubbleStyle = null
        if (direction === Direction.Send) {
            bubbleStyle = styles.messageSend
        } else {
            bubbleStyle = styles.messageReceive
        }
        if (body.type === MessageType.Text) {
            bubbleStyle = StyleSheet.flatten([styles.textBubble, bubbleStyle])
            messageChild = (
                <MessageText content={body.data.content} />
            )
        } else if (body.type === MessageType.Voice) {
            bubbleStyle = StyleSheet.flatten([styles.voiceBubble, bubbleStyle])
            messageChild = (
                <MessageVoice
                    remotePath={body.data.remotePath}
                    localPath={body.data.localPath}
                    fileLength={body.data.fileLength}
                    duration={body.data.duration}
                    displayName={body.data.displayName}
                />
            )
        }
        return (
            <MessageBubble style={bubbleStyle}>
                {messageChild}
            </MessageBubble>
        )
    }

    render() {
        const {
            ext,
            direction,
            showMessageTime,
            ...others
        } = this.props
        return direction === Direction.Receive ?
            <View style={styles.messageContainer}>
                <MessageAvatar
                    source={{ uri: ext.avatar }}
                    onPressAvatar={this._onPressAvatar}
                    onLongPressAvatar={this._onLongPressAvatar}
                />
                <View style={styles.messageReceiveContainer}>
                    <MessageName
                        style={styles.messageReceive}
                        name={ext.nickname}
                        ext={ext}
                        direction={direction}
                    />
                    {this.renderMessageType()}
                </View>
            </View >
            :
            <View style={[styles.messageContainer, { justifyContent: 'flex-end' }]}>
                <View style={styles.messageSendContainer}>
                    <MessageName
                        style={styles.messageSend}
                        name={ext.nickname}
                        ext={ext}
                        direction={direction}
                    />
                    {this.renderMessageType()}
                </View>
                <MessageAvatar
                    source={{ uri: ext.avatar }}
                    onPressAvatar={this._onPressAvatar}
                    onLongPressAvatar={this._onLongPressAvatar}
                />
            </View>
    }
}

const styles = StyleSheet.create({
    messageContainer: {
        flexDirection: 'row',
        marginHorizontal: 10,
        marginVertical: 12,
        alignItems: 'flex-start',
    },
    messageSend: {
        alignSelf: 'flex-end'
    },
    messageReceive: {
        alignSelf: 'flex-start'
    },
    messageReceiveContainer: {
        marginLeft: 10,
        marginRight: 100,
    },
    messageSendContainer: {
        marginLeft: 100,
        marginRight: 10,
    },
    sysTime: {
        alignSelf: "center",
        marginTop: 5,
    },
    textBubble: {
        marginTop: 10,
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    voiceBubble: {
        marginTop: 10,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },

});