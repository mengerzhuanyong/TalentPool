'use strict';
import React from 'react';
import { View, Text, StyleSheet, } from 'react-native';
import PropTypes from 'prop-types'
import Button from '../../Touchable/Button';
import SoundManager from '../../../config/manager/SoundManager'
import ImageView from '../../Image/ImageView';
import LottieView from 'lottie-react-native';

let _currentLottieRef = null

export default class MessageVoice extends React.PureComponent {

    static propTypes = {
        remotePath: PropTypes.string,
        localPath: PropTypes.string,
        fileLength: PropTypes.number,
        duration: PropTypes.number,
        displayName: PropTypes.string,
    }

    static defaultProps = {

    }

    componentWillUnmount() {
        this.resetAnimated()
        _currentLottieRef = null
    }

    playAnimated = () => {
        this.resetAnimated()
        if (_currentLottieRef) {
            _currentLottieRef.reset()
        }
        _currentLottieRef = this._lottieRef
        this._lottieRef && this._lottieRef.play()
    }

    resetAnimated = () => {
        this._lottieRef && this._lottieRef.reset()
    }

    _onPress = () => {
        const { localPath, remotePath } = this.props
        this.playAnimated()
        let soundPath = localPath;
        if (localPath.indexOf('tmp.aac') != -1) {
            soundPath = remotePath;
        }
        SoundManager.startSound(soundPath, () => {
            this.resetAnimated()
            _currentLottieRef = null
        })
    }

    _captureRef = (node) => {
        if (node) {
            this._lottieRef = node
        }
    }

    render() {
        const { duration } = this.props
        return (
            <Button style={[styles.button, { width: 50 + duration * 5 }]} onPress={this._onPress}>
                <LottieView
                    ref={this._captureRef}
                    style={styles.gifImage}
                    resizeMode={'cover'}
                    loop={true}
                    autoSize={false}
                    autoPlay={false}
                    source={Images.json_voice}
                />
                <Text style={styles.duration}>{`${duration}s`}</Text>
            </Button>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'transparent',
        borderRadius: 0,
        paddingVertical: 10,
        paddingHorizontal: 15,
        justifyContent: 'flex-start',
        minWidth: 80,
        maxWidth: Theme.screenWidth - 100,
    },
    gifImage: {
        width: 17,
        height: 17,
        // backgroundColor: '#fff',
        marginRight: 10,
    },
    duration: {
        fontSize: 11,
        color: "#9a9a9a"
    }
});