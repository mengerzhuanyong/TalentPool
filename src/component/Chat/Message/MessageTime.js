'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types'

export default class MessageTime extends React.PureComponent {



    render() {
        const { style } = this.props
        return (
            <Text style={[styles.time, style]}>{Moment().format('YYYY-MM-DD HH:mm:ss')}</Text>
        );
    }
}

const styles = StyleSheet.create({
    time: {
        fontSize: 12,
        color: '#333'
    }
});