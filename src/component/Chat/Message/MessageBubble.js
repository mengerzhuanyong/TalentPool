'use strict';
import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Image, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types'


export default class MessageBubble extends React.PureComponent {

    static propTypes = {
        style: ViewPropTypes.style,
        source: Image.propTypes.source,
        resizeMode: Image.propTypes.resizeMode,
    }

    static defaultProps = {

    }

    render() {
        const {
            style,
            children,
            source,
            ...others
        } = this.props
        return (
            <ImageBackground
                style={[styles.imageBackground, style]}
                source={source}
                {...others}
            >
                {children}
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    imageBackground: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 35,
    }
});