'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types'
import ImageView from '../../Image/ImageView';
import Button from '../../Touchable/Button'

export default class MessageAvatar extends React.PureComponent {

    static propTypes = {
        style: ImageView.propTypes.style,
        source: PropTypes.shape({ uri: PropTypes.string }),

        onPressAvatar: PropTypes.func,
        onLongPressAvatar: PropTypes.func,
    }

    static defaultProps = {

    }



    render() {
        const {
            style,
            onPressAvatar,
            onLongPressAvatar,
            ...others
        } = this.props
        return (
            <Button style={styles.button} onPress={onPressAvatar} onLongPress={onLongPressAvatar}>
                <ImageView
                    style={[styles.avatar, style]}
                    resizeMode={'cover'}
                    {...others}
                />
            </Button>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'transparent',
        borderRadius: 0,
        paddingVertical: 0,
        paddingHorizontal: 0,
    },
    avatar: {
        width: 40,
        height: 40,
    }
});