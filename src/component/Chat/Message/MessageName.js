'use strict';
import React from 'react';
import { View, Text, StyleSheet, ImageBackground, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types'

export default class MessageName extends React.PureComponent {

    static propTypes = {
        style: ViewPropTypes.style,
        name: PropTypes.string,
    }

    static defaultProps = {

    }

    /**
     * @TODO与聊天组件无关 [不需要时删除]
     */
    renderVip = () => {
        const { ext } = this.props
        const vipContainer = {
            borderRadius: 3,
            paddingHorizontal: 7,
            paddingVertical: 2,
            justifyContent: 'center',
            alignItems: 'center',
            overflow: 'hidden',
            marginHorizontal: 5,
            backgroundColor: 'red',
        }
        const vipText = {
            fontSize: 8,
            color: "#ffffff"
        }
        return (
            <ImageBackground style={vipContainer} source={Images.icon_vip}>
                <Text style={vipText}>{`${ext.level_name}`}</Text>
            </ImageBackground>
        )
    }

    render() {
        const { style, name, direction } = this.props
        return (
            <View style={[styles.container, style]}>
                {/**
                * @TODO与聊天组件无关 [不需要时删除]
                */}
                {direction === 'Send' ? this.renderVip() : null}
                <Text style={styles.text}>{name}</Text>
                {/**
                * @TODO与聊天组件无关 [不需要时删除]
                */}
                {direction === 'Receive' ? this.renderVip() : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    text: {
        fontSize: 13,
        color: '#fff'
    }
});