'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types'
import { ChatType, Direction, Status, MessageType } from './MessageConstants'

export default class MessageText extends React.PureComponent {

    static propTypes = {
        style: Text.propTypes.style,
        content: PropTypes.string,
    }

    static defaultProps = {

    }

    constructor(props) {
        super(props)
    }

    render() {
        const { style, content } = this.props
        return (
            <Text style={[styles.text, style]}>{content}</Text>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        fontSize: 13,
        lineHeight: 20,
        color: '#333'
    }
});