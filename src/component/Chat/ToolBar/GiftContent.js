'use strict';
import React from 'react';
import { View, Text, StyleSheet, ViewPropTypes, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types'
import Banner from '../../Common/Banner';
import ImageView from '../../Image/ImageView';
import Button from '../../Touchable/Button';
import Theme from '../../../config/themes/Theme';
import Stepper from 'teaset/components/Stepper/Stepper';
import { GiftBottomHeight } from './ToolConstants'
import WrapContainer from '../../Container/WrapContainer';
import { MessageType } from '../Message/MessageConstants';

/**
 * @TODO与聊天组件无关 [不需要时删除] 
 */
export default class GiftContent extends React.PureComponent {

    static propTypes = {
        style: ViewPropTypes.style
    };

    static defaultProps = {
        style: {}
    };

    constructor(props) {
        super(props);
        this.state = { oriDataSource: [], dataSource: [], selectId: 0, value: 1 }
    }


    componentDidMount() {
        this.requestDataSource()
    }

    requestDataSource = async () => {
        const url = ServicesApi.GIFT_INDEX;
        const data = {};
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            // result.data = result.data.concat(result.data)
            // result.data = result.data.concat(result.data)
            // result.data = result.data.concat(result.data)
            // result.data = result.data.concat(result.data)
            const bigIndex = result.data.length / 8;
            let dataSource = []
            for (let index = 0; index < bigIndex; index++) {
                const element = result.data.slice(index * 8, index * 8 + 8);
                dataSource.push(element)
            }
            this.setState({ dataSource: dataSource, oriDataSource: result.data })
        }
    }

    _onPressItem = (item) => {
        const { selectId } = this.state
        if (selectId !== item.id) {
            this.setState({ selectId: item.id })
        }
    }

    _onPressSend = async () => {
        const { oriDataSource, selectId, value } = this.state
        const { onGiftSend } = this.props
        let giftItem = oriDataSource.find((it) => it.id === selectId)
        Log.print('标记', giftItem)
        if (!giftItem) {
            ToastManager.message('请选择礼物');
            return;
        }
        onGiftSend && onGiftSend({
            type: MessageType.Text,
            data: {
                content: `送给房主 ${value} 个${giftItem.title}`,
                ext: {
                    gift: {
                        ...giftItem,
                        count: value
                    }
                }
            }
        })
    }

    _onChangeStepper = (value) => {
        this.setState({ value: value })
    }

    render() {
        const { style, senderInfo } = this.props
        const { dataSource } = this.state
        if (dataSource.length === 0) {
            return null
        }
        Log.print('Banner', senderInfo, senderInfo.balance)
        return (
            <View style={[styles.container, style]}>
                <Banner
                    loop={false}
                    autoplay={false}
                    style={styles.banner}
                    removeClippedSubviews={false}
                    width={style.width}
                    height={style.height - GiftBottomHeight}
                    paginationStyle={styles.paginationStyle}
                >
                    {dataSource.map((item, index) => {
                        return (
                            <WrapContainer
                                key={`gift_${index}`}
                                style={styles.wrapContainer}
                                itemColumnMargin={10}
                                itemRowMargin={0}
                                numColumns={4}>
                                {item.map((it, ii) => {
                                    let seletStyle = null
                                    const { selectId } = this.state
                                    if (selectId === it.id) {
                                        seletStyle = styles.seletStyle
                                    }
                                    return (
                                        <TouchableOpacity
                                            style={[styles.warpItemCon, seletStyle]}
                                            onPress={() => this._onPressItem(it)}
                                            key={`gift_${index}-${ii}`}>
                                            <ImageView
                                                style={styles.giftImage}
                                                source={{ uri: it.thumb }}
                                            />
                                            <Text style={styles.giftTitle}>{it.title}</Text>
                                            <Text style={styles.giftPice}>{it.price}</Text>
                                        </TouchableOpacity>
                                    )
                                })}
                            </WrapContainer>
                        )
                    })}
                </Banner>
                <View style={styles.bottomContainer}>
                    <View style={styles.bottomLeft}>
                        <View style={styles.bottomLeftSub}>
                            <Text style={styles.bottomAmountText}>{`金币余额：${senderInfo.balance}`}</Text>
                            <Button
                                style={[Theme.buttonStyle, styles.goReButton]}
                                title={'去充值'}
                                titleStyle={styles.goReText}
                                onPress={() => RouterHelper.navigate('', 'Wallet')}
                            />
                        </View>
                        <View style={styles.sep} />
                        <View style={styles.bottomRightSub}>
                            <Text style={styles.countText}>数量：</Text>
                            <Stepper
                                style={styles.stepper}
                                min={1}
                                defaultValue={1}
                                subButton={
                                    <View style={styles.subView}>
                                        <Text style={styles.subText}>－</Text>
                                    </View>
                                }
                                addButton={
                                    <View style={styles.subView}>
                                        <Text style={styles.subText}>＋</Text>
                                    </View>
                                }
                                valueStyle={styles.stepperValue}
                                onChange={this._onChangeStepper}
                            />
                        </View>
                    </View>
                    <Button
                        style={styles.sendButton}
                        title={'发送'}
                        titleStyle={styles.senTitle}
                        onPress={this._onPressSend}
                    />
                </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#2a2628',
        paddingVertical: 7,
    },
    bottomContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: GiftBottomHeight,
        // backgroundColor: 'red',
    },
    bottomLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'rgba(216, 216, 216, 0.2)',
        paddingHorizontal: 10,
        borderRadius: 40,
        height: GiftBottomHeight - 5
    },
    bottomLeftSub: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    bottomAmountText: {
        fontSize: 12,
        color: "#8b8a8b"
    },
    bottomRightSub: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    goReButton: {
        marginLeft: 10
    },
    goReText: {
        fontSize: 14,
        color: "#ff914d"
    },
    sep: {
        width: 1,
        height: '100%',
        backgroundColor: '#636363',
        marginHorizontal: 15,
    },
    countText: {
        fontSize: 14,
        color: "#8b8a8b"
    },
    stepper: {
        backgroundColor: 'transparent',
        borderWidth: 0,
    },
    stepperValue: {
        fontSize: 18,
        color: "#ffffff",
        minWidth: 40
    },
    subView: {
        borderColor: '#fff',
        borderWidth: 1,
        borderRadius: 10,
        width: 15,
        height: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    subText: {
        fontSize: 12,
        color: "#fff",
    },
    sendButton: {
        borderRadius: 20,
        backgroundColor: "rgba(216, 216, 216, 0.2)",
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    senTitle: {
        fontSize: 14,
        color: "#ff914d"
    },
    wrapContainer: {
        // flex: 1,
        // backgroundColor: 'red',
        marginHorizontal: 10
    },
    warpItemCon: {
        // backgroundColor: 'blue',
        paddingVertical: 5,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'transparent'
    },
    giftImage: {
        width: 40,
        height: 50
    },
    giftTitle: {
        marginTop: 5,
        fontSize: 11,
        color: '#fff'
    },
    giftPice: {
        marginTop: 2,
        fontSize: 11,
        color: '#999'
    },
    paginationStyle: {
        bottom: 2
    },
    seletStyle: {
        borderWidth: 1,
        borderColor: 'red'
    }
});