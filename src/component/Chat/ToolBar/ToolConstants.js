
'use strict';
import Theme from "../../../config/themes/Theme";

/**
 * @TODO与聊天组件无关 [不需要时删除] 
 */
export const GiftBottomHeight = 45

/**
 * @bar的高度 
 */
export const BarHeight = 45;
export const BarContainerHeight = Theme.isIPhoneX ? 250 + Theme.fitIPhoneXBottom : 250;


/**
 * @录音 [详细描述]
 */
export const Voice = {
    MoveToCancelEmitter: 'MoveToCancelEmitter',
    StatusIng: 'StatusIng',
    StatusCancel: 'StatusCancel',
}


