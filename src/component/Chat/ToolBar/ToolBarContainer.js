'use strict';
import React from 'react';
import { View, Text, StyleSheet, Keyboard, LayoutAnimation, TouchableOpacity, DeviceEventEmitter, PanResponder } from 'react-native';
import PropTypes from 'prop-types'
import InputBar from './InputBar';
import Button from '../../Touchable/Button';
import OtherContent from './OtherContent';
import { BarHeight, BarContainerHeight } from './ToolConstants';
import VoiceButton from './VoiceButton';
import Theme from '../../../config/themes/Theme';
import { scaleSize } from '../../../util/Adaptation';
import { MessageType } from '../Message/MessageConstants';

export default class ToolBarContainer extends React.PureComponent {

    static propTypes = {
        onSend: PropTypes.func
    }

    static defaultProps = {

    };

    constructor(props) {
        super(props)
        this.state = { bottom: 0 }
        this._showInput = false;
        this._showOthers = false;
        this._showVoice = false;
        this._containerFrame = {};
        this._keyboardFrame = {};
        this._subscriptions = [];
    }

    componentDidMount() {
        if (__IOS__) {
            this._subscriptions = [
                Keyboard.addListener('keyboardWillChangeFrame', this._onKeyboardChange),
            ];
        } else {
            this._subscriptions = [
                Keyboard.addListener('keyboardDidShow', this._onKeyboardChange),
                Keyboard.addListener('keyboardDidHide', this._onKeyboardChange),
            ];
        }
    }

    componentWillUnmount() {
        this._subscriptions.forEach(subscription => {
            subscription.remove();
        });
    }

    barDown = () => {
        if (this._showOthers) {
            LayoutAnimation.configureNext({
                duration: 200,
                update: {
                    duration: 200,
                    springDamping: 500,
                    type: LayoutAnimation.Types.spring,
                },
            });
            this.setState({ bottom: 0 })
            this._showOthers = false
        }
    }

    _onSubmitEditing = (event) => {
        const { onSend } = this.props
        this._inputBar.clear()
        onSend && onSend({
            type: MessageType.Text,
            data: {
                content: event.nativeEvent.text
            }
        })
    }

    _onSendVoice = (data) => {
        const { onSend } = this.props
        onSend && onSend({
            type: MessageType.Voice,
            data: {
                path: data.path,
                displayName: data.displayName,
                duration: data.duration,
            }
        })
    }

    _onChangeText = () => {

    }

    _onPressOthers = () => {
        // 当输入框聚焦是，需要关闭输入框
        if (this._showInput) {
            this._showOthers = !this._showOthers
            Keyboard.dismiss()
            LayoutAnimation.configureNext({
                duration: 200,
                update: {
                    duration: 200,
                    springDamping: 500,
                    type: LayoutAnimation.Types.spring,
                },
            });
            this.setState({ bottom: BarContainerHeight });
            Log.print('_showOthers', this._showOthers)
        } else {
            LayoutAnimation.configureNext({
                duration: 200,
                update: {
                    duration: 200,
                    springDamping: 500,
                    type: LayoutAnimation.Types.spring,
                },
            });
            Log.print('_showOthers', this._showOthers)
            Log.print('_showInput', this._showInput)
            // 
            if (this._showOthers) {
                this.setState({ bottom: 0 })
            } else {
                this._showVoice = false
                this.setState({ bottom: BarContainerHeight });
            }
            this._showOthers = !this._showOthers
        }
    }

    _onPressVoice = () => {
        this._showVoice = !this._showVoice
        this._showInput = false;
        if (this._showOthers) {
            this._showOthers = false;
            LayoutAnimation.configureNext({
                duration: 200,
                update: {
                    duration: 200,
                    springDamping: 500,
                    type: LayoutAnimation.Types.spring,
                },
            });
            this.setState({ bottom: 0 });
        }
        this.forceUpdate()
    }

    _relativeKeyboardHeight = (keyboardFrame) => {
        const frame = this._containerFrame;
        if (!frame || !keyboardFrame) {
            return 0;
        }
        const keyboardY = keyboardFrame.screenY - BarHeight - (Theme.isIPhoneX ? scaleSize(18) : scaleSize(30));
        return Math.max(frame.y + frame.height - keyboardY, 0);
    }

    _onKeyboardChange = (event) => {
        if (__ANDROID__ && event == null) {
            return;
        }
        const { duration, easing, endCoordinates } = event;
        Log.print('标记', endCoordinates)
        if (endCoordinates) {
            this._keyboardFrame = endCoordinates
        }
        if (__ANDROID__) {
            return;
        }
        // 当其他类目开启时，不需要将Bar降落
        if (this._showOthers) {
            return;
        }
        if (event == null) {
            this.setState({ bottom: 0 });
            return;
        }
        const height = this._relativeKeyboardHeight(endCoordinates);
        Log.print('标记', this.state.bottom, height)
        if (this.state.bottom === height) {
            return;
        }
        if (duration && easing) {
            LayoutAnimation.configureNext({
                duration: duration > 10 ? duration : 10,
                update: {
                    duration: duration > 10 ? duration : 10,
                    type: LayoutAnimation.Types[easing] || 'keyboard',
                },
            });
        }
        this.setState({ bottom: height });
    };

    _onBlur = () => {
        this._showInput = false
        if (__ANDROID__ && this._showOthers) {
            // LayoutAnimation.configureNext({
            //     duration: 10000,
            //     update: {
            //         duration: 10000,
            //         springDamping: 7,

            //         type: LayoutAnimation.Types.spring,
            //     },
            // });
            Log.print('_showOthers', this._showOthers)
            Log.print('_showInput', this._showInput)
            setTimeout(() => {
                this.cotainerView.setNativeProps({
                    style: {
                        // position: 'absolute',
                        paddingBottom: BarContainerHeight
                    }
                })
            }, 50);
            // this.cotainerView.blur()

            // this.setState({ bottom: this._keyboardFrame.height || BarContainerHeight });
        }
    }

    _onFocus = () => {
        this._showInput = true
        // 当输入框聚焦时，其他类目需要关闭
        if (__ANDROID__ && this._showOthers) {
            // LayoutAnimation.configureNext({
            //     duration: 200,
            //     update: {
            //         duration: 200,
            //         springDamping: 7,

            //         type: LayoutAnimation.Types.spring,
            //     },
            // });
            Log.print('_showOthers', this._showOthers)
            Log.print('_showInput', this._showInput)
            // this.cotainerView.focus()
            this.cotainerView.setNativeProps({
                style: {
                    // position: 'absolute',
                    paddingBottom: 0
                }
            })
            // this.setState({ bottom: 0 })
            // this.setState({ bottom: 0 })
        }

        this._showOthers = false
    }

    _onLayout = (event) => {
        this._containerFrame = event.nativeEvent.layout;
    };

    _captureInputBarRef = (node) => {
        if (node) {
            this._inputBar = node
        }
    }

    render() {
        const { onGiftSend, senderInfo } = this.props
        const { bottom } = this.state
        let bottomHeight = bottom, height = BarHeight, paddingBottom = 0
        if (bottom === 0 && Theme.isIPhoneX) {
            bottomHeight = 0
            height = BarHeight + Theme.fitIPhoneXBottom
            paddingBottom = Theme.fitIPhoneXBottom
        } else {
            bottomHeight = Theme.isIPhoneX ? Theme.fitIPhoneXBottom + bottom : bottom
            height = BarHeight
            paddingBottom = 0
        }
        return (
            <View
                ref={(v) => this.cotainerView = v}
                style={[styles.container, {
                    paddingBottom: bottomHeight
                }]}
                onLayout={this._onLayout}
            >
                <View style={[styles.barContainer, { height, paddingBottom }]}>
                    <Button
                        style={styles.voiceButton}
                        icon={Images.icon_chat_voice}
                        onPress={this._onPressVoice}
                    />
                    {this._showVoice ?
                        <VoiceButton
                            onSendVoice={this._onSendVoice}
                        />
                        :
                        <InputBar
                            ref={this._captureInputBarRef}
                            style={styles.input}
                            onBlur={this._onBlur}
                            onFocus={this._onFocus}
                            onChangeText={this._onChangeText}
                            onSubmitEditing={this._onSubmitEditing}
                        />}
                    <Button
                        style={styles.othersButton}
                        icon={Images.icon_chat_gift}
                        onPress={this._onPressOthers}
                    />
                </View>
                <View style={styles.contentContainer}>
                    <OtherContent senderInfo={senderInfo} onGiftSend={onGiftSend} />

                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#2a2628',
    },
    barContainer: {
        flexDirection: 'row',
        backgroundColor: "rgba(0, 0, 0, 0.5)",
        paddingHorizontal: 10,
        zIndex: 100
    },
    contentContainer: {

    },
    voiceButton: {
        backgroundColor: 'transparent',
        borderRadius: 0,
        paddingVertical: 0,
        paddingHorizontal: 0,
        alignSelf: 'center',
    },
    othersButton: {
        backgroundColor: 'transparent',
        borderRadius: 0,
        paddingVertical: 0,
        paddingHorizontal: 0,
        alignSelf: 'center',
    },
    input: {
        borderRadius: 15,
        padding: 0,
        paddingHorizontal: 20,
        marginVertical: 7,
        marginHorizontal: 15,
        backgroundColor: '#fff',
    },

});