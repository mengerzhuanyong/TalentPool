'use strict';
import React from 'react';
import { View, Text, StyleSheet, PanResponder, DeviceEventEmitter, ImageBackground, UIManager, findNodeHandle } from 'react-native';
import PropTypes from 'prop-types'
import VoiceView from './VoiceView';
import { Voice } from './ToolConstants';
import ImageView from '../../Image/ImageView';
import RecordManager from '../../../config/manager/RecordManager';

export default class VoiceButton extends React.PureComponent {

    static propTypes = {

    };

    static defaultProps = {

    };

    constructor(props) {
        super(props)
        this.state = {}
        this._showVoiceView = false;
        this._moveToCancel = false;
        this._voiceTime = 0
        this.createPanResponder()
    }

    createPanResponder() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: this._onPanResponderGrant,
            onPanResponderMove: this._onPanResponderMove,
            onPanResponderRelease: this._onPanResponderRelease,
            onPanResponderTerminate: this._onPanResponderTerminate,
        });
    }

    _onVoiceTime = (time) => {
        this._voiceTime = time
    }

    startRecording = () => {
        // 配置录音
        return RecordManager.startRecord((data) => {
            Log.print('progress - Record', data)
        })
    }

    stopRecording = (isCancel) => {
        const { onSendVoice } = this.props
        RecordManager.stopRecord((data) => {
            Log.print('stopRecord', data)
            !isCancel && onSendVoice && onSendVoice({
                path: RecordManager.audioPath,
                displayName: `${Moment().format('x')}.aac`,
                duration: this._voiceTime,
            })
        })
    }

    _onPanResponderGrant = async (event, gestureState) => {
        if (!this._showVoiceView) {
            this._showVoiceView = true
            Log.print('开始语音')
            const result = await this.startRecording()
            if (result) {
                AlertManager.showPopView(<VoiceView onTime={this._onVoiceTime} />, { overlayOpacity: 0, type: 'none' })
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }

    _onPanResponderMove = (event, gestureState) => {
        let { locationY } = event.nativeEvent
        if (__ANDROID__) {
            locationY = gestureState.dy + 30
        }
        if (locationY < 0) {
            if (!this._moveToCancel) {
                Log.print('上划取消')
                DeviceEventEmitter.emit(Voice.MoveToCancelEmitter, true)
                this._moveToCancel = true;
            }
        } else {
            Log.print('正在录音')
            if (this._moveToCancel) {
                DeviceEventEmitter.emit(Voice.MoveToCancelEmitter, false)
            }
            this._moveToCancel = false;
        }

    }

    _onPanResponderRelease = (event, gestureState) => {
        AlertManager.hide()
        if (this._moveToCancel) {
            Log.print('结束录音-取消')
            this.stopRecording(true)
        } else {
            Log.print('结束录音-发送')
            this.stopRecording(false)
        }
        this._showVoiceView = false
        this._moveToCancel = false
    }

    _onPanResponderTerminate = (event) => {
        Log.print('终端录音')

    };

    _onLayout = (event) => {
        const nativeEvent = event.nativeEvent
        console.log('_onLayout', event.nativeEvent)

    }

    render() {
        return (
            <View style={styles.voiceTap} onLayout={this._onLayout} {...this._panResponder.panHandlers}>
                <ImageBackground
                    resizeMode={'stretch'}
                    style={styles.imageBack}
                    source={Images.img_bg_navbar}
                />
                <Text style={styles.voiceTapTitle}>按住说话</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    voiceTap: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 0,
        marginHorizontal: 15,
        marginVertical: 7,
        borderRadius: 40,
        backgroundColor: '#333',
        overflow: 'hidden',
    },
    voiceTapTitle: {
        fontSize: 12,
        color: '#fff'
    },
    imageBack: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    }
});