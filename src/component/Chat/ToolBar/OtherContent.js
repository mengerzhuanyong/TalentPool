'use strict';
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import SyanImagePicker from 'react-native-syan-image-picker';
import ImagePicker from 'react-native-image-picker';
import { BarHeight, BarContainerHeight } from './ToolConstants';
import GiftContent from './GiftContent';
import Theme from '../../../config/themes/Theme';

export default class AddContent extends React.PureComponent {

    static propTypes = {

    };

    static defaultProps = {

    };

    constructor(props) {
        super(props);


    }

    render() {
        const { senderInfo, onGiftSend } = this.props
        return (
            <View
                ref={this._captureRef}
                style={[styles.container, { height: BarContainerHeight }]}>
                {/**
                * @TODO与聊天组件无关 [不需要时删除]
                */}
                <GiftContent
                    style={styles.giftContent}
                    senderInfo={senderInfo}
                    onGiftSend={onGiftSend}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        // borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: '#ebeced',
        backgroundColor: '#fff',
        justifyContent: 'space-around',
        flexDirection: 'row',
        // flexWrap: 'wrap',
        left: 0,
        right: 0,

    },
    itemContainer: {
        alignItems: 'center',
        // backgroundColor: 'blue',
    },
    itemImage: {
        width: 50,
        height: 50,
        backgroundColor: 'red',
    },
    itemTitleText: {
        fontSize: 12,
        marginTop: 8,
    },
    giftContent: {
        width: Theme.screenWidth,
        height: BarContainerHeight
    }
});
