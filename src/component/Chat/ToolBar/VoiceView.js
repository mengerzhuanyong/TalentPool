'use strict';
import React from 'react';
import { View, Text, StyleSheet, DeviceEventEmitter } from 'react-native';
import PropTypes from 'prop-types'
import { Voice } from './ToolConstants';
import { conversionSeconds } from '../Util';
import RecordManager from '../../../config/manager/RecordManager';

export default class VoiceView extends React.PureComponent {

    static propTypes = {

    }

    static defaultProps = {

    };

    constructor(props) {
        super(props)
        this.state = { status: Voice.StatusIng, voiceTime: 0 }
        this.voiceTimeout = null

    }

    componentDidMount() {
        this.startVoiceTime()
        DeviceEventEmitter.addListener(Voice.MoveToCancelEmitter, this._moveToCancel)
    }

    componentWillUnmount() {
        this.stopVoiceTime()
        DeviceEventEmitter.removeListener(Voice.MoveToCancelEmitter, this._moveToCancel)
    }

    _moveToCancel = (cancel) => {
        const { status } = this.state
        if (cancel) {
            if (status !== Voice.StatusCancel) {
                this.setState({ status: Voice.StatusCancel })
            }
        } else {
            if (status !== Voice.StatusIng) {
                this.setState({ status: Voice.StatusIng })
            }
        }
    }

    startVoiceTime = () => {
        const { onTime } = this.props
        this.stopVoiceTime()
        const startTime = Moment().format('X');
        this.voiceTimeout = setInterval(() => {
            const currentTime = Moment().format('X');
            this.setState({ voiceTime: currentTime - startTime }, () => {
                onTime && onTime(this.state.voiceTime)
            });
        }, 1000);
    }

    stopVoiceTime = () => {
        this.voiceTimeout && clearInterval(this.voiceTimeout)
    }

    render() {
        const { status, voiceTime } = this.state
        return (
            <View style={styles.container}>
                <Text style={styles.voiceTime}>{conversionSeconds(voiceTime)}</Text>
                <Text style={styles.status}> {status === Voice.StatusIng ? '正在录音' : '取消录音'}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: 150,
        height: 100,
        backgroundColor: 'rgba(35,24,21,0.8)',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12,
    },
    voiceTime: {
        fontSize: 30,
        color: '#fff'
    },
    status: {
        marginTop: 10,
        fontSize: 15,
        color: '#fff'
    },
});