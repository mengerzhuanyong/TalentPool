'use strict';
import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import PropTypes from 'prop-types'

export default class InputBar extends React.PureComponent {

    static propTypes = {
        ...TextInput.propTypes
    }

    static defaultProps = {
        ...TextInput.defaultProps
    };

    isFocused = () => {
        return this.textInputRef.isFocused()
    }

    focus = () => {
        if (!this.textInputRef.isFocused()) {
            this.textInputRef.focus()
        }
    };

    blur = () => {
        if (this.textInputRef.isFocused()) {
            this.textInputRef.blur()
        }
    };

    clear = () => {
        this.textInputRef.clear()
    };

    _captureRef = (v) => {
        this.textInputRef = v
    };

    render() {
        const { style, ...others } = this.props
        return (
            <TextInput
                ref={this._captureRef}
                style={[styles.input, style]}
                placeholder={'请输入'}
                blurOnSubmit={false}
                returnKeyType={'send'}
                returnKeyLabel={'发送'}
                {...others}
            />
        );
    }
}

const styles = StyleSheet.create({
    input: {
        flex: 1,
        padding: 0,
    }
});