export default [
    {
        messageId: `messageId${Moment().format('x') + 1}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x') + 1}`, // 所属会话的唯一标识符
        direction: 'Receive', // Send， Receive 消息的方向
        from: 'to', //  发送方
        to: 'me', //  接收方 
        timestamp: Moment().format('x') + 1, // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x') + 1, // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            // data: {content:''},
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
    {
        messageId: `messageId${Moment().format('x')}`, // 消息的唯一标识符
        conversationId: `conversationId${Moment().format('x')}`, // 所属会话的唯一标识符
        direction: 'Send', // Send， Receive 消息的方向
        from: 'me', //  发送方
        to: 'to', //  接收方 
        timestamp: Moment().format('x'), // 时间戳，服务器收到此消息的时间
        localTime: Moment().format('x'), // 客户端发送/收到此消息的时间
        chatType: 'Chat',
        status: 'Succeed', // Pending  Delivering Succeed Failed
        isReadAcked: false, // 已读回执是否已发送/收到, 对于发送方表示是否已经收到已读回执，对于接收方表示是否已经发送已读回执
        isDeliverAcked: false,
        isRead: false,
        body: {
            type: 'Text',
            data: '我是发送者的文字消息',
        }, // 消息内容
        ext: {},// 消息扩展
    },
]