
export function conversionSeconds(seconds) {
    let arr = [
        parseInt(seconds / 60 % 60),
        parseInt(seconds % 60)
    ]
    return arr.join(":").replace(/\b(\d)\b/g, "0$1");
}

export function conversionMessage(message) {
    if (Array.isArray(message)) {
        const newMessage = message.slice();
        newMessage.forEach((item) => {
            if (item.ext && item.ext.data) {
                item.ext = JSON.parse(item.ext.data)
            }
        })
        return newMessage
    } else if (typeof message === 'object') {
        if (message.ext && message.ext.data) {
            message.ext = JSON.parse(message.ext.data)
        }
        return message
    }
    return message
}

