'use strict';
import { DeviceEventEmitter, PermissionsAndroid, Platform } from 'react-native';
import HyphenateManager from '../Hyphenate/HyphenateManager'
import { CallType, CallStatus, CallIdentity, EventName } from './RTCConstants'

export default class RTCManager {

    static async requestPermissions() {
        if (Platform.OS === 'ios') {
            return Promise.resolve(true);
        }
        const result = Promise.all([
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, null),
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, null),
        ])
        if ((result[0] || result[0] === PermissionsAndroid.RESULTS.GRANTED) &&
            (result[1] || result[1] === PermissionsAndroid.RESULTS.GRANTED)
        ) {
            return Promise.resolve(true);
        } else {
            return Promise.resolve(false);
        }
    }

    constructor() {
        this.startCallTime = 0 // 记录开始时间
        this.endCallTime = 0 // 记录结束时间
        this.currentStatus = CallStatus.None // 当前通话的状态
        this.currentIdentity = CallIdentity.None // 通话的身份
        this.currentCallType = CallType.None // 通话类型
        this.subscriptions = []
        this.initRTCLister()
    }

    initRTCLister = () => {
        this.subscriptions = [
            HyphenateManager.addCallDidReceiveListener(this._callDidReceive),
            HyphenateManager.addCallDidAcceptListener(this._callDidAccept),
            HyphenateManager.addCallDidEndListener(this._callDidEnd),
            HyphenateManager.addCallDidConnectListener(this._callDidConnect),
        ]
    }

    removeRTCLister = () => {
        // 清除监听
        this.subscriptions.forEach(subscription => {
            subscription && subscription.remove && subscription.remove();
        });
    }



    addMonitorStatus = (callBack) => {
        callBack && callBack(this.currentStatus)
        return DeviceEventEmitter.addListener(EventName.Status, callBack)
    }

    addMonitorIdentity = (callBack) => {
        callBack && callBack(this.currentIdentity)
        return DeviceEventEmitter.addListener(EventName.Identity, callBack)
    }

    addMonitorCallDidReceive = (callBack) => {
        return DeviceEventEmitter.addListener(EventName.CallDidReceive, callBack)
    }

    addMonitorCallDidAccept = (callBack) => {
        return DeviceEventEmitter.addListener(EventName.CallDidAccept, callBack)
    }

    addMonitorCallDidEnd = (callBack) => {
        return DeviceEventEmitter.addListener(EventName.CallDidEnd, callBack)
    }

    // 拨打电话
    startCall = async (type, tartgetName, ext = {}) => {
        if (type === 'video') {
            type = CallType.Video
        } else {
            type = CallType.Voice
        }
        const result = await HyphenateManager.startCall(type, tartgetName, ext)
        if (result.code === 'success') {
            this.setCurrentCallType(type)
            this.changeCallStatus(CallStatus.Callering)
            this.changeCallIdentity(CallIdentity.Caller)
        }
        return result
    }

    // 挂断电话
    endCall = async (callId) => {
        let reason = 0
        if (this.currentStatus === CallStatus.Calleding) {
            reason = 2 // 拒接通话
        } else {
            reason = 0 // 挂断通话
        }
        const result = await HyphenateManager.endCall(callId, reason)
        return result
    }

    // 同意电话
    answerIncomingCall = async (callId) => {
        if (this.currentStatus === CallStatus.Calleding) {
            const result = await HyphenateManager.answerIncomingCall(callId)
            if (result.code === 'success') {
                this.startCallTime = Moment().format('X')
                this.changeCallStatus(CallStatus.Calling)
                this.setCallingOption()
            }
            return result;
        }
    }

    setCallingOption = () => {
        Log.print('setCallingOption', this.currentCallType)
        if (this.currentCallType === CallType.Voice) {
            HyphenateManager.setIdleTimerDisabled(false)
            HyphenateManager.setSpeaker(false)
        } else if (this.currentCallType === CallType.Video) {
            HyphenateManager.setIdleTimerDisabled(true)
            HyphenateManager.setSpeaker(true)
        } else {
            HyphenateManager.setSpeaker(false)
            HyphenateManager.setIdleTimerDisabled(false)
        }
    }

    setCurrentCallType = (type) => {
        this.currentCallType = type
    }

    changeCallStatus = (status) => {
        this.currentStatus = status
        DeviceEventEmitter.emit(EventName.Status, this.currentStatus)
    }

    changeCallIdentity = (identity) => {
        this.currentIdentity = identity
        DeviceEventEmitter.emit(EventName.Identity, this.currentIdentity)
    }

    // 用户A拨打用户B，用户B会收到这个回调
    _callDidReceive = (data) => {
        this.setCurrentCallType(data.type)
        this.changeCallStatus(CallStatus.Calleding)
        this.changeCallIdentity(CallIdentity.Called)
        // 处理完之后，发送通知
        DeviceEventEmitter.emit(EventName.CallDidReceive, {
            ...data,
            ext: data.ext ? JSON.parse(data.ext) : {},
            callType: this.currentCallType,
            status: this.currentStatus,
            identity: this.currentIdentity
        })
    }

    // 用户B同意用户A拨打的通话后，用户A会收到这个回调
    _callDidAccept = (data) => {
        this.startCallTime = Moment().format('X')
        this.changeCallStatus(CallStatus.Calling);
        this.setCallingOption();
        // 处理完之后，发送通知
        DeviceEventEmitter.emit(EventName.CallDidAccept, {
            ...data,
            ext: data.ext ? JSON.parse(data.ext) : {},
            callType: this.currentCallType,
            status: this.currentStatus,
            identity: this.currentIdentity,
            startCallTime: this.startCallTime,
            endCallTime: this.endCallTime
        })
    }

    // 1. 用户A或用户B结束通话后，对方会收到该回调
    // 2. 通话出现错误，双方都会收到该回调
    _callDidEnd = (data) => {
        if (this.currentStatus !== CallStatus.None) {
            this.endCallTime = Moment().format('X')
            const oriType = this.currentCallType
            const oriIdentity = this.currentIdentity
            this.setCurrentCallType(CallType.None)
            this.changeCallStatus(CallStatus.None)
            this.changeCallIdentity(CallIdentity.None)
            this.setCallingOption()
            // 发送通知，需要发送之前的通话类型和身份
            DeviceEventEmitter.emit(EventName.CallDidEnd, {
                ...data,
                ext: data.ext ? JSON.parse(data.ext) : {},
                callType: oriType,
                identity: oriIdentity,
                status: this.currentStatus,
                startCallTime: this.startCallTime,
                endCallTime: this.endCallTime
            })
        }
    }

    // 通话通道建立完成，用户A和用户B都会收到这个回调
    _callDidConnect = (data) => {
        Log.print('_callDidConnect', data)
    }




}