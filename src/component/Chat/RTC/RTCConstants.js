
'use strict';
/**
 * @类型 []
 */
export const CallType = {
    None: 'None',
    Video: 'video',
    Voice: 'voice',
}

/**
 * @通话状态 []
 */
export const CallStatus = {
    None: 'None',
    Callering: 'Callering', // 主叫中
    Calleding: 'Calleding', // 被叫中
    Calling: 'Calling', // 通话中
}
/**
 * @通话身份 []
 */
export const CallIdentity = {
    None: 'None',
    Caller: 'Caller', // 主叫
    Called: 'Called', // 被叫
}
/**
 * @事件名字 []
 */
export const EventName = {
    Status: 'Status', // 状态
    Identity: 'Identity', // 身份
    CallDidReceive: 'CallDidReceive', // 
    CallDidAccept: 'CallDidAccept', //
    CallDidEnd: 'CallDidEnd', // 
    CallDidConnect: 'CallDidConnect', //
}


