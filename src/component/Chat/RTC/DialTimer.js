'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ImageView from '../../Image/ImageView'
import { conversionSeconds } from '../Util'

export default class DialTimer extends React.PureComponent {

    constructor(props) {
        super(props)
        this.state = { time: 0 }
    }

    componentDidMount() {
        const oriTime = Moment().format('X')
        this.timeInterval = setInterval(() => {
            const currentTime = Moment().format('X')
            this.setState({ time: currentTime - oriTime })
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timeInterval)
    }

    render() {
        const { time } = this.state
        return (
            <View style={styles.hintTop}>
                <Text style={styles.hintTimeTopText} numberOfLines={1}>{`计时：${conversionSeconds(time)}`}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    hintTop: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    hintTimeTopText: {
        fontSize: FontSize(12),
        color: '#ffffff'
    },
    hintTimeText: {
        fontSize: FontSize(12),
        color: '#ffffff'
    },
    notice: {
        marginTop: 15,
        paddingVertical: 8,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.5,
        backgroundColor: '#000000'
    },
    noticeImg: {
        width: 15,
        height: 15
    },
    noticeText: {
        fontSize: FontSize(12),
        color: '#ffffff',
        marginLeft: 7
    },
});

