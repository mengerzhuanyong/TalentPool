/*
 * @author: jiasong
 * @creation time: 2018-11-02 19:22:00
 */

'use strict'
import React from 'react'
import { View, Text, StyleSheet, ImageBackground, Modal } from 'react-native'
import PropTypes from 'prop-types'
import { CallStatus } from './RTCConstants';
import ImageView from '../../Image/ImageView'
import Button from '../../Touchable/Button';
import DialTimer from './DialTimer'

export default class VoiceDial extends React.PureComponent {

    static propTypes = {
        rtcManager: PropTypes.any.isRequired,
        callId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, // 会话标志符号
        senderInfo: PropTypes.object.isRequired, // 发送者信息
        receiveInfo: PropTypes.object.isRequired, // 接收者的信息
        onHook: PropTypes.func,
        onCutIn: PropTypes.func,
        // status: PropTypes.oneOf([CallStatus.None, CallStatus.Calleding, CallStatus.Callering, CallStatus.Calling])
    };

    static defaultProps = {
        senderInfo: {},
        receiveInfo: {},
        // status: CallStatus.None
    };

    constructor(props) {
        super(props)
        this.state = { status: CallStatus.None }
        this.subscriptions = []
    }

    componentDidMount() {
        const { rtcManager } = this.props
        if (rtcManager) {
            this.subscriptions = [
                rtcManager.addMonitorStatus(this.updateStatus)
            ]
        }
    }

    componentWillUnmount() {
        const { rtcManager } = this.props
        if (rtcManager) {
            this.subscriptions.forEach(subscription => {
                subscription && subscription.remove && subscription.remove();
            });
        }
    }

    updateStatus = (_status) => {
        const { status } = this.state
        Log.print('updateStatus', _status)
        if (status != _status) {
            this.setState({ status: _status })
        }
    }

    _onPressHook = async () => {
        const { rtcManager, callId, onHook } = this.props
        if (rtcManager) {
            const result = await rtcManager.endCall(callId)
            if (result.code === 'success') {
                onHook && onHook(result)
            }
        }
        // const { hyphenateStore } = this.props
        // hyphenateStore.endCall()
    }

    _onPressCutIn = async () => {
        const { rtcManager, callId, onCutIn } = this.props
        if (rtcManager) {
            const result = await rtcManager.answerIncomingCall(callId)
            if (result.code === 'success') {
                onCutIn && onCutIn(result)
            }
        }
    }

    renderImageBackground = () => {
        return (
            <ImageBackground
                style={styles.viewBack}
                source={Images.img_dial_back}
            />
        )
    }

    renderhint = () => {
        const { status } = this.state
        let time = null
        if (status === CallStatus.Calling) {
            time = <DialTimer />
        }
        return (
            <View style={styles.hint}>
                {time}
            </View>
        )
    }

    renderCutIn = () => {
        const { status } = this.state
        Log.print('标记', status)
        if (status === CallStatus.Calleding) {
            return (
                <Button
                    style={styles.hookButton}
                    icon={Images.icon_cutIn}
                    iconStyle={styles.hook}
                    onPress={this._onPressCutIn}
                />
            )
        }
        return null
    }

    renderCondition = () => {
        const { status } = this.state
        if (status === CallStatus.Calleding) {
            return <Text style={styles.headerText}>语音请求...</Text>
        }
        if (status === CallStatus.Callering) {
            return <Text style={styles.headerText}>正在呼叫...</Text>
        }
        if (status === CallStatus.Calling) {
            return (
                <View style={styles.connect}>
                    <ImageView style={styles.connectImg} source={Images.icon_connect} />
                    <Text style={styles.connectText}>已接通</Text>
                </View>
            )
        }
        return null
    }


    render() {
        const { receiveInfo } = this.props
        return (
            <View style={styles.container}>
                {this.renderImageBackground()}
                {this.renderhint()}
                <View style={styles.header}>
                    <ImageView
                        style={styles.headerImg}
                        source={receiveInfo.avatar ? { uri: receiveInfo.avatar } : Images.icon_default_header}
                    />
                    <Text style={styles.headerName} numberOfLines={1}>{receiveInfo.nickname}</Text>
                    {this.renderCondition()}
                </View>
                <View style={styles.bottomButton}>
                    <Button
                        style={styles.hookButton}
                        icon={Images.icon_onHook}
                        iconStyle={styles.hook}
                        onPress={this._onPressHook}
                    />
                    {this.renderCutIn()}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    status: {
        height: __IOS__ ? Theme.statusBarHeight : Theme.statusBarHeight,
        backgroundColor: '#000'
    },
    hint: {
        width: SCREEN_WIDTH - 50,
    },
    header: {
        flexDirection: 'column',
        alignItems: 'center'
    },
    headerImg: {
        width: 100,
        height: 100,
        borderRadius: 6
    },
    headerName: {
        marginTop: 10,
        fontSize: FontSize(22),
        fontWeight: 'bold',
        color: '#ffffff'
    },
    headerHeadline: {
        marginTop: 5,
        fontSize: FontSize(14),
        color: '#ffffff'
    },
    headerText: {
        marginTop: 25,
        fontSize: FontSize(14),
        color: '#ffffff'
    },
    connect: {
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    connectImg: {
        marginRight: 5,
        width: 10,
        height: 16
    },
    connectText: {
        fontSize: FontSize(14),
        color: '#2dcbe7'
    },
    viewBack: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    bottomButton: {
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    notice: {
        marginTop: 15,
        marginHorizontal: 15,
        paddingHorizontal: 15,
        paddingVertical: 8,
        flexDirection: 'row',
        alignItems: 'center',
        opacity: 0.5,
        backgroundColor: '#000000'
    },
    noticeImg: {
        width: 15,
        height: 15
    },
    noticeText: {
        flex: 1,
        paddingLeft: 10,
        fontSize: FontSize(12),
        color: '#ffffff'
    },
    hookButton: {
        backgroundColor: 'transparent',
    },
    hook: {
        width: 90,
        height: 55,
    }
})


