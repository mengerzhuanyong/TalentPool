/*
 * @author: jiasong
 * @creation time: 2018-11-02 19:22:00
 */

'use strict'
import React from 'react'
import { View, Text, StyleSheet, ImageBackground, Modal } from 'react-native'
import PropTypes from 'prop-types'
import { CallStatus } from './RTCConstants';
import HyphenateVideo from '../Hyphenate/HyphenateVideo'
import ImageView from '../../Image/ImageView'
import Button from '../../Touchable/Button';
import DialTimer from './DialTimer'

export default class VoiceDial extends React.PureComponent {

    static propTypes = {
        rtcManager: PropTypes.any.isRequired,
        callId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, // 会话标志符号
        senderInfo: PropTypes.object.isRequired, // 发送者信息
        receiveInfo: PropTypes.object.isRequired, // 接收者的信息
        onHook: PropTypes.func,
        onCutIn: PropTypes.func,
        // status: PropTypes.oneOf([CallStatus.None, CallStatus.Calleding, CallStatus.Callering, CallStatus.Calling])
    };

    static defaultProps = {
        senderInfo: {},
        receiveInfo: {},
        // status: CallStatus.None
    };

    constructor(props) {
        super(props)
        this.state = { status: CallStatus.None }
        this.subscriptions = []
    }

    componentDidMount() {
        const { rtcManager } = this.props
        if (rtcManager) {
            this.subscriptions = [
                rtcManager.addMonitorStatus(this.updateStatus)
            ]
        }
    }

    componentWillUnmount() {
        const { rtcManager } = this.props
        if (rtcManager) {
            this.subscriptions.forEach(subscription => {
                subscription && subscription.remove && subscription.remove();
            });
        }
    }

    updateStatus = (_status) => {
        const { status } = this.state
        Log.print('updateStatus', _status)
        if (status != _status) {
            this.setState({ status: _status })
        }
    }

    _onPressHook = async () => {
        const { rtcManager, callId, onHook } = this.props
        if (rtcManager) {
            const result = await rtcManager.endCall(callId)
            if (result.code === 'success') {
                onHook && onHook(result)
            }
        }
        // const { hyphenateStore } = this.props
        // hyphenateStore.endCall()
    }

    _onPressCutIn = async () => {
        const { rtcManager, callId, onCutIn } = this.props
        if (rtcManager) {
            const result = await rtcManager.answerIncomingCall(callId)
            if (result.code === 'success') {
                onCutIn && onCutIn(result)
            }
        }
    }

    renderImageBackground = () => {
        return (
            <ImageBackground
                style={styles.viewBack}
                source={Images.img_dial_back}
            />
        )
    }

    renderhint = () => {
        const { status } = this.state
        let time = null
        if (status === CallStatus.Calling) {
            time = <DialTimer />
        }
        return (
            <View style={styles.hint}>
                {time}
            </View>
        )
    }

    renderCutIn = () => {
        const { status } = this.state
        Log.print('标记', status)
        if (status === CallStatus.Calleding) {
            return (
                <Button
                    style={styles.hookButton}
                    icon={Images.icon_cutIn}
                    iconStyle={styles.hook}
                    onPress={this._onPressCutIn}
                />
            )
        }
        return null
    }

    renderHeaderImageBackground = () => {
        const { status } = this.state
        if (status === CallStatus.Calling) {
            return (
                <ImageBackground
                    style={styles.viewBack}
                    source={Images.img_dial_head_back}
                />
            )
        }
        return null
    }

    renderCondition = () => {
        const { status } = this.state
        if (status === CallStatus.Calleding) {
            return <Text style={styles.headerText}>视频请求...</Text>
        }
        if (status === CallStatus.Callering) {
            return <Text style={styles.headerText}>正在呼叫...</Text>
        }
        return null
    }

    renderVideo = () => {
        const { status } = this.state
        if (status === CallStatus.Calling) {
            return <HyphenateVideo style={styles.video} showLocalView={false} />
        }
        return null
    }


    render() {
        const { receiveInfo } = this.props
        return (
            <View style={styles.container}>
                {this.renderImageBackground()}
                {this.renderVideo()}
                <View style={styles.header}>
                    {this.renderHeaderImageBackground()}
                    <ImageView
                        style={styles.headerImg}
                        source={receiveInfo.avatar ? { uri: receiveInfo.avatar } : Images.icon_default_header}
                    />
                    <View style={styles.headerRight}>
                        <Text style={styles.headerName} numberOfLines={1}>{receiveInfo.nickname}</Text>
                        {this.renderhint()}
                    </View>
                </View>
                {this.renderCondition()}
                <View style={styles.bottomButton}>
                    <Button
                        style={styles.hookButton}
                        icon={Images.icon_onHook}
                        iconStyle={styles.hook}
                        onPress={this._onPressHook}
                    />
                    {this.renderCutIn()}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
    },
    viewBack: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    header: {
        paddingTop: Theme.statusBarHeight + 10,
        paddingHorizontal: 15,
        paddingVertical: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerImg: {
        width: 100,
        height: 100,
        borderRadius: 6
    },
    headerRight: {
        flex: 1,
        height: 100,
        marginLeft: 10,
        paddingBottom: 5,
        justifyContent: 'space-between',
    },
    headerText: {
        fontSize: FontSize(14),
        color: '#ffffff',
        alignSelf: 'center',
    },
    headerName: {
        fontSize: FontSize(22),
        color: '#fff',
        fontWeight: 'bold'
    },
    headerHeadline: {
        marginTop: -10,
        fontSize: FontSize(13),
        color: '#fff'
    },
    bottomButton: {
        paddingBottom: 70,
        paddingHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    notice: {
        marginTop: 15,
        marginHorizontal: 15,
        paddingHorizontal: 15,
        paddingVertical: 8,
        flexDirection: 'row',
        alignItems: 'center',
        opacity: 0.5,
        backgroundColor: '#000000'
    },
    noticeImg: {
        width: 15,
        height: 15
    },
    noticeText: {
        paddingLeft: 10,
        fontSize: FontSize(12),
        color: '#ffffff'
    },
    navBar: {
        position: 'absolute',
        backgroundColor: 'transparent',
    },
    hookButton: {
        backgroundColor: 'transparent',
    },
    hook: {
        width: 90,
        height: 55,
    },
    video: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        // backgroundColor: 'red',
    }
})


