'use strict';
import React from 'react';
import { View, Text, StyleSheet, ImageBackground, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types'
import MessageList from './MessageList/MessageList';
import HyphenateManager from './Hyphenate/HyphenateManager'
import ToolBarContainer from './ToolBar/ToolBarContainer';
import { ChatType, MessageType } from './Message/MessageConstants';
import { conversionMessage } from './Util';

export default class ChatScreen extends React.PureComponent {

    static propTypes = {
        chatType: PropTypes.oneOf([ChatType.Single, ChatType.Group, ChatType.Room]),// 聊天类型
        receiveId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
        receiveInfo: PropTypes.object,
        senderInfo: PropTypes.object.isRequired, // 发送者信息
        /**
         * @列表 
         */
        listContentContainerStyle: ViewPropTypes.style,
        /**
         * @背景图 
         */
        bckgroundImage: PropTypes.oneOfType([PropTypes.number, PropTypes.shape({ uri: PropTypes.string })]),
        /**
         * @聊天回调 
         */
        onUpdateMessage: PropTypes.func,
        onUserDidJoinChatroom: PropTypes.func,
        onUserDidLeaveChatroom: PropTypes.func,
        /**
         * @点击头像 
         */
        onPressAvatar: PropTypes.func,
        onLongPressAvatar: PropTypes.func,
    };

    static defaultProps = {
        chatType: ChatType.Room,
    };

    constructor(props) {
        super(props)
        this.state = { dataSource: [] }
        this._dataSource = []
        this._subscriptions = []
    }

    componentDidMount() {
        const { chatType } = this.props
        if (chatType === ChatType.Room) {
            this._handleEnterChatroom()
        }
        // 添加监听
        this._subscriptions = [
            HyphenateManager.addMessagesDidReceive(this._messagesDidReceive),
            HyphenateManager.addUserDidJoinChatroom(this._userDidJoinChatroom),
            HyphenateManager.addUserDidLeaveChatroom(this._userDidLeaveChatroom),
            HyphenateManager.addDidDismissFromChatroom(this._didDismissFromChatroom),
        ]
    }

    componentWillUnmount() {
        const { chatType } = this.props
        if (chatType === ChatType.Room) {
            this._handleLeaveChatroom()
        }
        // 清除监听
        this._subscriptions.forEach(subscription => {
            subscription && subscription.remove && subscription.remove();
        });
    }

    _handleEnterChatroom = async () => {
        const { receiveId, onUserDidJoinChatroom } = this.props
        // 加入聊天室
        const result = await HyphenateManager.joinChatroom(receiveId)
        if (result.code === 'success') {
            const detailRes = await HyphenateManager.getChatroomDetail(receiveId)
            Log.print('_handleEnterChatroom', detailRes)
            if (detailRes.code === 'success') {
                onUserDidJoinChatroom && onUserDidJoinChatroom({
                    code: detailRes.code,
                    data: { ...detailRes.data, userName: '' }
                })
            } else {
                onUserDidJoinChatroom && onUserDidJoinChatroom(detailRes)
            }
        } else {
            onUserDidJoinChatroom && onUserDidJoinChatroom(result)
        }
    }

    _handleLeaveChatroom = async () => {
        const { receiveId } = this.props
        // 离开聊天室
        HyphenateManager.leaveChatroom(receiveId)
    }

    _onTouchStart = () => {
        this._toolBarRef.barDown()
    }

    _messagesDidReceive = (data) => {
        let messageData = conversionMessage(data)
        const { receiveId, onUpdateMessage } = this.props
        Log.print('_messagesDidReceive', messageData)
        messageData = messageData.filter((msgItem) => {
            return msgItem.to == receiveId;
        })
        if (messageData.length > 0) {
            this._dataSource = messageData.concat(this._dataSource)
            this.setState({ dataSource: this._dataSource }, () => {
                onUpdateMessage && onUpdateMessage({
                    code: 'success', data: messageData
                })
            })
        }
    }

    _userDidJoinChatroom = async (data) => {
        const { receiveId, onUserDidJoinChatroom } = this.props
        const result = await HyphenateManager.getChatroomDetail(receiveId)
        if (result.code === 'success') {
            onUserDidJoinChatroom && onUserDidJoinChatroom({
                code: result.code,
                data: { ...data, ...result.data }
            })
        }
    }

    _userDidLeaveChatroom = async (data) => {
        const { receiveId, onUserDidLeaveChatroom } = this.props
        const result = await HyphenateManager.getChatroomDetail(receiveId)
        if (result.code === 'success') {
            onUserDidLeaveChatroom && onUserDidLeaveChatroom({
                code: result.code,
                data: { ...data, ...result.data }
            })
        }
    }

    _didDismissFromChatroom = async (data) => {
        const { receiveId, didDismissFromChatroom } = this.props
        didDismissFromChatroom && didDismissFromChatroom(data)
    }

    sendMessage = async (type, data) => {
        const { senderInfo, receiveId, chatType, onUpdateMessage } = this.props
        Log.print('senderInfo', senderInfo)
        let result;
        if (type === MessageType.Text) {
            result = await HyphenateManager.sendTextMessage({
                conversationId: receiveId,
                chatType: chatType,
                content: data.content,
                to: receiveId,
                ext: { ...senderInfo, ...data.ext },
            })
        } else if (type === MessageType.Voice) {
            result = await HyphenateManager.sendVoiceMessage({
                conversationId: receiveId,
                chatType: chatType,
                path: data.path,
                displayName: data.displayName,
                duration: data.duration,
                to: receiveId,
                ext: { ...senderInfo, ...data.ext },
            })
        }
        if (result.code === 'success') {
            const messageData = conversionMessage(result.data)
            Log.print('sendMessage', messageData)
            let newData = this._dataSource.slice()
            newData.splice(0, 0, messageData)
            this._dataSource = newData;
            this.setState({ dataSource: this._dataSource }, () => {
                onUpdateMessage && onUpdateMessage({
                    code: 'success', data: messageData
                })
            })
        } else {
            onUpdateMessage && onUpdateMessage(result)
        }
    }

    _onSend = ({ type, data }) => {
        this.sendMessage(type, data)
    }

    /**
     * @与聊天组件无关 [不需要时删除]
     */
    _onGiftSend = ({ type, data }) => {
        Log.print('标记', { type, data })
        const { receiveId, onGiftSend } = this.props
        onGiftSend && onGiftSend({ type, data, receiveId })
    }

    _onEndReached = (stopEndReached) => {

    }

    _captureRef = (node) => {
        if (node) {
            this._messageListRef = node.getListViewRef()
        }
    }

    _captureBarRef = (node) => {
        if (node) {
            this._toolBarRef = node
        }
    }

    render() {
        const { dataSource } = this.state
        const {
            bckgroundImage,
            onPressAvatar,
            onLongPressAvatar,
            senderInfo,
            chatType,
            listContentContainerStyle
        } = this.props
        return (
            <View style={styles.container}>
                {bckgroundImage ? <ImageBackground style={styles.backImage} source={bckgroundImage} /> : null}
                <MessageList
                    ref={this._captureRef}
                    contentContainerStyle={listContentContainerStyle}
                    enableLoadMore={chatType === ChatType.Room ? false : true}
                    onEndReached={this._onEndReached}
                    dataSource={dataSource}
                    onPressAvatar={onPressAvatar}
                    onLongPressAvatar={onLongPressAvatar}
                    onTouchStart={this._onTouchStart}
                />
                <ToolBarContainer
                    ref={this._captureBarRef}
                    senderInfo={senderInfo}
                    onKeyboardChange={this._onKeyboardChange}
                    onSend={this._onSend}
                    /**
                     * @与聊天组件无关 [不需要时删除]
                     */
                    onGiftSend={this._onGiftSend}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backImage: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    }
});