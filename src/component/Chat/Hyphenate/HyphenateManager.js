// import { NativeModules, NativeEventEmitter } from 'react-native';
// import { ChatType } from '../Message/MessageConstants';
// import { CallType } from '../RTC/RTCConstants';

// const HyphenateModule = NativeModules.HyphenateModule
// const HyphenateEmitter = new NativeEventEmitter(HyphenateModule)

// class HyphenateManager {

//     static conversionParams(params) {
//         let { conversationId, chatType, ext } = params
//         if (chatType === ChatType.Single) {
//             chatType = 0
//         } else if (chatType === ChatType.Group) {
//             chatType = 1
//         } else {
//             chatType = 2
//         }
//         if (ext) {
//             ext = JSON.stringify(ext)
//         } else {
//             ext = "{}"
//         }
//         return { ...params, chatType, conversationId, ext }
//     }

//     /*!
//      *  初始化sdk
//      *
//      *  @param aOptions  SDK配置项
//      *
//      *  @result 错误信息
//      *
//      */
//     static async init(appKey, apnsCertName) {
//         const result = await HyphenateModule.init({ appKey, apnsCertName })
//         // alert(JSON.stringify(result))
//         return result
//     }

//     /*!
//      *  密码登录
//      *
//      *  @param aUsername  用户名
//      *  @param aPassword  密码
//      *
//      *  @result 错误信息
//      *
//      */
//     static async login(userName, password) {
//         const result = await HyphenateModule.login({ userName, password })
//         // alert(JSON.stringify(result))
//         return result
//     }
//     /*!
//     *  加入聊天室
//     *
//     *  @param aChatroomId           聊天室的ID
//     */
//     static async joinChatroom(aChatroomId) {
//         const result = await HyphenateModule.joinChatroom(aChatroomId)
//         return result
//     }
//     /*!
//     *  退出聊天室
//     *
//     *  @param aChatroomId          聊天室ID
//     */
//     static async leaveChatroom(aChatroomId) {
//         const result = await HyphenateModule.leaveChatroom(aChatroomId)
//         return result
//     }
//     /*!
//     *  获取聊天室详情
//     *
//     *  @param aChatroomId           聊天室ID
//     *
//     */
//     static async getChatroomDetail(aChatroomId) {
//         const result = await HyphenateModule.getChatroomSpecificationWithId(aChatroomId)
//         return result
//     }
//     /*!
//         *  发送文字消息
//         *
//         *  @param aChatroomId           聊天室ID
//         *
//         */
//     static async sendTextMessage(params) {
//         const { conversationId, chatType, content, to, ext } = this.conversionParams(params)
//         if (!content) {
//             return;
//         }
//         const result = await HyphenateModule.sendTextMessage(conversationId, chatType, content, to, ext)
//         return result
//     }
//     /*!
//            *  发送语音消息
//            *
//            *  @param aChatroomId           聊天室ID
//            *
//            */
//     static async sendVoiceMessage(params) {
//         const {
//             conversationId,
//             chatType,
//             to,
//             ext,
//             path,
//             displayName,
//             duration
//         } = this.conversionParams(params)
//         const result = await HyphenateModule.sendVoiceMessage(conversationId, chatType, path, displayName, duration, to, ext)
//         return result
//     }


//     /*!
//      *  退出
//      */
//     static async logout(isUnbindDeviceToken = false) {
//         const result = await HyphenateModule.logout(isUnbindDeviceToken)
//         // alert(JSON.stringify(result))
//         return result
//     }
//     /*!
//      *  \~chinese
//      *  设置设置项
//      *
//      *  @param aOptions  设置项
//      */
//     static async setCallOptions(options = {}) {
//         const result = await HyphenateModule.setCallOptions(options)
//         return result
//     }

//     /*!
//      *  发起实时会话
//      *
//      *  @param aType            通话类型
//      *  @param aRemoteName      被呼叫的用户（不能与自己通话）
//      *  @param aExt             通话扩展信息，会传给被呼叫方
//      *  @param aCompletionBlock 完成的回调
//      */
//     static async startCall(type, tartgetName, ext = {}) {
//         const result = await HyphenateModule.startCall(type.toLowerCase(), tartgetName, JSON.stringify(ext))
//         return result
//     }
//     /*!
//      *  接收方同意通话请求
//      *
//      *  @param  aCallId     通话ID
//      *
//      *  @result 错误信息
//      */
//     static async answerIncomingCall(callId) {
//         const result = await HyphenateModule.answerIncomingCall(callId)
//         return result
//     }
//     /*!
//      *  结束通话
//      *
//      *  @param aCallId     通话的ID
//      *  @param aReason     结束原因
//      *
//      *  @result 错误
//      */
//     static async endCall(callId, reason = 0) {
//         const result = await HyphenateModule.endCall(callId, reason)
//         return result
//     }
//     /*!
//     *  强制结束所有通话
//     *  使用场景：做了某些错误操作造成Call UI已经消失但是没有释放掉EMCallManager中维护的EMCallSession,造成再次调用方法[IEMCallManager startCall:remoteName:ext:completion:]返回错误EMErrorCallBusy，如果这时无法调用方法[IEMCallManager endCall:reason:]，可以调用该方法
//     */
//     static forceEndAllCall() {
//         HyphenateModule.forceEndAllCall()
//     }
//     /*!
//      *  设置使用前置摄像头还是后置摄像头,默认使用前置摄像头
//      *
//      *  @param  aIsFrontCamera    是否使用前置摄像头, YES使用前置, NO使用后置
//      */
//     static switchCameraPosition(isFrontCamera) {
//         HyphenateModule.switchCameraPosition(isFrontCamera)
//     }
//     /*!
//      *  设置是否为扬声器
//      *
//      *  @param  isSpeaker    是否为扬声器
//      */
//     static setSpeaker(isSpeaker) {
//         HyphenateModule.setSpeaker(isSpeaker)
//     }
//     /*!
//      *  屏幕常亮
//      *
//      *  @param  disabled    是否常亮
//      */
//     static setIdleTimerDisabled(disabled) {
//         HyphenateModule.setIdleTimerDisabled(disabled)
//     }
//     /*!
//      *  用户A拨打用户B，用户B会收到这个回调
//      *
//      */
//     static addCallDidReceiveListener(callBack) {
//         return HyphenateEmitter.addListener('callDidReceive', callBack)
//     }
//     /*!
//      *  通话通道建立完成，用户A和用户B都会收到这个回调
//      *
//      */
//     static addCallDidConnectListener(callBack) {
//         return HyphenateEmitter.addListener('callDidConnect', callBack)
//     }
//     /*!
//      *  用户B同意用户A拨打的通话后，用户A会收到这个回调
//      *
//      */
//     static addCallDidAcceptListener(callBack) {
//         return HyphenateEmitter.addListener('callDidAccept', callBack)
//     }
//     /*!
//      *  1. 用户A或用户B结束通话后，对方会收到该回调
//      *  2. 通话出现错误，双方都会收到该回调
//      *
//      */
//     static addCallDidEndListener(callBack) {
//         return HyphenateEmitter.addListener('callDidEnd', callBack)
//     }
//     /*!
//      *  用户A和用户B正在通话中，用户A中断或者继续数据流传输时，用户B会收到该回调
//      *
//      */
//     static addCallStateDidChangeListener(callBack) {
//         return HyphenateEmitter.addListener('callStateDidChange', callBack)
//     }
//     /*!
//      *  \~chinese
//      *  当前登录账号在其它设备登录时会接收到此回调
//      */

//     static addUserDidLoginFromOtherDevice(callBack) {
//         return HyphenateEmitter.addListener('userAccountDidLoginFromOtherDevice', callBack)
//     }

//     /*!
//       * // 收到消息的回调，带有附件类型的消息可以用 SDK 提供的下载附件方法下载（后面会讲到）
//       *  
//       */
//     static addMessagesDidReceive(callBack) {
//         return HyphenateEmitter.addListener('messagesDidReceive', callBack)
//     }
//     /*!
//      *  有用户加入聊天室
//      *
//      *  @param aChatroom    加入的聊天室
//      *  @param aUsername    加入者
//      */
//     static addUserDidJoinChatroom(callBack) {
//         return HyphenateEmitter.addListener('userDidJoinChatroom', callBack)
//     }
//     /*!
//      *  有用户离开聊天室
//      *
//      *  @param aChatroom    离开的聊天室
//      *  @param aUsername    离开者
//      */
//     static addUserDidLeaveChatroom(callBack) {
//         return HyphenateEmitter.addListener('userDidLeaveChatroom', callBack)
//     }
//     /*!
//      *  被踢出聊天室
//      *
//      *  @param aChatroom    被踢出的聊天室
//      *  @param aReason      被踢出聊天室的原因
//      */
//     static addDidDismissFromChatroom(callBack) {
//         return HyphenateEmitter.addListener('didDismissFromChatroom', callBack)
//     }


// }


// export default HyphenateManager