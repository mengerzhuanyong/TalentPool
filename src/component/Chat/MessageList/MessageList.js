'use strict';
import React from 'react';
import { View, Text, StyleSheet, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types'
import ListView from '../../List/ListView';
import MessageItem from './../Message/MessageItem'

export default class MessageList extends React.PureComponent {

    static propTypes = {
        dataSource: PropTypes.arrayOf(PropTypes.shape({
            messageId: PropTypes.string,
            conversationId: PropTypes.string,
            from: PropTypes.string,
            to: PropTypes.string,
            timestamp: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
            localTime: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
            direction: MessageItem.propTypes.direction,
            chatType: MessageItem.propTypes.chatType,
            status: MessageItem.propTypes.status,
            isReadAcked: PropTypes.bool,
            isDeliverAcked: PropTypes.bool,
            isRead: PropTypes.bool,
            body: MessageItem.propTypes.body,
            ext: PropTypes.object,
        })).isRequired,
        millisecTime: PropTypes.number,
        /**
         * @列表 []
         */
        contentContainerStyle: ViewPropTypes.style,
        /**
         * @点击事件 
         */
        onPressAvatar: PropTypes.func,
        onLongPressAvatar: PropTypes.func,
    };

    static defaultProps = {
        millisecTime: 5000
    };

    constructor(props) {
        super(props)
        this._lastMessageTime = parseFloat(Moment().format('x'))

    };

    componentDidMount() {

    };

    componentDidUpdate(prevProps, prevState) {
        const { dataSource: prevDataSource } = prevProps
        const { dataSource } = this.props
        if (dataSource.length > 5 && prevDataSource.length < dataSource.length) {
            LayoutAnimationManager.updateWithScaleXY({
                duration: 100,
            });
        }
    }

    _minuteOffset = (localTime) => {
        const { millisecTime, dataSource } = this.props
        const difference = localTime - this._lastMessageTime
        // Log.print('_minuteOffset', difference, Moment(localTime).format('YYYY-MM-DD HH:mm:ss'), Moment(this._lastMessageTime).format('YYYY-MM-DD HH:mm:ss'))
        Log.print('_minuteOffset', difference)
        if (difference >= millisecTime) {
            this._lastMessageTime = localTime
            return true
        } else {
            return false
        }
    }

    _onEndReached = (stopEndReached) => {
        const { onEndReached } = this.props
        onEndReached && onEndReached(stopEndReached)
    };

    renderItem = (info) => {
        const {
            onPressAvatar,
            onLongPressAvatar,
        } = this.props
        const { item, index } = info
        return (
            <MessageItem
                {...item}
                onPressAvatar={onPressAvatar}
                onLongPressAvatar={onLongPressAvatar}
            />
        )
    };

    _keyExtractor = (item, index) => {
        return `todo_chat_message${item.messageId}`
    };

    getListViewRef = () => {
        return this._listViewRef
    }

    _captureRef = (v) => {
        this._listViewRef = v
    };


    render() {
        const {
            dataSource,
            onTouchStart,
            contentContainerStyle,
            ...others
        } = this.props
        return (
            <ListView
                ref={this._captureRef}
                contentContainerStyle={[styles.contentContainerStyle, contentContainerStyle]}
                initialRefresh={false}
                enableRefresh={false}
                inverted={true}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                ListEmptyComponent={null}
                data={dataSource}
                keyExtractor={this._keyExtractor}
                renderItem={this.renderItem}
                onEndReached={this._onEndReached}
                onTouchStart={onTouchStart}
                {...others}
            />
        );
    }
}

const styles = StyleSheet.create({
    contentContainerStyle: {
        flexGrow: 1,
        justifyContent: 'flex-end',
    }
});