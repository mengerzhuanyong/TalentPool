'use strict';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types'
import ListView from './ListView';
import SpinnerLoading from '../Spinner/SpinnerLoading';

/**
 * @通用列表 [目的是为了减少开发代码]
 */
export default class CommonListView extends React.PureComponent {

    static propTypes = {
        data: PropTypes.array.isRequired,
        limit: PropTypes.number,
        requestParams: PropTypes.oneOfType([
            PropTypes.shape({
                url: PropTypes.string.isRequired,
                params: PropTypes.object.isRequired,
                method: PropTypes.oneOf(['get', 'post']),
            }),
            PropTypes.arrayOf({
                url: PropTypes.string.isRequired,
                params: PropTypes.object.isRequired,
                method: PropTypes.oneOf(['get', 'post']),
            })
        ]).isRequired,
        onRequest: PropTypes.func,
        onRequestStart: PropTypes.func,
        onRequestEnd: PropTypes.func,
        enableLoading: PropTypes.bool,
        ListHeaderComponent: PropTypes.func,
        ListFooterComponent: PropTypes.func,
        ListEmptyComponent: PropTypes.func,
        ItemSeparatorComponent: PropTypes.func,
        extraData: PropTypes.any,
    }

    static defaultProps = {
        limit: 10,
        enableLoading: true,
    }

    constructor(props) {
        super(props)
        const { onRequest, enableLoading } = props
        this.state = { loading: onRequest || !enableLoading ? false : true }
        this.page = 1
    };

    componentDidMount() {
        this.requestDataSource()
    };

    requestDataSource = async () => {
        const {
            limit,
            requestParams,
            onRequest,
            onRequestStart,
            enableLoading
        } = this.props
        if (onRequest) {
            onRequest({ ...this.props, page: this.page })
            return;
        }
        onRequestStart && onRequestStart({ ...this.props })
        if (typeof requestParams === 'object') {
            const servicesMethod = requestParams.method === 'get' ? Services.get : Services.post
            const url = requestParams.url
            const parmas = {
                page: this.page,
                limit: limit,
                ...requestParams.params
            }
            const result = await servicesMethod(url, parmas)
            this.handleResult(result)
        } else if (Array.isArray(requestParams)) {
            console.warn('TODO--多请求的方式情况比较复杂，待实现')
        } else {
            console.error('requestParams格式不正确')
        }
        if (enableLoading) {
            this.setState({ loading: false })
        }
    };

    handleResult = (result) => {
        const {
            data: dataSource,
            onRequestEnd,
            limit
        } = this.props
        let listData = [], handleData = { limit: limit, page: this.page }
        if (result.code === StatusCode.SUCCESS_CODE) {
            listData = result.data.data
            if (this.page === 1) {
                handleData = { ...handleData, list: listData }
            } else if (this.page > 1) {
                if (listData.length === 0) {
                    this.page--;
                    handleData = { ...handleData, list: listData }
                } else {
                    let newDataSource = dataSource.slice()
                    handleData = { ...handleData, list: newDataSource.concat(listData) }
                }
            }
        } else {
            this.page > 1 && this.page--;
            handleData = { ...handleData, list: listData }
        }
        onRequestEnd({ ...result, handleData: handleData })
        // 停止刷新
        this._listViewRef && this._listViewRef.stopRefresh();
        this._listViewRef && this._listViewRef.stopEndReached({ allLoad: listData.length < limit });
    }

    _onRefresh = (stopRefresh) => {
        this.page = 1
        this.requestDataSource()
    };

    _onEndReached = (stopEndReached) => {
        this.page++
        this.requestDataSource()
    };

    renderLoading = () => {
        const { enableLoading } = this.props
        const { loading } = this.state
        if (enableLoading) {
            return <SpinnerLoading style={styles.loading} loading={loading} />
        }
        return null;
    };

    _keyExtractor = (item, index) => {
        return `item_${item.id}`
    };

    _captureRef = (v) => {
        this._listViewRef = v
    };


    render() {
        return (
            <View style={styles.container}>
                <ListView
                    ref={this._captureRef}
                    enableLoadMore={true}
                    enableRefresh={true}
                    keyExtractor={this._keyExtractor}
                    onRefresh={this._onRefresh}
                    onEndReached={this._onEndReached}
                    {...this.props}
                />
                {this.renderLoading()}
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    loading: {
        top: 0
    }
});