'use strict';
import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types'

export default class EmptyView extends React.PureComponent {

    static propTypes = {

    };
    static defaultProps = {

    };

    render() {

        return (
            <View style={styles.emptyContainer}>
                <Image style={styles.emptyImage} source={Images.icon_no_record} />
                <Text style={styles.emptyText}>暂无数据</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    emptyContainer: {
        height: ScaleSize(800),
        justifyContent: 'center',
        alignItems: 'center',
    },
    emptyText: {
        marginTop: ScaleSize(20),
        color: '#cdcdcd',
        fontSize: 13,
    },
    emptyImage: {
        width: ScaleSize(180),
        height: ScaleSize(180)
    }
});
