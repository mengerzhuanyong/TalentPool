'use strict';
import React from 'react';
import { RefreshControl } from 'react-native';
import PropTypes from 'prop-types'

export default class HeaderLoading extends React.PureComponent {

    static propTypes = {
        isRefreshing: PropTypes.bool,
        onRefresh: PropTypes.func,
        colors: PropTypes.array,
        progressBackgroundColor: PropTypes.string,
        size: PropTypes.any,
        tintColor: PropTypes.string,
        title: PropTypes.string,
        progressViewOffset: PropTypes.number,
    }

    static defaultProps = {
        isRefreshing: false,
    }

    render() {
        const { isRefreshing, onRefresh, ...others } = this.props;
        return (
            <RefreshControl
                refreshing={isRefreshing}
                onRefresh={onRefresh}
                {...others}
            />
        );
    }
}

