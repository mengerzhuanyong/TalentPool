'use strict'
import React from 'react';
import { View, Text, StyleSheet, TouchableHighlight, Platform } from 'react-native';
import PropTypes from 'prop-types'
import { checkEmpty } from '../../util/Tool';
import { scaleSize } from '../../util/Adaptation';
import Theme from '../../config/themes/Theme'


export default class AlertContent extends React.PureComponent {

    static propTypes = {
        title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
        titleStyle: Text.propTypes.style,
        detail: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
        detailStyle: Text.propTypes.style,
        actions: PropTypes.arrayOf(PropTypes.shape({ title: PropTypes.string, titleStyle: Text.propTypes.style, onPress: PropTypes.func }))
        // 例如,
        //   actions: [
        //         { title: '取消', titleStyle: {}, onPress: () => alert('取消') },
        //         { title: '确定', titleStyle: {}, onPress: () => alert('取消') },
        //     ]
    };

    static defaultProps = {
        actions: []
    };

    constructor(props) {
        super(props)
        this.lastActionTime = 0
        this.interval = 500
    };

    componentWillUnmount() {
        // 卸载
        // clearTimeout(this.times)
    }

    _onPress = (onPressItem) => {
        let nowTime = Moment().format('x')
        if ((nowTime - this.lastActionTime) <= this.interval) {
            // console.warn('间隔时间内重复点击了');
            return;
        }
        requestAnimationFrame(() => {
            this.lastActionTime = nowTime;
            onPressItem && onPressItem()
        })
        AlertManager.hide()
    };

    separator = (index) => {
        const { actions } = this.props
        let separator;
        if (actions.length === 1) {
            separator = null
        } else {
            separator = actions.length - 1 === index ? null : styles.separator
        }
        return separator
    }

    _renderTitle = () => {
        const { title, titleStyle } = this.props
        if (React.isValidElement(title)) {
            return title
        } else if (!checkEmpty(title)) {
            return <Text style={[styles.title, titleStyle]}>{title}</Text>
        }
        return null
    };

    _renderDetail = () => {
        const { detail, detailStyle } = this.props
        if (React.isValidElement(detail)) {
            return detail
        } else if (!checkEmpty(detail)) {
            return <Text style={[styles.detail, detailStyle]}>{detail}</Text>
        }
        return null
    };

    _rederAction = () => {
        const { actions } = this.props
        if (actions.length === 0) {
            return null
        }
        return (
            <View style={styles.actionContainer}>
                {actions.map((item, index) => {
                    const { title, titleStyle, actionStyle, onPress } = item
                    return (
                        <TouchableHighlight
                            underlayColor={'#eeeeee'}
                            style={styles.actionHighlight}
                            key={`action_${index}`}
                            onPress={() => this._onPress(onPress)}>
                            <View style={[styles.action, actionStyle, this.separator(index)]}>
                                <Text style={[styles.actionText, titleStyle]}>
                                    {title}
                                </Text>
                            </View>
                        </TouchableHighlight>
                    )
                })}
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this._renderTitle()}
                {this._renderDetail()}
                {this._rederAction()}
            </View>
        );
    }
}
// define your styles
const styles = StyleSheet.create({
    container: {
        width: Theme.alertWidth,
        minHeight: Theme.alertMinHeight,
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        overflow: 'hidden',
    },
    title: {
        marginTop: 20,
        maxWidth: Theme.alertTitleMaxWidth,
        textAlign: 'center',
        fontSize: Theme.alertTitleFontSize,
        fontWeight: 'bold',
        color: Theme.alertTitleColor,
    },
    detail: {
        marginTop: 12,
        maxWidth: Theme.alertDetailMaxWidth,
        textAlign: 'center',
        fontSize: Theme.alertDetailFontSize,
        lineHeight: scaleSize(40),
        color: Theme.alertDetailColor,
        fontFamily: Platform.OS === 'ios' ? 'PingFangSC-Regular' : null,
    },
    actionContainer: {
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Theme.alertWidth,
        height: Theme.alertActionHeight,
        borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: Theme.alertSeparatorColor,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    actionHighlight: {
        flex: 1,
        height: Theme.alertActionHeight,
    },
    action: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: Theme.alertActionHeight,
    },
    actionText: {
        color: Theme.alertActionColor,
        fontSize: Theme.alertActionFontSize,
    },
    separator: {
        borderRightWidth: StyleSheet.hairlineWidth,
        borderRightColor: Theme.alertSeparatorColor,
    },
});

