'use strict';
import React from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity, ViewPropTypes, CameraRoll, PermissionsAndroid } from 'react-native';
import { TransformView } from 'teaset';
import PropTypes from 'prop-types'
import ImageView from '../Image/ImageView';
import Video from 'react-native-video';
import Theme from '../../config/themes/Theme';

export default class LargePicture extends React.PureComponent {

    static propTypes = {
        style: ViewPropTypes.style,
        type: PropTypes.oneOf(['image', 'video']),
        imageStyle: ViewPropTypes.style,
    }

    static defaultProps = {
        type: 'image'
    }

    _onLongPress = () => {
        const { source } = this.props
        const save = async () => {
            if (__ANDROID__) {
                let perRes = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, null);
            }
            const result = await Services.download(source.uri)
            if (result) {
                const rollUri = __IOS__ ? result : `file://${result}`
                const saveRes = await CameraRoll.saveToCameraRoll(rollUri, 'photo')
                if (saveRes) {
                    ToastManager.message('图片已保存至相册');
                } else {
                    ToastManager.message('保存失败');
                }
            } else {
                ToastManager.message('下载失败')
            }
        }
        ActionsManager.show({
            title: '',
            actions: [
                { title: '保存图片', onPress: save }
            ]
        })
    }

    _onPressHide = () => {
        AlertManager.hide()
    }

    _onPressShow = () => {
        const { type, source } = this.props
        let content = null
        if (type === 'image') {
            content = (
                <TransformView
                    style={styles.transformView}
                    onLongPress={this._onLongPress}
                    onPress={this._onPressHide}>
                    <ImageView
                        style={styles.largeImage}
                        source={source}
                        maxImageWidth={SCREEN_WIDTH}
                        useOpacity={false}
                        resizeMode={'contain'}
                    />
                </TransformView>
            )

        } else {
            content = (
                <TouchableOpacity activeOpacity={1.0} onPress={this._onPressHide}>
                    <Video
                        style={styles.video}
                        paused={false}
                        source={source}
                        onLoad={this._onVideoEnd}
                    />
                </TouchableOpacity>
            )
        }
        AlertManager.showPopView(content, { type: 'none', overlayOpacity: 1.0, containerStyle: { flex: 1 } })
    }

    _onVideoEnd = () => {
        console.log('_onVideoEnd')
    }

    render() {
        const {
            type,
            style,
            imageStyle,
            source,
            ...others
        } = this.props
        let newSource = { ...source }
        if (type === 'video') {
            newSource.uri = `${newSource.uri}?vframe/jpg/offset/0`
        }
        return (
            <TouchableOpacity style={style} onPress={this._onPressShow}>
                <ImageView style={imageStyle} source={newSource} {...others} />
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    transformView: {

    },
    largeImage: {
        width: Theme.screenWidth,
    },
    video: {
        width: Theme.screenWidth,
        height: Theme.screenHeight
    }
});

