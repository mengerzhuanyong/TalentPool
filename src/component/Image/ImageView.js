'use strict';
import React from 'react';
import { StyleSheet, Image, View, Animated } from 'react-native';
import PropTypes from 'prop-types'
import FastImage from 'react-native-fast-image'

const ImageStatus = {
    START: 'START',  // 开始加载
    LOADING: 'LOADING', // 加载中
    END: 'END',  // 结束加载
    ERROR: 'ERROR' // 加载错误
}

export default class ImageView extends React.PureComponent {

    static propTypes = {
        ...FastImage.propTypes,
        useFastImage: PropTypes.bool, // 使用原生桥接的缓存image 
        maxImageWidth: PropTypes.number,
        useOpacity: PropTypes.bool,
    };

    static defaultProps = {
        ...FastImage.defaultProps,
        useFastImage: true,
        useOpacity: true,
        resizeMode: 'contain',
    };

    constructor(props) {
        super(props);
        this.state = { imageSize: null, imageStatus: ImageStatus.START }
        this._imageStatus = ImageStatus.START
        this.opacity = new Animated.Value(1.0)
    };

    componentWillUnmount() {
        this.opacity.stopAnimation()
    }

    _startOpacityAnimated = () => {
        this.opacity.setValue(1.0)
        Animated.timing(this.opacity, {
            toValue: 0,
            duration: 300,
            useNativeDriver: true
        }).start(() => {

        })
    }

    _onImageProgress = (event) => {
        const { onProgress } = this.props
        // Log.print('_onImageProgress', event.nativeEvent)
        onProgress && onProgress(event)
    };

    _onImageLoadStart = (event) => {
        // Log.print('_onImageLoadStart', event)
        const { onLoadStart } = this.props
        if (this._imageStatus === ImageStatus.START || this._imageStatus === ImageStatus.END) {
            this._imageStatus = ImageStatus.LOADING;
            this.setState({ imageStatus: ImageStatus.LOADING });
        }
        onLoadStart && onLoadStart(event)
    };

    _onImageLoad = (event) => {
        const { maxImageWidth, onLoad } = this.props
        const { width, height } = event.nativeEvent
        if (maxImageWidth) {
            this._setImageSize(width, height)
        }
        // Log.print('_onImageLoad', event.nativeEvent)
        onLoad && onLoad(event)
    };

    _onImageLoadEnd = (event) => {
        // Log.print('_onImageLoadEnd', event.nativeEvent)
        const { onLoadEnd, maxImageWidth, useOpacity } = this.props
        if (this._imageStatus === ImageStatus.LOADING) {
            this._imageStatus = ImageStatus.END;
            this.setState({ imageStatus: ImageStatus.END }, () => {
                maxImageWidth && useOpacity && this._startOpacityAnimated()
            })
        }
        onLoadEnd && onLoadEnd(event)
    };

    _onImageError = (event) => {
        // Log.print('_onImageError', event.nativeEvent)
        const { onError } = this.props
        this._imageStatus = ImageStatus.ERROR;
        this.setState({ imageStatus: ImageStatus.ERROR })
        onError && onError(event)
    };

    _setImageSize = (width, height) => {
        const { maxImageWidth } = this.props
        if (width >= maxImageWidth) {
            height = (maxImageWidth / width) * height
            width = maxImageWidth
        }
        this.setState({ imageSize: { width, height } })
    };

    renderPlaceholder = () => {
        const { maxImageWidth } = this.props
        if (!maxImageWidth && (this._imageStatus === ImageStatus.START || this._imageStatus === ImageStatus.LOADING)) {
            return (
                <View style={styles.placeholderCon}>
                    <Image
                        style={styles.placeholderImage}
                        source={Images.icon_image_placeholder}
                        resizeMode={'contain'}
                    />
                </View>
            )
        }
        return null
    };

    renderOpacityMask = () => {
        const { maxImageWidth, useOpacity } = this.props
        if (maxImageWidth && useOpacity) {
            return (
                <Animated.View
                    style={[styles.opacityMask, { opacity: this.opacity }]}
                />
            )
        }
        return null
    }

    renderImageView = () => {
        const { useFastImage, style, source, ...others } = this.props
        const { imageSize } = this.state
        if (useFastImage && typeof source !== 'number') {
            if (source && (source.uri === undefined || source.uri === null)) {
                source.uri = ''
            }
            if (source && typeof source.uri === 'string' && !source.uri.startsWith('http') && !source.uri.startsWith('file://')) {
                source.uri = `${ServicesApi.QI_NIU_LINKE}${source.uri}`
            }
            return (
                <FastImage
                    {...others}
                    style={[style, imageSize]}
                    source={{ priority: FastImage.priority.low, ...source }}
                    onLoadStart={this._onImageLoadStart}
                    onProgress={this._onImageProgress}
                    onLoad={this._onImageLoad}
                    onLoadEnd={this._onImageLoadEnd}
                    onError={this._onImageError}
                >
                    {this.renderPlaceholder()}
                    {this.renderOpacityMask()}
                </FastImage>
            )
        } else if (!useFastImage || typeof source === 'number') {
            return (
                <Image
                    {...others}
                    style={[style, imageSize]}
                    source={source}
                />
            )
        }
        return null
    }

    render() {
        return this.renderImageView()
    }
}


const styles = StyleSheet.create({
    placeholderCon: {
        ...StyleSheet.absoluteFillObject,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    placeholderImage: {
        width: '45%',
        height: '45%',
        backgroundColor: 'transparent',
    },
    opacityMask: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
    }
});

