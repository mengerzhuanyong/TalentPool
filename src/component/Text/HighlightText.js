'use strict';
import React from 'react';
import { View, Text, StyleSheet, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types'

export default class HighlightText extends React.PureComponent {

    static propTypes = {
        style: Text.propTypes.style,
        text: PropTypes.string.isRequired,
        highlightText: PropTypes.string,
        highlightStyle: Text.propTypes.style,
    };

    static defaultProps = {

    };

    constructor(props) {
        super(props);

    }

    render() {
        const { style, text, highlightText, highlightStyle } = this.props
        let startValue, splits = []
        if (text && highlightText) {
            splits = text.split(highlightText)
            startValue = splits[0]
        } else {
            startValue = text
        }
        // console.log('startValue', startValue, highlightText)
        return (
            <Text style={style}>{startValue}
                {splits.map((item, index) => {
                    if (index === 0) {
                        return null
                    }
                    return (
                        <Text style={highlightStyle} key={`${index}`}>{highlightText}
                            <Text style={style}>{item}</Text>
                        </Text>
                    )
                })}
            </Text>
        );
    }
}

const styles = StyleSheet.create({

});

