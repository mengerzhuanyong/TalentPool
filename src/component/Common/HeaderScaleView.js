'use strict';
import React from 'react';
import { View, Text, StyleSheet, Animated, Image, ViewPropTypes, ImageBackground } from 'react-native';
import NavigationBar from '../Navigation/NavigationBar';
import ListView from '../List/ListView';
import PropTypes from 'prop-types'

const AnimatedImageBackground = Animated.createAnimatedComponent(ImageBackground)
const AnimatedListView = Animated.createAnimatedComponent(ListView)

export default class HeaderScaleView extends React.PureComponent {

    static propTypes = {
        type: PropTypes.oneOf(['ScrollView', 'ListView']),
        scaleRatio: PropTypes.number,
        headerBarStyle: ViewPropTypes.style,
        headerStatusBarStyle: NavigationBar.propTypes.statusBarStyle,
        headerTitle: PropTypes.string,
        headerTitleStyle: NavigationBar.propTypes.titleStyle,
        headerScrollTitle: PropTypes.string,
        headerScrollTitleStyle: NavigationBar.propTypes.titleStyle,
        headerLeftAction: NavigationBar.propTypes.renderLeftAction,
        headerRightAction: NavigationBar.propTypes.renderRightAction,
        headerScaleImage: PropTypes.oneOfType([PropTypes.objectOf({ uri: PropTypes.string }), PropTypes.number]),
        headerScaleImageStyle: Image.propTypes.style,
        renderHeader: PropTypes.oneOfType([PropTypes.element, PropTypes.func]),
        headerContainerStyle: ViewPropTypes.style,
    }

    static defaultProps = {
        type: 'ScrollView',
        scaleRatio: 2.0,
    }

    constructor(props) {
        super(props)
        this.contentOffsetY = new Animated.Value(0)
    }

    renderNavBar = () => {
        const {
            headerTitle,
            headerTitleStyle,
            headerScrollTitle,
            headerScrollTitleStyle,
            headerLeftAction,
            headerRightAction,
            headerBarStyle,
            headerStatusBarStyle
        } = this.props
        const opacity = this.contentOffsetY.interpolate({
            inputRange: [Theme.statusBarHeight, Theme.statusBarHeight + Theme.navBarHeight],
            outputRange: [0, 1],
            extrapolateRight: 'clamp'
        })
        const antiOpacity = this.contentOffsetY.interpolate({
            inputRange: [Theme.statusBarHeight, Theme.statusBarHeight + Theme.navBarHeight],
            outputRange: [1, 0],
            extrapolateRight: 'clamp'
        })
        return (
            <View style={styles.navBarContainer}>
                <Animated.View pointerEvents={'none'} style={[styles.navBarBack, { opacity: opacity }]} />
                {headerTitle ? <Animated.View style={[styles.navTitleCon, { opacity: antiOpacity }]}>
                    <Text style={[styles.navTitle, headerTitleStyle]}>{headerTitle}</Text>
                </Animated.View> : null}
                {headerScrollTitle ? <Animated.View style={[styles.navTitleCon, { opacity }]}>
                    <Text style={[styles.navTitle, headerScrollTitleStyle]}>{headerScrollTitle}</Text>
                </Animated.View> : null}
                <NavigationBar
                    style={[styles.navBar, headerBarStyle]}
                    statusBarStyle={headerStatusBarStyle}
                    renderLeftAction={headerLeftAction}
                    renderRightAction={headerRightAction}
                />
            </View>
        )
    }

    renderBackHeader = () => {
        const {
            scaleRatio,
            renderHeader,
            headerScaleImage,
            headerScaleImageStyle,
            headerContainerStyle
        } = this.props
        const scale = this.contentOffsetY.interpolate({
            inputRange: [-32 * scaleRatio, 0],
            outputRange: [scaleRatio, 1],
            extrapolateRight: 'clamp'
        })
        const translateY = this.contentOffsetY.interpolate({
            inputRange: [-1, 0],
            outputRange: [-1, 0],
            extrapolateRight: 'clamp'
        })
        let headerChildren = null
        if (React.isValidElement(renderHeader)) {
            headerChildren = headerChildren
        } else if (typeof renderHeader === 'function') {
            headerChildren = renderHeader()
        }
        return (
            <View style={[styles.headerContainer, headerContainerStyle]}>
                <AnimatedImageBackground
                    resizeMode={'stretch'}
                    source={headerScaleImage}
                    style={[styles.backgroudImage, headerScaleImageStyle, {
                        transform: [{ translateY: translateY }, { scale: scale }]
                    }]}
                />
                {headerChildren}
            </View>
        )
    }

    renderView = () => {
        const { type, style, children, onScroll, ListHeaderComponent, scrollEventThrottle, ...others } = this.props
        if (type === 'ScrollView') {
            return (
                <Animated.ScrollView
                    style={styles.scroll}
                    scrollEventThrottle={1}
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { y: this.contentOffsetY } } }],
                        { useNativeDriver: true }
                    )}>
                    <this.renderBackHeader />
                    {children}
                </Animated.ScrollView>
            )
        } else if (type === 'ListView') {
            return (
                <AnimatedListView
                    {...others}
                    scrollEventThrottle={1}
                    ListHeaderComponent={this.renderBackHeader}
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { y: this.contentOffsetY } } }],
                        { useNativeDriver: true }
                    )}
                />
            )
        }
        return null
    }

    render() {
        const { type, children } = this.props
        return (
            <View style={styles.container}>
                <this.renderNavBar />
                <this.renderView />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    navBarContainer: {
        width: '100%',
        position: 'absolute',
        zIndex: 10,
    },
    navBarBack: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: Theme.navBarBackgroundColor,
    },
    navTitleCon: {
        position: 'absolute',
        top: Theme.statusBarHeight,
        height: Theme.navBarHeight,
        alignSelf: 'center',
        justifyContent: 'center',
        zIndex: 10,
    },
    navTitle: {
        textAlign: 'center',
        textAlignVertical: 'center',
        color: Theme.navBarTitleColor,
        fontSize: Theme.navBarTitleFontSize,
    },
    navBar: {
        backgroundColor: 'transparent',
        borderBottomWidth: 0,
    },
    headerContainer: {
        paddingTop: Theme.statusBarHeight + Theme.navBarHeight
    },
    backgroudImage: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
    },
});

