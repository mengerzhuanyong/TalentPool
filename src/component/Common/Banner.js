'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Swiper from 'react-native-swiper';
import Theme from '../../config/themes/Theme';

export default class Banner extends React.PureComponent {

    render() {
        return (
            <Swiper
                loop={true}
                autoplay={true}
                dotColor={'#80524a'}
                activeDotColor={'#b8b8b8'}
                paginationStyle={styles.paginationStyle}
                width={Theme.screenWidth}
                height={Theme.screenWidth * 0.427}
                {...this.props}
            />
        );
    }
}

const styles = StyleSheet.create({
    paginationStyle: {
        bottom: 10
    }
});