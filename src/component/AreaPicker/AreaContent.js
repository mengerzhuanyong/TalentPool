'use strict'
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import { Wheel } from 'teaset';
import Theme from '../../config/themes/Theme';
import AddressDataFunc from './AddressData';

export default class AreaContent extends React.PureComponent {

    constructor(props) {
        super(props)
        const { enableAll } = this.props
        const data = AddressDataFunc()
        let oriProvince = [], oriCity = [], oriArea = []
        if (enableAll) {
            oriProvince.push({ provinceCode: '', provinceName: "全国", mallCityList: [{ cityName: '', mallAreaList: [{ areaName: '' }] }] })
            oriCity.push([{ cityCode: '', cityName: '', mallAreaList: [{ areaName: '' }] }])
            oriArea.push([[{ areaCode: '', areaName: '' }]])
            data.map((item, index) => {
                let newItem = { ...item }
                if (newItem.provinceCode === '120000' || newItem.provinceCode === '110000' ||
                    newItem.provinceCode === '500000' || newItem.provinceCode === '310000' ||
                    newItem.provinceCode === '820000' || newItem.provinceCode === '810000') {
                    if (newItem.mallCityList[0] && newItem.mallCityList[0]['cityName'] !== '全市') {
                        newItem.mallCityList.unshift({ cityCode: '', cityName: "全市", mallAreaList: [] })
                    }
                } else {
                    if (newItem.mallCityList[0] && newItem.mallCityList[0]['cityName'] !== '全省') {
                        newItem.mallCityList.unshift({ cityCode: '', cityName: "全省", mallAreaList: [] })
                    }
                }
                let areas = []
                newItem.mallCityList.forEach((cityItem, index) => {
                    // console.log('cityItem', cityItem)
                    if (cityItem.areaName !== '' && cityItem.areaName !== '全省' && cityItem.areaName !== '全市' && newItem.provinceCode != '710000') {
                        if (cityItem['mallAreaList'][0] && cityItem['mallAreaList'][0]['areaName'] !== '全市') {
                            cityItem['mallAreaList'].unshift({ areaCode: '', areaName: '全市' })
                        }
                    }
                    areas.push(cityItem.mallAreaList)
                });
                oriProvince.push(newItem)
                oriCity.push(item.mallCityList)
                oriArea.push(areas)
            })
        } else {
            data.map((item, index) => {
                let areas = []
                item.mallCityList.forEach((cityItem, index) => {
                    areas.push(cityItem.mallAreaList)
                });
                oriProvince.push(item)
                oriCity.push(item.mallCityList)
                oriArea.push(areas)
            })
        }
        this.oriProvince = oriProvince
        this.oriCity = oriCity
        this.oriArea = oriArea
        this.state = { provinceIndex: 0, cityIndex: 0, areaIndex: 0 }
    }

    componentDidMount() {

    }

    _onProChange = (index) => {
        this.setState({ provinceIndex: index, cityIndex: 0, areaIndex: 0 })
    }

    _onCityChange = (index) => {
        this.setState({ cityIndex: index, areaIndex: 0 })
    }

    _onAreaChange = (index) => {
        this.setState({ areaIndex: index })
    }

    _onPressOK = () => {
        requestAnimationFrame(() => {
            const { provinceIndex, cityIndex, areaIndex } = this.state
            const { onPress, enableAll } = this.props
            const province = this.oriProvince[provinceIndex]
            const city = this.oriCity[provinceIndex][cityIndex]
            const district = this.oriArea[provinceIndex][cityIndex][areaIndex]
            let returnData = {}
            if (enableAll) {
                if (province.provinceName === '全国') {
                    returnData = {
                        province: { code: '', name: '' },
                        city: { code: '', name: '' },
                        district: { code: '', name: '' },
                    }
                } else if (city.cityName === '全市' || city.cityName === '全省') {
                    returnData = {
                        province: { code: province.provinceCode, name: province.provinceName },
                        city: { code: '', name: '' },
                        district: { code: '', name: '' },
                    }
                } else if (district.areaName === '全市') {
                    returnData = {
                        province: { code: province.provinceCode, name: province.provinceName },
                        city: { code: city.cityCode, name: city.cityName },
                        district: { code: '', name: '' },
                    }
                } else {
                    returnData = {
                        province: { code: province.provinceCode, name: province.provinceName },
                        city: { code: city.cityCode, name: city.cityName },
                        district: { code: district.areaCode, name: district.areaName },
                    }
                }
            } else {
                returnData = {
                    province: { code: province.provinceCode, name: province.provinceName },
                    city: { code: city.cityCode, name: city.cityName },
                    district: { code: district.areaCode, name: district.areaName },
                }
            }
            onPress && onPress(returnData)
        })
        ActionsManager.hide()
    }

    _onPressCancel = () => {
        requestAnimationFrame(() => {
            ActionsManager.hide()
        })
    }

    renderProvinceItem = (item, index) => {
        return (
            <Text style={styles.itemText}>{item.provinceName}</Text>
        )
    }

    renderCityItem = (item, index) => {
        return (
            <Text style={styles.itemText}>{item.cityName}</Text>
        )
    }

    renderAreaItem = (item, index) => {
        return (
            <Text style={styles.itemText}>{item.areaName}</Text>
        )
    }


    render() {
        const { provinceIndex, cityIndex, areaIndex } = this.state
        return (
            <View style={styles.container}>
                <View style={styles.actionContainer}>
                    <TouchableOpacity style={styles.button} onPress={this._onPressCancel}>
                        <Text style={styles.actionText}>取消</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this._onPressOK}>
                        <Text style={styles.actionText}>确定</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.wheelContainer}>
                    <Wheel
                        style={styles.wheelStyle}
                        itemStyle={styles.itemStyle}
                        index={provinceIndex}
                        items={this.oriProvince}
                        renderItem={this.renderProvinceItem}
                        // defaultIndex={this.years.findIndex((item) => item == currentYear)}
                        onChange={this._onProChange}
                    />
                    <Wheel
                        style={styles.wheelStyle}
                        itemStyle={styles.itemStyle}
                        index={cityIndex}
                        items={this.oriCity[provinceIndex]}
                        renderItem={this.renderCityItem}
                        // defaultIndex={this.months.findIndex((item) => item == currentMounth)}
                        onChange={this._onCityChange}
                    />
                    <Wheel
                        style={styles.wheelStyle}
                        itemStyle={styles.itemStyle}
                        index={areaIndex}
                        items={this.oriArea[provinceIndex][cityIndex]}
                        renderItem={this.renderAreaItem}
                        // defaultIndex={days.findIndex((item) => item == currentDay)}
                        onChange={this._onAreaChange}
                    />
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        paddingBottom: Theme.isIPhoneX ? Theme.fitIPhoneXBottom : 0,
    },
    wheelContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10,
    },
    wheelStyle: {
        height: 180,
        width: Theme.screenWidth / 3,
    },
    itemStyle: {
        textAlign: "center",
        fontSize: 14,
    },
    actionContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingBottom: 10,
        paddingHorizontal: 30,
    },
    actionText: {
        color: Theme.overallColor,
        fontSize: 14,
    },
    itemText: {
        // backgroundColor: 'red',
        textAlign: 'center',
        fontSize: 12,
        color: '#333'
    },
    button: {
        padding: 10
    }
});

