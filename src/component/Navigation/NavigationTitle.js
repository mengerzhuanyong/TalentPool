'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Theme from '../../config/themes/Theme'

export default class NavigationTitle extends React.PureComponent {

    static propTypes = {
        title: PropTypes.oneOfType([PropTypes.string, PropTypes.func, PropTypes.element]),
        titleStyle: Text.propTypes.style,
        leftActionWidth: PropTypes.number,
        rightActionWidth: PropTypes.number,
    };

    static defaultProps = {
        leftActionWidth: 10,
        rightActionWidth: 10,
    };

    renderContent = () => {
        const { title, titleStyle } = this.props
        if (typeof title === 'string') {
            return (
                <Text
                    style={[styles.title, titleStyle]}
                    numberOfLines={1}
                    ellipsizeMode={'middle'}>
                    {title}
                </Text>
            )
        } else if (typeof title === 'function') {
            return title()
        } else if (React.isValidElement(title)) {
            return title
        }
        return null
    }

    buildStyle = () => {
        const { leftActionWidth, rightActionWidth } = this.props
        const maxWidth = Math.max(leftActionWidth, rightActionWidth) + Theme.navBarPadding * 2
        return { width: Theme.screenWidth - maxWidth * 2 }
    };

    render() {
        const { style } = this.props
        const defaultStyle = this.buildStyle()
        return (
            <View style={[defaultStyle, style]}>
                {this.renderContent()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'center',
        color: Theme.navBarTitleColor,
        fontSize: Theme.navBarTitleFontSize,
    }
});

