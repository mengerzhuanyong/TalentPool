'use strict';
import React from 'react';
import { View, Text, StyleSheet, Image, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'
import Theme from '../../config/themes/Theme';

export default class ToastView extends React.PureComponent {

    static propTypes = {
        type: PropTypes.oneOf(['message', 'success', 'fail', 'warn', 'loading']),
        text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    };

    static defaultProps = {
        type: 'message'
    };

    constructor(props) {
        super(props)
        this.rotate = new Animated.Value(0)
        this.loopAnimated = null
    }

    componentDidMount() {
        const { type } = this.props
        type === 'loading' && this.startRotateAnimated()
    }

    componentWillUnmount() {
        const { type } = this.props
        type === 'loading' && this.stopRotateAnimated()
    }

    startRotateAnimated = () => {
        const timingAnimated = Animated.timing(this.rotate, {
            toValue: 3600,
            duration: 10000,
            easing: Easing.linear,
            useNativeDriver: true
        })
        this.loopAnimated = Animated.loop(timingAnimated, { iterations: -1 })
        this.loopAnimated.start((event) => {

        });
    }

    stopRotateAnimated = () => {
        this.loopAnimated.stop()
        this.rotate.stopAnimation()
        this.loopAnimated = null
        this.rotate = null
    }

    renderIcon = () => {
        const { type } = this.props
        let source = null
        if (type === 'message') {
            return null
        } else if (type === 'success') {
            source = Images.icon_toast_success
        } else if (type === 'fail') {
            source = Images.icon_toast_fail
        } else if (type === 'warn') {
            source = Images.icon_toast_warn
        } else if (type === 'loading') {
            source = Images.icon_toast_loading
        } else {
            return null
        }
        const rotate = this.rotate.interpolate({
            inputRange: [0, 360],
            outputRange: ['0deg', '360deg'],
        })
        return (
            <Animated.Image
                style={[styles.loadingImage, { transform: [{ rotate }] }]}
                resizeMode={'contain'}
                source={source}
            />
        )
    }

    renderText = () => {
        const { text } = this.props
        if (typeof text === 'string') {
            return (
                <Text style={styles.toastText}>{text}</Text>
            );
        } else if (React.isValidElement(text)) {
            return text
        }
        return null
    }

    buildStyle = () => {
        const { type } = this.props
        let style = null
        if (type === 'message') {
            style = {
                paddingVertical: 10,
                paddingHorizontal: 20,
                borderRadius: 5,
            }
        } else {
            style = {
                paddingTop: 22,
                paddingBottom: 17,
                paddingHorizontal: 20,
                borderRadius: 10,
                minWidth: 125,
                maxWidth: Theme.screenWidth - 80 * 2,
            }
        }
        return { style }
    }

    render() {
        const { style } = this.buildStyle()
        return (
            <View style={[styles.container, style]}>
                {this.renderIcon()}
                {this.renderText()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(35,24,21,0.8)',
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: Theme.screenWidth - 40 * 2,
    },
    loadingImage: {
        width: 48,
        height: 48,
        marginBottom: 10,
    },
    toastText: {
        fontSize: Theme.toastTextFontSize,
        color: Theme.toastTextColor,
        lineHeight: 20
    }
});

