//import liraries
import React from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types'
import Images from '../../asset/index';
import { scaleSize } from '../../util/Adaptation';
import Theme from '../../config/themes/Theme'

export default class ShareContent extends React.PureComponent {

    static propTypes = {
        source: PropTypes.arrayOf(PropTypes.shape({
            title: PropTypes.string,
            icon: PropTypes.number,
            type: PropTypes.oneOf(['weixin', 'timeline', 'qq', 'qqzone', 'weibo'])
        })),
        otherSource: PropTypes.arrayOf(PropTypes.shape({
            title: PropTypes.string,
            icon: PropTypes.number,
            type: PropTypes.oneOf(['copy'])
        }))
    };

    static defaultProps = {
        source: [
            { title: '微信好友', icon: Images.icon_share_wechat, type: 'weixin' },
            { title: '朋友圈', icon: Images.icon_share_timeline, type: 'timeline' }
        ],
        otherSource: [
            { title: '复制', icon: Images.icon_share_link, type: 'copy' }
        ]
    };

    constructor(props) {
        super(props);

    }

    _onPressAcion = (type) => {
        const { onPress } = this.props
        onPress && onPress(type)
        ActionsManager.hide()
    }

    _onPressCancel = () => {
        ActionsManager.hide()
    }

    renderContent = (dataSource, key) => {
        return (
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                {dataSource.map((item, index) => {
                    return (
                        <View key={`${key}_${index}`} style={styles.actionContainer}>
                            <TouchableOpacity
                                style={styles.actionBack}
                                onPress={() => this._onPressAcion(item.type)}>
                                <Image
                                    style={styles.actionImage}
                                    source={item.icon}
                                />
                            </TouchableOpacity>
                            <Text style={styles.actionText}>{item.title}</Text>
                        </View>
                    )
                })}
            </ScrollView>
        )
    }

    renderCancelAction = () => {
        return (
            <TouchableOpacity style={styles.cancelButton} onPress={this._onPressCancel}>
                <Text style={styles.cancelText}>取消</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const { source, otherSource } = this.props
        return (
            <View style={styles.container}>
                {this.renderContent(source, 'share')}
                <View style={styles.separator} />
                {this.renderContent(otherSource, 'other')}
                {this.renderCancelAction()}
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: Theme.shareBackColor,
        paddingBottom: Theme.isIPhoneX ? Theme.fitIPhoneXBottom : 0,
    },
    actionContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 17,
        marginTop: 12,
        marginBottom: 12,
    },
    actionBack: {
        width: Theme.shareActionWidth,
        height: Theme.shareActionHeight,
        borderRadius: Theme.shareActionRadius,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F8F8FF',
    },
    actionText: {
        marginTop: 7,
        // marginBottom: ShareTheme.shareActionMargin,
        color: Theme.shareActionTextColor,
        fontSize: 11,
    },
    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#cdcdcd',
    },
    cancelButton: {
        height: Theme.shareCancelActionHeight,
        backgroundColor: Theme.shareCancelBackColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cancelText: {
        fontSize: 14,
        color: Theme.shareCancelTextColor
    },
    actionImage: {
        width: scaleSize(80),
        height: scaleSize(80),
        // backgroundColor: 'red',
    }
});


