'use strict';
import React from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types'
import Theme from '../../config/themes/Theme'


export default class ActionContent extends React.PureComponent {

    static propTypes = {
        title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
        titleStyle: Text.propTypes.style,
        actions: PropTypes.arrayOf(PropTypes.shape({ title: PropTypes.string, titleStyle: Text.propTypes.style, onPress: PropTypes.func })),
    };

    static defaultProps = {
        title: '',
        actions: [],
    };

    constructor(props) {
        super(props)
        this.lastActionTime = 0
        this.interval = 500
    };

    _onPressAction = (onPress) => {
        let nowTime = Moment().format('x')
        if ((nowTime - this.lastActionTime) <= this.interval) {
            // console.warn('间隔时间内重复点击了');
            return;
        }
        this.lastActionTime = nowTime;
        onPress && onPress();
        ActionsManager.hide();
    }

    _onPressCancel = () => {
        let nowTime = Moment().format('x')
        if ((nowTime - this.lastActionTime) <= this.interval) {
            // console.warn('间隔时间内重复点击了');
            return;
        }
        this.lastActionTime = nowTime;
        ActionsManager.hide()
    }

    renderTitle = () => {
        const { title, titleStyle } = this.props
        if (React.isValidElement(title)) {
            return title
        } else if (title) {
            return (
                <View style={styles.titleContainer}>
                    <Text style={[styles.title, titleStyle]}>{title}</Text>
                </View>
            )
        }
        return null
    }

    renderContent = () => {
        const { actions } = this.props
        return (
            <ScrollView
                style={styles.contentContainer}
                bounces={true}
            >
                {actions.map((item, index) => {
                    const { title, titleStyle, onPress } = item
                    const borderTopWidth = index === 0 ? 0 : StyleSheet.hairlineWidth
                    return (
                        <TouchableHighlight
                            key={`action_item${index}`}
                            style={[styles.actionContainer, { borderTopWidth }]}
                            underlayColor={'#eeeeee'}
                            onPress={() => this._onPressAction(onPress)}>
                            <Text style={[styles.actionText, titleStyle]}>{title}</Text>
                        </TouchableHighlight>
                    )
                })}
            </ScrollView>
        )
    }

    renderCancelAction = () => {
        return (
            <TouchableHighlight
                style={styles.cancelActionContainer}
                underlayColor={'#eeeeee'}
                onPress={this._onPressCancel}>
                <Text style={styles.cancelActionText}>取消</Text>
            </TouchableHighlight>
        )
    }

    render() {
        const { title } = this.props
        return (
            <View style={styles.container}>
                {this.renderTitle()}
                {this.renderContent()}
                <View style={styles.sep} />
                {this.renderCancelAction()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        paddingBottom: Theme.isIPhoneX ? Theme.fitIPhoneXBottom : 0,
    },
    titleContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: 'rgba(127, 127, 127, 0.3)',
        paddingVertical: 15,
    },
    title: {
        fontSize: Theme.titleFontSize,
        color: Theme.titleColor
    },
    contentContainer: {
        maxHeight: Theme.actionMaxHeight,
    },
    actionContainer: {
        backgroundColor: '#fff',
        borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: 'rgba(127, 127, 127, 0.3)',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 15,
    },
    actionText: {
        fontSize: Theme.actionTitleFontSize,
        color: Theme.actionTitleColor
    },
    sep: {
        height: 5,
        backgroundColor: 'rgba(127, 127, 127, 0.3)',
    },
    cancelActionContainer: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 15,
    },
    cancelActionText: {
        fontSize: Theme.cancelTitleFontSize,
        color: Theme.cancelTitleColor
    },
});
