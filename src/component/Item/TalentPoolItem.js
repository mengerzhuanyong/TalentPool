/**
 * 人才库 - 人才列表
 * http://menger.me
 * @大梦
 */


'use strict';

import React from 'react'
import {StyleSheet, Text, View,} from 'react-native'
import {HorizontalLine} from "../Common/CommonLine";
import TalentLevel from "../TalentPool/TalentLevel";
import TalentWorkStatus from "../TalentPool/TalentWorkStatus";
import Button from "../Touchable/Button";

export default class TalentPoolItem extends React.Component {

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
    }

    renderBtnContent = (item) => {
        let BtnItem1 = (
            <Button
                title={'推荐岗位'}
                icon={Images.icon_user_plane}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem2 = (
            <Button
                title={'不可用'}
                icon={Images.icon_close_red}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem3 = (
            <Button
                title={'分配'}
                icon={Images.icon_success}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem4 = (
            <Button
                title={'取消候选'}
                icon={Images.icon_user_exit}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem5 = (
            <Button
                title={'进入面试'}
                icon={Images.icon_exchange}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem6 = (
            <Button
                title={'面试不通过'}
                icon={Images.icon_user_exit}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem7 = (
            <Button
                title={'进入体检'}
                icon={Images.icon_exchange}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem8 = (
            <Button
                title={'体检不成功'}
                icon={Images.icon_user_exit}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem9 = (
            <Button
                title={'进入待入职'}
                icon={Images.icon_exchange}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem10 = (
            <Button
                title={'人才安排'}
                icon={Images.icon_user_plane}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem11 = (
            <Button
                title={'确认入职'}
                icon={Images.icon_success}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem12 = (
            <Button
                title={'放弃入职'}
                icon={Images.icon_close_red}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem13 = (
            <Button
                title={'办理离职'}
                icon={Images.icon_user_exit}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem14 = (
            <Button
                title={'更换岗位'}
                icon={Images.icon_exchange}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem15 = (
            <Button
                title={'推荐岗位'}
                icon={Images.icon_user_card}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem16 = (
            <Button
                title={'推荐岗位'}
                icon={Images.icon_user_exit}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem17 = (
            <Button
                title={'不可用'}
                icon={Images.icon_exchange}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        let BtnItem18 = (
            <Button
                title={'恢复可用'}
                icon={Images.icon_user_card}
                style={[Theme.CCC, styles.btnItemStyle]}
                titleStyle={styles.btnTitleStyle}
                iconStyle={styles.btnIconStyle}
            />
        );
        return (
            <View style={[Theme.RCA, styles.btnContentStyle]}>
                {BtnItem1}
                {BtnItem2}
                {BtnItem3}
            </View>
        );
    };

    render() {
        let {ready, dataSource} = this.state;
        let {style, item} = this.props;
        return (
            <View style={[styles.container, style]}>
                <View style={[Theme.RCB, styles.talentItemInfo]}>
                    <View style={[Theme.RCS, styles.talentNameInfo]}>
                        <Text style={styles.talentName}>{item.name || '梦'}</Text>
                        <TalentLevel level={item.level}/>
                    </View>
                    <TalentWorkStatus status={item.status_str}/>
                </View>
                <View style={[Theme.RCB, styles.talentDetailInfo]}>
                    <View style={styles.detailLeftCon}>
                        <View style={[Theme.RCS, styles.talentProfileInfo]}>
                            <Text style={styles.detailItem}>{item.sex_str || '男'}</Text>
                            <Text style={styles.detailItem}>{item.age || 20}</Text>
                            <Text style={styles.detailItem}>{item.city || '青岛'}</Text>
                            <Text style={styles.detailItem}>|</Text>
                            <Text style={[styles.detailItem, styles.detailItemCur]}>{item.phone || '15066886006'}</Text>
                        </View>
                        <View style={[Theme.RCS, styles.flexSource]}>
                            <Text style={styles.sourceTitle}>来源:</Text>
                            <Text style={styles.sourceDetail}>{item.channel_name || 'BOSS直聘'}</Text>
                        </View>
                    </View>
                    <View style={styles.detailRightCon}>
                        <Text style={styles.cuIconRight}/>
                    </View>
                </View>
                <HorizontalLine style={styles.horizontalLine}/>
                {this.renderBtnContent(item)}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        paddingVertical: 15,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
    },
    talentItemInfo: {},
    talentNameInfo: {},
    talentName: {
        fontSize: 18,
        color: '#333',
        marginRight: 10,
    },
    talentDetailInfo: {},
    detailLeftCon: {},
    talentProfileInfo: {
        marginTop: 5,
        marginBottom: 10,
    },
    detailItem: {
        color: '#666',
        fontSize: 14,
        marginRight: 10,
    },
    detailItemCur: {
        color: '#333',
    },
    flexSource: {},
    sourceTitle: {
        fontSize: 14,
        color: '#333',
        marginRight: 5,
    },
    sourceDetail: {},
    detailRightCon: {
        width: 40,
        height: 40,
        marginLeft: 10,
    },
    cuIconRight: {},
    lineStyle: {},
    horizontalLine: {
        marginVertical: 10,
    },

    btnContentStyle: {
    },
    btnItemStyle: {
        backgroundColor: 'transparent',
    },
    btnTitleStyle: {
        color: '#333',
    },
    btnIconStyle: {
        marginBottom: 8,
    },
});