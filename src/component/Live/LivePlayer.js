// 'use strict';
import React from 'react';
import { View, Text, StyleSheet, requireNativeComponent, NativeModules, findNodeHandle, UIManager } from 'react-native';

export default () => <View />
// // import KSYVideo from 'react-native-ksyvideo'
// import PropTypes from 'prop-types'

// const LivePlayerManager = NativeModules.LivePlayerManager
// const LivePlayer = requireNativeComponent('LivePlayer', LivePlayerComponent)

// class LivePlayerComponent extends React.PureComponent {

//     static propTypes = {
//         source: PropTypes.shape({ uri: PropTypes.string }).isRequired,
//         onLiveLoadStart: PropTypes.func
//     }

//     static defaultProps = {

//     }

//     constructor(props) {
//         super(props);
//         this.isPlaying = true
//         this._liverHandle = null
//     }

//     componentDidMount() {

//     }

//     componentWillUnmount() {
//         // console.log('componentWillUnmount')
//         LivePlayerManager.releasePlayer(this._liverHandle)
//     }

//     // 播放或者暂停
//     pauseResume = () => {
//         if (this.isPlaying) {
//             LivePlayerManager.pause(this._liverHandle)
//         } else {
//             LivePlayerManager.resume(this._liverHandle)
//         }
//         this.isPlaying = !this.isPlaying
//     }

//     seek = (time) => {
//         LivePlayerManager.seek(this._liverHandle, time)
//     }

//     replay = () => {
//         LivePlayerManager.replay(this._liverHandle)
//         this.isPlaying = true
//     }

//     _onLiveLoadStart = () => {
//         // console.log('_onLiveLoadStart')
//         const { onLiveLoadStart } = this.props
//         LivePlayerManager.start(this._liverHandle)
//         onLiveLoadStart && onLiveLoadStart()
//     }

//     _onLiveLoadProgress = (event) => {
//         const { onLiveLoadProgress } = this.props
//         // console.log('_onLiveLoadProgress', event.nativeEvent)
//         onLiveLoadProgress && onLiveLoadProgress(event.nativeEvent.currentTime)
//     }

//     _captureRef = (v) => {
//         if (v) {
//             this.livePlayerRef = v
//             this._liverHandle = findNodeHandle(v)
//         } else {
//             this.livePlayerRef = null
//             this._liverHandle = null
//         }
//     }

//     render() {
//         const { source, style } = this.props
//         // console.log('LivePlayer')
//         return (
//             <LivePlayer
//                 ref={this._captureRef}
//                 style={style}
//                 source={source}
//                 onLiveLoadStart={this._onLiveLoadStart}
//                 onLiveLoadProgress={this._onLiveLoadProgress}
//             />
//         );
//     }
// }


// class LivePlayerAndroid extends React.PureComponent {

//     static propTypes = {
//         source: PropTypes.shape({ uri: PropTypes.string }).isRequired,
//         onLiveLoadStart: PropTypes.func
//     }

//     static defaultProps = {

//     }

//     constructor(props) {
//         super(props);
//         this.state = { isPlaying: false }
//         this._liverHandle = null
//     }

//     componentDidMount() {

//     }

//     componentWillUnmount() {
//         // console.log('componentWillUnmount')

//     }

//     componentWillReceiveProps(nextProps) {
//         const newUri = nextProps.source.uri
//         const oldUri = this.props.source.uri
//         if (newUri != oldUri) {
//             // console.log('componentWillReceiveProps', newUri, oldUri)
//             this.livePlayerRef.setNativeProps({ src: { uri: newUri } });
//         }
//     }

//     // 播放或者暂停
//     pauseResume = () => {
//         const { isPlaying } = this.state
//         if (isPlaying) {
//             this.setState({ isPlaying: false })
//         } else {
//             this.setState({ isPlaying: true })
//         }
//     }

//     seek = (time) => {
//         this.livePlayerRef.seek(time)
//     }

//     replay = () => {
//         UIManager.dispatchViewManagerCommand(
//             findNodeHandle(this.livePlayerRef.refs['KSYVideo']),
//             UIManager.RCTKSYVideo.Commands.replay,
//             null
//         );
//     }

//     _onLiveLoadStart = () => {
//         // console.log('_onLiveLoadStart')
//         const { onLiveLoadStart } = this.props
//         this.setState({ isPlaying: true })
//         onLiveLoadStart && onLiveLoadStart()
//     }

//     _onLiveLoadProgress = (event) => {
//         const { onLiveLoadProgress } = this.props
//         onLiveLoadProgress && onLiveLoadProgress(event.currentTime)
//     }

//     _onVideoError = () => {
//         // console.log('_onVideoError')
//     }

//     _captureRef = (v) => {
//         if (v) {
//             this.livePlayerRef = v
//             this._liverHandle = findNodeHandle(v)
//         } else {
//             this.livePlayerRef = null
//             this._liverHandle = null
//         }
//     }

//     render() {
//         const { style, source } = this.props
//         const { isPlaying } = this.state
//         // console.log('KSYVideo')
//         return (
//             <KSYVideo
//                 style={style}
//                 source={source}
//                 ref={this._captureRef}
//                 volume={1.0}
//                 muted={false}
//                 paused={!isPlaying}
//                 resizeMode={'cover'}
//                 repeat={false}
//                 playInBackground={false}
//                 onLoadStart={this._onLiveLoadStart}
//                 onVideoError={this._onVideoError}
//                 onProgress={this._onLiveLoadProgress}
//             />
//         );
//     }
// }


// const styles = StyleSheet.create({

// });

// export default __IOS__ ? LivePlayerComponent : LivePlayerAndroid
