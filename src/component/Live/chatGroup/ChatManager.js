'use strict';
import Constants from './Constants';

class ChatManager {

    // params = {
    //     token:'',
    //     webSocketUrl:'',
    //     onChatMessage:()=>{}
    // }
    constructor(params) {
        this.heartbeatFrequency = 5000 // 心跳频率
        this.heartbeatInterval = null
        this.wsInstance = null
        this.chatParams = params
        this.createWebSocket(params)
        // setTimeout(() => {
        //     // 模拟断掉socket
        //     this.wsInstance && this.wsInstance.close()
        // }, 10000);
    }

    createWebSocket = (params) => {
        if (this.wsInstance) {
            // 保证只有一个实例
            this.wsInstance.close();
            this.wsInstance = null
        }
        const webSocketUrl = params.webSocketUrl
        this.wsInstance = new WebSocket(webSocketUrl);
        this.wsInstance.onopen = this.onOpen
        this.wsInstance.onmessage = this.onMessage
        this.wsInstance.onerror = this.onError
        this.wsInstance.onclose = this.onClose
        // console.log('createWebSocket', this.heartbeatInterval)
    }

    closeWebSocket = () => {
        // console.log('closeWebSocket', this.heartbeatInterval)
        this.closeHeartbeat()
        this.wsInstance.close();
        this.wsInstance = null
    }

    reconnectionWebSocket = () => {
        if (this.wsInstance) {
            this.wsInstance.close();
            this.wsInstance = null
        }
        const webSocketUrl = this.chatParams.webSocketUrl
        this.wsInstance = new WebSocket(webSocketUrl);
        this.wsInstance.onopen = this.onOpen
        this.wsInstance.onmessage = this.onMessage
        this.wsInstance.onerror = this.onError
        this.wsInstance.onclose = this.onClose
    }

    openHeartbeat = () => {
        this.closeHeartbeat()
        this.heartbeatInterval = setInterval(this.heartbeatDetection, this.heartbeatFrequency)
    }

    closeHeartbeat = () => {
        clearInterval(this.heartbeatInterval)
        this.heartbeatInterval = null
    }

    heartbeatDetection = () => {
        // readonly CLOSED: 3; // 已经关闭
        // readonly CLOSING: number; // 正在关闭
        // readonly CONNECTING: number; // 正在连接
        // readonly OPEN: 1; // 已经打开
        if (this.wsInstance) {
            // console.log('心跳检测---', this.wsInstance.readyState)
            if (this.wsInstance.readyState === this.wsInstance.OPEN) {
                this.wsInstance.send('pong')
            } else if (this.wsInstance.readyState === this.wsInstance.CLOSED) {
                // 没有手动关闭，手动关闭会清除定时器
                // console.log('socket断开连接')
                this.reconnectionWebSocket()
            }
        }
    }

    // 注册接口
    registerInfo = () => {
        const { user } = this.chatParams
        const info = {
            eventName: Constants.REGISTER,
            data: {
                send_uid: user.id
            }
        }
        // console.log('register', info)
        this.wsInstance.send(JSON.stringify(info))
    }

    sendMessage = (params) => {
        // console.log('sendMessage', params)
        if (this.wsInstance) {
            if (this.wsInstance.readyState === this.wsInstance.OPEN) {
                this.wsInstance.send(JSON.stringify(params))
            }
        }
    }

    onOpen = (event) => {
        // console.log('onOpen', event)
        // 打开后先发送注册的消息
        this.registerInfo()
        // 建立心跳
        this.openHeartbeat()
    }

    onMessage = (event) => {
        const jsonData = JSON.parse(event.data)
        // console.log('onMessage', jsonData)
        this.chatParams.onChatMessage && this.chatParams.onChatMessage(jsonData)
    }

    onError = (event) => {
        // console.log('onError', event)
    }

    onClose = (event) => {
        // console.log('onClose', event)
    }

}

export default ChatManager