'use strict';

class ChatManager {

    constructor(params) {
        this.heartbeatFrequency = 5000 // 心跳频率
        this.heartbeatInterval = null
        this.wsInstance = null
        this.chatParams = params
        this.createWebSocket(params)
        // setTimeout(() => {
        //     // 模拟断掉socket
        //     this.wsInstance && this.wsInstance.close()
        // }, 10000);
    }

    createWebSocket = (params) => {
        if (this.wsInstance) {
            // 保证只有一个实例
            this.wsInstance.close();
            this.wsInstance = null
        }
        const webSocketUrl = params.webSocketUrl
        this.wsInstance = new WebSocket(webSocketUrl);
        this.wsInstance.onopen = this.onOpen
        this.wsInstance.onmessage = this.onMessage
        this.wsInstance.onerror = this.onError
        this.wsInstance.onclose = this.onClose
    }

    closeWebSocket = () => {
        this.closeHeartbeat()
        this.wsInstance.close();
        this.wsInstance = null
    }

    reconnectionWebSocket = () => {
        if (this.wsInstance) {
            this.wsInstance.close();
            this.wsInstance = null
        }
        const webSocketUrl = this.chatParams.webSocketUrl
        this.wsInstance = new WebSocket(webSocketUrl);
        this.wsInstance.onopen = this.onOpen
        this.wsInstance.onmessage = this.onMessage
        this.wsInstance.onerror = this.onError
        this.wsInstance.onclose = this.onClose
    }

    openHeartbeat = () => {
        // 保证只有一个实例只有一个心跳存在
        this.closeHeartbeat()
        this.heartbeatInterval = setInterval(this.heartbeatDetection, this.heartbeatFrequency)
    }

    closeHeartbeat = () => {
        clearInterval(this.heartbeatInterval)
        this.heartbeatInterval = null
    }

    heartbeatDetection = () => {
        // readonly CLOSED: 3; // 已经关闭
        // readonly CLOSING: number; // 正在关闭
        // readonly CONNECTING: number; // 正在连接
        // readonly OPEN: 1; // 已经打开
        if (this.wsInstance) {
            if (this.wsInstance.readyState === this.wsInstance.OPEN) {
                this.wsInstance.send('pong')
            } else if (this.wsInstance.readyState === this.wsInstance.CLOSED) {
                // 没有手动关闭，手动关闭会清除定时器
                this.reconnectionWebSocket()
            }
        }
    }

    registerInfo = (params) => {
        const data = { ...params, type: 7, } // 注册的消息类型是7
        this.wsInstance.send(JSON.stringify(data))
    }

    sendMessage = (params) => {

        if (this.wsInstance) {
            if (this.wsInstance.readyState === this.wsInstance.OPEN) {
                this.wsInstance.send(JSON.stringify(params))
            }
        }
    }

    onOpen = (event) => {
        // 打开后先发送注册的消息
        const register = { token: this.chatParams.token }
        this.registerInfo(register)
        // 建立心跳
        this.openHeartbeat()
    }

    onMessage = (event) => {

        const jsonData = JSON.parse(event.data)
        this.chatParams.onChatMessage && this.chatParams.onChatMessage(jsonData)
    }

    onError = (event) => {

    }

    onClose = (event) => {

    }

}

export default ChatManager