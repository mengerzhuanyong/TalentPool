/**
 * 人才库 - 人才等级
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react'
import {ImageBackground, StyleSheet, Text} from 'react-native'

export default class TalentLevel extends React.Component {

    static defaultProps = {
        level: 1,
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
    }

    render() {
        let {style, level} = this.props;
        return (
            <ImageBackground
                style={[styles.container, style]}
                source={Images.img_bg_talent}
                resizeMode={'contain'}
            >
                <Text style={styles.level}>{level}</Text>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: 60,
        height: 30,
    },
    level: {
        lineHeight: 30,
        paddingLeft: 8,
        color: '#D3942E',
    }
});