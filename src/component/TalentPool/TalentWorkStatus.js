/**
 * 人才库 - 工作状态
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react'
import {StyleSheet, Text, View,} from 'react-native'

export default class TalentWorkStatus extends React.Component {

    static defaultProps = {
        status: '候选中'
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
    }

    render() {
        let {style, status} = this.props;
        return (
            <View style={[styles.container, style]}>
                <Text style={styles.workStatus}>{status}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    workStatus: {
        fontSize: 13,
        minWidth: 40,
        color: '#4a90e2',
        borderRadius: 4,
        overflow: 'hidden',
        paddingVertical: 5,
        textAlign: 'center',
        paddingHorizontal: 8,
        backgroundColor: '#f3f3f3',
    }
});