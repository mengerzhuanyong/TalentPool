/**
 * 人才库 - 人才列表
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react'
import {StyleSheet, Text, TextInput, View,} from 'react-native'

import SpinnerLoading from "../Spinner/SpinnerLoading"
import ListView from '../../component/List/ListView'
import Button from "../Touchable/Button"
import TalentPoolItem from "../Item/TalentPoolItem"

export default class TalentPoolTabList extends React.Component {

    static defaultProps = {
        item: {},
    };

    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            dataSource: [],
        };
        this.page = 1;
        this.limit = 10;
    }

    onPressToNavigate = (pageTitle, component, params = {}) => {
        RouterHelper.navigate(pageTitle, component, {
            onCallBack: () => this._onRefresh(),
            ...params,
        })
    };


    componentDidMount() {
        this.requestListDataSources();
    }

    componentWillReceiveProps(nextProps) {}

    _captureRef = (v) => {
        this._listRef = v
    };

    _keyExtractor = (item, index) => {
        return `${index}`
    };

    requestListDataSources = async () => {
        let {dataSource} = this.state;
        let {item} = this.props;
        let url = item.api;
        let data = {
            token: '4faf64f1f52138b5ca8f4f2dd168698b',
            page: this.page,
            limit: this.limit,
        };
        let result = await Services.post(url, data);
        let dataSourceTemp = dataSource.slice();
        let dataList = [];
        if (result.code === StatusCode.SUCCESS_CODE) {
            dataList = result.data.data;
            if (parseInt(data.page) === 1) {
                dataSourceTemp = dataList;
            } else {
                if (dataList.length !== 0) {
                    dataSourceTemp = dataSourceTemp.concat(dataList);
                }
            }
        }
        this.setState({
            dataSource: dataSourceTemp,
            loading: false
        });
        this._onStopLoading(dataList.length < this.limit);
    };

    _onStopLoading = (status) => {
        this.setState({ready: true});
        this._listRef && this._listRef.stopRefresh();
        this._listRef && this._listRef.stopEndReached({allLoad: status});
    };

    _onRefresh = () => {
        this.page = 1;
        this.requestListDataSources();
    };

    _onEndReached = () => {
        this.page++;
        this.requestListDataSources();
    };

    _renderHeader = () => {
        return null
    };

    _renderItem = ({item, index}) => {
        return (
            <TalentPoolItem
                item={item}
            />
        );
    };

    render() {
        let {ready, dataSource} = this.state;
        let {style,} = this.props;
        return (
            <View style={[styles.container, style]}>
                {ready ?
                    <ListView
                        data={dataSource}
                        exData={this.state}
                        ref={this._captureRef}
                        style={styles.listContent}
                        onRefresh={this._onRefresh}
                        renderItem={this._renderItem}
                        keyExtractor={this._keyExtractor}
                        onEndReached={this._onEndReached}
                        ListHeaderComponent={this._renderHeader}
                    />
                    :
                    <SpinnerLoading
                        loading={!ready}
                        style={styles.loadingStyle}
                    />
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    listContent: {
        flex: 1,
    },
    loadingStyle: {
        flex: 1,
        top: 0,
    },
});