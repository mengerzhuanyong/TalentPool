'use strict';
import React from 'react';
import { View, Text, StyleSheet, ViewPropTypes } from 'react-native';
import LottieView from 'lottie-react-native';
import PropTypes from 'prop-types'

export default class SpinnerLoading extends React.PureComponent {

    static propTypes = {
        ...LottieView.propTypes,
        style: ViewPropTypes.style,
        loading: PropTypes.bool
    };

    static defaultProps = {
        ...LottieView.defaultProps,
        source: Images.json_loading,
        loading: false
    };

    constructor(props) {
        super(props)

    }

    _captureRef = (node) => {
        if (node) {
            this.lotteryRef = node
        }
    }

    render() {
        const { style, loading, source } = this.props
        if (!loading) {
            return null
        }
        return (
            <View style={[styles.container, style]}>
                <LottieView
                    ref={this._captureRef}
                    style={styles.lottery}
                    resizeMode={'cover'}
                    loop={true}
                    autoSize={false}
                    autoPlay={true}
                    source={source}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: Theme.navBarHeight + Theme.statusBarHeight,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    lottery: {
        marginTop: -20,
        width: 210,
        height: 210,
        // backgroundColor: 'red',
    },
});