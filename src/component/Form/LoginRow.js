'use strict';
import React from 'react';
import { View, Text, StyleSheet, TextInput, ViewPropTypes } from 'react-native';
import ImageView from '../Image/ImageView';
import PropTypes from 'prop-types'
import VerificationCode from './VerificationCode';

export default class LoginRow extends React.PureComponent {

    static propTypes = {
        ...TextInput.propTypes,
        type: PropTypes.oneOf(['default', 'verification']),
        style: ViewPropTypes.style,
        inputStyle: ViewPropTypes.style,
        icon: PropTypes.number,
        iconStyle: ViewPropTypes.style,
        open: PropTypes.bool,
        onStopInterval: PropTypes.func,
        onPressVerification: PropTypes.func,
    };

    static defaultProps = {
        ...TextInput.defaultProps,
        type: 'default'
    };

    render() {
        const {
            type,
            inputStyle,
            style,
            icon,
            iconStyle,
            open,
            onStopInterval,
            onPressVerification,
            ...others } = this.props
        return (
            <View style={[styles.inputContainer, style]}>
                <ImageView
                    style={[styles.inputIcon, iconStyle]}
                    source={icon}
                />
                <TextInput
                    style={[styles.input, inputStyle]}
                    {...others}
                />
                {type === 'verification' ? (
                    <VerificationCode
                        open={open}
                        onStopInterval={onStopInterval}
                        onPress={onPressVerification}
                    />
                ) : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#dbdbdb',
        marginHorizontal: 30,
        marginTop: 10,
    },
    inputIcon: {
        width: 17,
        height: 20
    },
    input: {
        flex: 1,
        padding: 0,
        fontSize: 14,
        marginLeft: 10,
    },
});