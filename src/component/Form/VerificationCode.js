'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types'
import Button from '../Touchable/Button';

export default class VerificationCode extends React.PureComponent {

    static propTypes = {
        interval: PropTypes.number,
        open: PropTypes.bool,
        onStopInterval: PropTypes.func,
    }

    static defaultProps = {
        interval: 60,
        open: false,
    }

    constructor(props) {
        super(props)
        const { interval } = props
        this.state = { intervalNumber: interval }
    }

    componentWillUnmount() {
        // 必须调用
        this.stopInterval()
    }

    componentWillReceiveProps(nextPorps) {
        const oldOpen = this.props.open
        if (nextPorps.open != oldOpen && nextPorps.open) {
            this.startInterval()
        }
    }

    _onPress = () => {
        const { onPress } = this.props
        onPress && onPress()
    }

    startInterval = () => {
        this.stopInterval()
        const { interval, onStopInterval } = this.props
        this.interval = setInterval(() => {
            const { intervalNumber } = this.state
            if (intervalNumber === 0) {
                this.stopInterval()
                this.setState({ intervalNumber: interval })
                onStopInterval && onStopInterval()
            } else {
                this.setState({ intervalNumber: intervalNumber - 1 })
            }
        }, 1000)
    }

    stopInterval = () => {
        clearInterval(this.interval)
    }

    render() {
        const { open } = this.props
        const { intervalNumber } = this.state
        return (
            <View style={styles.container}>
                {open ?
                    <View style={styles.countdownTextContainer}>
                        <Text style={styles.countdownText}>{`${intervalNumber}s`}</Text>
                    </View>
                    :
                    <Button style={styles.codeContainer} onPress={this._onPress}>
                        <Text style={styles.codeText}>获取验证码</Text>
                    </Button>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {

    },
    codeText: {
        color: '#43a4fe',
        fontSize: 13,
    },
    codeContainer: {
        borderRadius: 0,
        backgroundColor: 'transparent',
        paddingHorizontal: 13,
        paddingVertical: 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderLeftWidth: 1,
        borderLeftColor: '#dbdbdb',
        marginRight: -10,
    },
    countdownText: {
        color: '#333',
        fontSize: 14,
    },
    countdownTextContainer: {
        width: ScaleSize(130),
        borderRadius: 0,
        backgroundColor: 'transparent',
        paddingHorizontal: 13,
        paddingVertical: 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderLeftWidth: 1,
        borderLeftColor: '#dbdbdb',
        marginRight: -10,
    }
});
