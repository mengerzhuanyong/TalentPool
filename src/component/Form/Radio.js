'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types'
import Button from '../Touchable/Button';

export default class Radio extends React.PureComponent {

    static propTypes = {
        checked: PropTypes.bool,
        title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    };

    static defaultProps = {
        title: '',
        checked: false,
    };

    render() {
        const { style, title, checked, onPress } = this.props
        const icon = checked ? Images.icon_radio_select : Images.icon_radio_normal
        return (
            <Button
                style={[styles.button, style]}
                title={title}
                icon={icon}
                titleStyle={styles.titleStyle}
                iconStyle={styles.iconStyle}
                onPress={onPress}
            />
        );
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'transparent',
        paddingVertical: 0,
        borderRadius: 0,
    },
    titleStyle: {
        fontSize: 14,
        color: "#666666",
        marginLeft: 5,
    },
    iconStyle: {
        width: 15,
        height: 15,
    },
});

