'use strict';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types'
import Button from '../Touchable/Button';

export default class Checkbox extends React.PureComponent {

    static propTypes = {
        checked: PropTypes.bool,
        defaultChecked: PropTypes.bool,
        title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    };

    static defaultProps = {
        title: '',
        checked: undefined,
        defaultChecked: false,
    };

    constructor(props) {
        super(props)
        const { defaultChecked } = props
        this.state = { checked: defaultChecked }
    }

    _onPress = () => {
        const { onPress, checked } = this.props
        let newChecked = checked
        if (checked === undefined) {
            newChecked = !this.state.checked
            this.setState({ checked: !this.state.checked })
        }
        onPress && onPress(newChecked)
    }

    render() {
        const { style, title, checked } = this.props
        let newChecked = checked === undefined ? this.state.checked : checked
        const icon = newChecked ? Images.icon_check_select : Images.icon_check_narmal
        return (
            <Button
                style={[styles.button, style]}
                title={title}
                icon={icon}
                titleStyle={styles.titleStyle}
                iconStyle={styles.iconStyle}
                onPress={this._onPress}
                actionInterval={0}
            />
        );
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'transparent',
        paddingVertical: 0,
        borderRadius: 0,
    },
    titleStyle: {
        fontSize: 14,
        color: "#666666",
        marginLeft: 10,
    },
    iconStyle: {
        width: 15,
        height: 15,
    },
});

