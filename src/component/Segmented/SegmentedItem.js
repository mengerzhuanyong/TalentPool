// SegmentedItem.js

'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ViewPropTypes, StyleSheet } from 'react-native';
import { Badge } from 'teaset';

export default class SegmentedItem extends React.PureComponent {

    static propTypes = {
        ...ViewPropTypes,
        title: PropTypes.oneOfType([PropTypes.element, PropTypes.string, PropTypes.number]),
        titleStyle: Text.propTypes.style,
        activeTabStyle: ViewPropTypes.style,
        activeTitleStyle: Text.propTypes.style,
        active: PropTypes.bool,
        // badge: PropTypes.oneOfType([PropTypes.element, PropTypes.string, PropTypes.number]),
        // onAddWidth: PropTypes.func, //(width)
    };

    static defaultProps = {
        ...View.defaultProps,
        active: false,
    };

    constructor(props) {
        super(props);
        this.state = {
            badgeWidth: 0,
        };
    }

    render() {
        const { title, titleStyle, tabStyle, active, activeTitleStyle, activeTabStyle, onLayout } = this.props;
        let titleItem = null
        if (!React.isValidElement(title) && (title || title === '' || title === 0)) {
            const textStyleDefault = active ? styles.activeTitleStyle : styles.textStyle
            const textStyleProps = active ? activeTitleStyle : titleStyle
            titleItem = (
                <Text
                    key='title'
                    style={[textStyleDefault, textStyleProps]}
                    numberOfLines={1}>
                    {title}
                </Text>
            )
        }
        const tabStyleDefault = active ? styles.activeTabStyle : styles.tabStyle
        const tabStyleProps = active ? activeTabStyle : tabStyle
        return (
            <View style={[tabStyleDefault, tabStyleProps]} onLayout={onLayout}>
                {titleItem}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    tabStyle: {
        paddingTop: Theme.sbBtnPaddingTop,
        paddingBottom: Theme.sbBtnPaddingBottom,
        paddingLeft: Theme.sbBtnPaddingLeft,
        paddingRight: Theme.sbBtnPaddingRight,
        overflow: 'visible',
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeTabStyle: {
        paddingTop: Theme.sbBtnPaddingTop,
        paddingBottom: Theme.sbBtnPaddingBottom,
        paddingLeft: Theme.sbBtnPaddingLeft,
        paddingRight: Theme.sbBtnPaddingRight,
        overflow: 'visible',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'blue',
    },
    textStyle: {
        color: Theme.sbBtnTitleColor,
        fontSize: Theme.sbBtnTextFontSize,
    },
    activeTitleStyle: {
        color: Theme.sbBtnActiveTitleColor,
        fontSize: Theme.sbBtnActiveTextFontSize,
    }
});


