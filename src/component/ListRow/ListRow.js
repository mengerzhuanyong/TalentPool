'use strict';
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ViewPropTypes } from 'react-native';
import ImageView from '../Image/ImageView'
import PropTypes from 'prop-types'
import Button from '../Touchable/Button';
import Theme from '../../config/themes/Theme';

export default class ListRow extends React.PureComponent {

    static propTypes = {
        style: ViewPropTypes.style,
        contentStyle: ViewPropTypes.style,
        title: PropTypes.oneOfType([PropTypes.element, PropTypes.string, PropTypes.func]),
        titleStyle: Text.propTypes.style,
        icon: PropTypes.oneOfType([PropTypes.element, PropTypes.number, PropTypes.func]),
        iconStyle: ImageView.propTypes.style,
        detail: PropTypes.oneOfType([PropTypes.element, PropTypes.string, PropTypes.func]),
        detailStyle: Text.propTypes.style,
        accessory: PropTypes.oneOfType([PropTypes.oneOf(['none', 'indicator']), PropTypes.element, PropTypes.func]),
        bottomSeparator: PropTypes.oneOfType([PropTypes.element, PropTypes.func, PropTypes.oneOf(['none', 'full', 'indent'])]),
        bottomSeparatorStyle: ViewPropTypes.style
    }

    static defaultProps = {
        bottomSeparator: 'full',
        accessory: 'indicator'
    };

    constructor(props) {
        super(props)

    }

    _onPress = (event) => {
        const { onPress } = this.props
        onPress && onPress(event)
    }

    renderIcon = () => {
        const { icon, iconStyle } = this.props
        if (React.isValidElement(icon)) {
            return icon
        } else if (typeof icon === 'function') {
            return icon()
        } else if (typeof icon === 'number') {
            return (
                <ImageView
                    style={[styles.rowImage, iconStyle]}
                    source={icon}
                />
            )
        }
        return null
    }

    renderTitle = () => {
        const { title, titleStyle } = this.props
        if (React.isValidElement(title)) {
            return title
        } else if (typeof title === 'function') {
            return title()
        } else if (typeof title === 'string') {
            return <Text style={[Theme.FC15333, titleStyle]}>{title}</Text>
        }
        return null
    }

    renderDetail = () => {
        const { detail, detailStyle } = this.props
        if (React.isValidElement(detail)) {
            return detail
        } else if (typeof detail === 'function') {
            return detail()
        } else if (typeof detail === 'string') {
            return <Text style={[Theme.FC14666, Theme.MR10, detailStyle]}>{detail}</Text>
        }
        return null
    }

    renderAccessory = () => {
        const { accessory } = this.props
        if (accessory === 'none') {
            return null
        } else if (accessory === 'indicator') {
            return (
                <ImageView
                    style={styles.accessory}
                    source={Images.icon_arrow_right}
                    resizeMode={'contain'}
                />
            )
        } else if (React.isValidElement(accessory)) {
            return accessory
        } else if (typeof accessory === 'function') {
            return accessory()
        }
        return null
    }

    renderBottomSeparator = () => {
        const { bottomSeparator, bottomSeparatorStyle } = this.props
        if (React.isValidElement(bottomSeparator)) {
            return bottomSeparator
        } else if (typeof bottomSeparator === 'function') {
            return bottomSeparator()
        } else if (typeof bottomSeparator === 'string') {
            if (bottomSeparator === 'none') {
                return null
            } else if (bottomSeparator === 'full') {
                return <View style={[styles.sepFull, bottomSeparatorStyle]} />
            } else if (bottomSeparator === 'indent') {
                return <View style={[styles.sepIndent, bottomSeparatorStyle]} />
            }
        }
        return null
    }

    render() {
        const { style, contentStyle } = this.props
        return (
            <Button style={[styles.container, style]} onPress={this._onPress}>
                <View style={[styles.contentContainer, contentStyle]}>
                    <View style={Theme.RCA}>
                        {this.renderIcon()}
                        {this.renderTitle()}
                    </View>
                    <View style={[Theme.RCC, styles.detailContainer]}>
                        {this.renderDetail()}
                        {this.renderAccessory()}
                    </View>
                </View>
                {this.renderBottomSeparator()}
            </Button>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        paddingVertical: 0,
        paddingHorizontal: 0,
        backgroundColor: '#fff',
        alignSelf: 'stretch'
    },
    contentContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
        paddingVertical: 15,
        paddingHorizontal: 12,
        justifyContent: 'space-between',
        backgroundColor: 'transparent',
    },
    detailContainer: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'flex-end',
    },
    rowImage: {
        width: 16,
        height: 16,
        marginRight: 10,
    },
    sepFull: {
        alignSelf: 'stretch',
        height: 1,
        backgroundColor: '#f3f3f3',
    },
    sepIndent: {
        alignSelf: 'stretch',
        marginLeft: 15,
        height: 1,
        backgroundColor: '#f3f3f3',
    },
    accessory: {
        width: 12,
        height: 12
    }
});