'use strict'
import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import PropTypes from 'prop-types'
import NetworkError from '../Error/NetworkError'
import SpinnerLoading from '../Spinner/SpinnerLoading';
import NetInfo from "@react-native-community/netinfo";
import { observer, inject } from 'mobx-react';

/**
    // 每个页面最外层的View，
    // 不建议在拆分后的组件上使用这个组件，因为耦合度很高，容易造成不必要的Bug。
    // 每个拆分后的组件处理自己的事情就够了，这个组件用来处理页面最外层的事情
*/
@inject('appStore')
@observer
export default class Container extends React.Component {

    static propTypes = {
        fitIPhoneX: PropTypes.bool,
        fitIPhoneXType: PropTypes.oneOf(['padding', 'margin']),

        loading: PropTypes.bool,

        showNetworkError: PropTypes.bool,
        onNetworkReload: PropTypes.func,// 网络出现错误后重新刷新
    };

    static defaultProps = {
        fitIPhoneX: true,
        fitIPhoneXType: 'padding',
        showNetworkError: true,
        loading: false
    };

    constructor(props) {
        super(props)

    };

    componentDidMount() {

    };

    componentWillUnmount() {

    };

    _onNetworkReload = async () => {
        const { appStore, onNetworkReload } = this.props
        NetInfo.fetch().then(state => {
            appStore.changeNetworkState(state.type)
        });
        onNetworkReload && onNetworkReload()
    };

    buildProps = () => {
        const { fitIPhoneX, fitIPhoneXType } = this.props
        let iphoneXStyle;
        if (fitIPhoneX) {
            iphoneXStyle = fitIPhoneXType === 'padding' ? {
                paddingBottom: Theme.isIPhoneX ? Theme.fitIPhoneXBottom : 0
            } : { marginBottom: Theme.isIPhoneX ? Theme.fitIPhoneXBottom : 0 }
        } else {
            iphoneXStyle = null
        }
        return { iphoneXStyle }
    };

    renderLoading = () => {
        const { loading } = this.props
        return <SpinnerLoading loading={loading} />
    }

    renderNetworkError = () => {
        const { appStore, showNetworkError } = this.props
        if (showNetworkError && appStore.appModel.networkState === 'none') {
            return <NetworkError onNetworkReload={this._onNetworkReload} />
        }
        return null
    }

    render() {
        const { children, style } = this.props
        const { iphoneXStyle } = this.buildProps()
        return (
            <View style={[styles.container, iphoneXStyle, style]}>
                {children}
                {this.renderLoading()}
                {this.renderNetworkError()}
            </View>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Theme.pageBackgroundColor,
    },
});

