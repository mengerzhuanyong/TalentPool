'use strict';
import React from 'react';
import { View, Text, StyleSheet, FlatList, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types'

export default class WrapContainer extends React.PureComponent {

    static propTypes = {
        style: ViewPropTypes.style,  // 最外层的样式
        numColumns: PropTypes.number.isRequired,
        itemColumnMargin: PropTypes.number, // 列间距
        itemRowMargin: PropTypes.number, // 行间距
        extraData: PropTypes.any
    }

    static defaultProps = {
        itemColumnMargin: 0,
        itemRowMargin: 0,
    }

    constructor(props) {
        super(props);
        this.state = { containerWidth: 0 }
    }

    _onLayout = (event) => {
        const layout = event.nativeEvent.layout
        const { containerWidth } = this.state
        if (Math.round(containerWidth) != Math.round(layout.width)) {
            this.setState({ containerWidth: layout.width })
        }
    }

    render() {
        const { containerWidth } = this.state
        const { style, numColumns, itemColumnMargin, itemRowMargin, children } = this.props
        const wrapItemWidth = Math.floor((containerWidth - itemColumnMargin * (numColumns - 1)) / numColumns)
        return (
            <View style={[styles.wrapContainer, style]} onLayout={this._onLayout}>
                {children.map((element, index) => {
                    const marginLeft = index % numColumns === 0 ? 0 : itemColumnMargin
                    const marginTop = index < numColumns ? 0 : itemRowMargin
                    return React.cloneElement(element, {
                        style: [{
                            width: wrapItemWidth,
                            marginLeft,
                            marginTop
                        }, element.props.style]
                    })
                })}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    wrapContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },

});
