'use strict';
import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ViewPropTypes, ImageBackground } from 'react-native';
import PropTypes from 'prop-types';
import ImageView from '../Image/ImageView';

export default class Button extends React.PureComponent {

    static propTypes = {
        ...TouchableOpacity.propTypes,
        icon: PropTypes.oneOfType([PropTypes.number, PropTypes.shape({ uri: PropTypes.string })]),
        iconStyle: ImageView.propTypes.style,
        iconResizeMode: PropTypes.oneOf(['contain', 'cover', 'stretch', 'center']),
        title: PropTypes.string,
        titleStyle: Text.propTypes.style,
        backgroundImage: PropTypes.number,
        actionInterval: PropTypes.number, // 多次点击之间的延迟
    };

    static defaultProps = {
        ...TouchableOpacity.defaultProps,
        iconResizeMode: 'contain',
        activeOpacity: 0.8,
        actionInterval: 300
    };

    constructor(props) {
        super(props)
        this.lastActionTime = 0
    }

    _onPress = (event) => {
        const { onPress, actionInterval } = this.props
        const nowTime = Moment().format('x')
        if ((nowTime - this.lastActionTime) <= actionInterval) {
            // console.warn('间隔时间内重复点击了');
            return
        }
        this.lastActionTime = nowTime;
        onPress && onPress(event)
    }

    renderImageBackground = () => {
        const { backgroundImage } = this.props
        if (backgroundImage) {
            return (
                <ImageBackground style={styles.imageBackground} source={backgroundImage} />
            )
        }
        return null
    }

    renderTitle = () => {
        const { title, titleStyle } = this.props
        if (title) {
            return (
                <Text style={[styles.titleStyle, titleStyle]}>{title}</Text>
            )
        }
        return null
    }

    renderIcon = () => {
        const { icon, iconStyle, iconResizeMode } = this.props
        if (icon) {
            return (
                <ImageView resizeMode={iconResizeMode} style={[styles.imageStyle, iconStyle]} source={icon} />
            )
        }
        return null
    }

    render() {
        const {
            style,
            backgroundImage,
            onPress,
            children,
            icon,
            title,
            ...others
        } = this.props
        let originalStyle = null
        if (title) {
            originalStyle = styles.defaultContainer
        } else {
            originalStyle = styles.otherContainer
        }
        return (
            <TouchableOpacity style={[originalStyle, style]} onPress={this._onPress} {...others} >
                {this.renderImageBackground()}
                {this.renderIcon()}
                {this.renderTitle()}
                {children}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    defaultContainer: {
        paddingVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        flexDirection: 'row',
        overflow: 'hidden',
        backgroundColor: Theme.overallColor,
    },
    otherContainer: {

    },
    titleStyle: {
        fontSize: 13,
        color: "#fff"
    },
    imageStyle: {
        width: 25,
        height: 25,
    },
    imageBackground: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    }
});

