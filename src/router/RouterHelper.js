'use strict';
import { StackActions, NavigationActions } from 'react-navigation'

export default class RouterHelper {

    /**
     * @导航实例
     */
    static navigation = null
    /**
     * @上次执行to方法的时间
     */
    static lastActionTime = 0;
    /**
     * @重复点击判断间隔时间
     */
    static interval = 600;
    /**
     * @列表保存路由栈信息
     */
    static routerStacks = [];
    /**
     * @未登录忽略名单 跳转时跳到登录界面
     */
    static loginIgnoreRoute = []

    static checkActionState() {
        if (!this.navigation) {
            console.error('请先初始化路由');
            return false
        }
        const nowTime = new Date().getTime();
        if ((nowTime - this.lastActionTime) <= this.interval) {
            // console.warn('间隔时间内重复点击了');
            return false
        }
        this.lastActionTime = nowTime;
        return true
    }

    static initRouter(navition) {
        this.setRouter(navition)
    }

    static setRouter(navition) {
        this.setNavigation(navition)
        this.setRouterStacks(navition.state.routes)
    }

    static setNavigation(navigation) {
        this.navigation = navigation
    }

    static setRouterStacks(routerStacks = []) {
        this.routerStacks = routerStacks
    }

    // 最好使用这个
    static navigate(pageTitle, routeName, params = {}) {
        params = {
            pageTitle,
            ...params,
        };
        if (!this.checkActionState()) {
            return;
        }
        // 未登录判断的规则
        if (!global.token && this.loginIgnoreRoute.length > 0) {
            const index = this.loginIgnoreRoute.findIndex((item) => item === routeName);
            Log.print('navigate', routeName)
            if (index !== -1) {
                routeName = 'Login'
                params = { ...params, screenInterpolator: 'modal' }
            }
        }
        this.navigation.navigate(routeName, params);
    }

    static push(pageTitle, routeName, params = {}) {
        params = {
            pageTitle,
            ...params,
        };
        if (!this.checkActionState()) {
            return;
        }
        // 未登录判断的规则
        if (!global.token && this.loginIgnoreRoute.length > 0) {
            const index = this.loginIgnoreRoute.findIndex((item) => item === routeName);
            Log.print('navigate', routeName)
            if (index !== -1) {
                routeName = 'Login'
                params = { ...params, screenInterpolator: 'modal' }
            }
        }
        this.navigation.push(routeName, params);
    }

    static goBack(routeName) {
        if (!this.checkActionState()) {
            return;
        }
        if (routeName) {
            const index = this.routerStacks.findIndex((item) => routeName === item.routeName)
            if (index >= 0) {
                const navTarget = this.routerStacks[index + 1]
                this.navigation.goBack(navTarget.key);
            }
        } else {
            this.navigation.goBack(null)
        }
    }

    static dismiss() {
        if (!this.checkActionState()) {
            return;
        }
        this.navigation.dismiss()
    }

    static pop(number, params = {}) {
        if (!this.checkActionState()) {
            return;
        }
        this.navigation.pop(number, params)
    }

    static popToTop(params = {}) {
        if (!this.checkActionState()) {
            return;
        }
        this.navigation.popToTop(params)
    }

    static replace(pageTitle, routeName, params = {}) {
        params = {
            pageTitle,
            ...params,
        };
        if (!this.checkActionState()) {
            return;
        }
        const replaceAction = StackActions.replace({
            routeName: routeName,
            params: params,
        });
        this.navigation.dispatch(replaceAction)
    }

    static reset(pageTitle, routeName, params = {}) {
        params = {
            pageTitle,
            ...params,
        };
        if (!this.checkActionState()) {
            return;
        }
        this.navigation.reset([NavigationActions.navigate({ routeName, params })], 0)
    }
}


