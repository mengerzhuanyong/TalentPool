
'use strict'
import React from 'react'
import { Image } from 'react-native';
import hoistNonReactStatics from 'hoist-non-react-statics'
import { StackViewTransitionConfigs } from 'react-navigation'

export function transitionConfig(transitionProps, prevTransitionProps) {
    const params = transitionProps && transitionProps.scene.route.params
    const prevParams = prevTransitionProps && prevTransitionProps.scene.route.params
    let isModal = false
    if (prevTransitionProps && transitionProps && prevTransitionProps.index > transitionProps.index) {
        if (prevParams && prevParams.screenInterpolator === 'modal') {
            isModal = true
        }
    } else if (prevTransitionProps && transitionProps && prevTransitionProps.index < transitionProps.index) {
        if (params && params.screenInterpolator === 'modal') {
            isModal = true
        }
    }
    return isModal ? StackViewTransitionConfigs.ModalSlideFromBottomIOS : StackViewTransitionConfigs.SlideFromRightIOS;
}

export function tabOptions(params) {
    return {
        title: params.title,
        tabBarIcon: ({ focused, tintColor }) => (
            <Image
                resizeMode="contain"
                style={{
                    width: params.iconWidth ? params.iconWidth : ScaleSize(38),
                    height: params.iconHeight ? params.iconHeight : ScaleSize(38)
                }}
                source={!focused ? params.normalIcon : params.selectedIcon}
            />
        )
    }
};

export function configRouter(routeConfig) {
    for (let name in routeConfig) {
        let Component = routeConfig[name].screen;
        routeConfig[name].screen = createNavigationContainer(Component)
    }
    return routeConfig
}

// 高阶组件
export function createNavigationContainer(OldComponent) {

    class NewComponent extends React.PureComponent {

        static displayName = `todoNavigationContainer(${OldComponent.displayName ||
            OldComponent.name})`;

        constructor(props) {
            super(props)
            // this.subscriptions = []
        }

        componentDidMount() {

        };

        componentWillUnmount() {

        };

        _captureRef = (node) => {
            if (node) {
                // bind native component's methods
                for (let methodName in node) {
                    const method = node[methodName];
                    if (!methodName.startsWith('_') && // private methods
                        !methodName.startsWith('component') && // lifecycle methods
                        typeof method === 'function' &&
                        this[methodName] === undefined) {
                        this[methodName] = method;
                    }
                }
                this._oldComponentRef = node
            }
        };

        render() {
            return (
                <OldComponent
                    ref={this._captureRef}
                    {...this.props}
                />
            )
        }
    }

    return hoistNonReactStatics(NewComponent, OldComponent)
};
