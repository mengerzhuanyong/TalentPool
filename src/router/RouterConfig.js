/**
 * 人科云 - 路由设置
 * http://menger.me
 * @大梦
 */

'use strict';

import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation'
import { tabOptions, transitionConfig } from './RouterTool'
import TabBottomBar from '../component/Navigation/TabBottomBar'

// WebPage
import WebPage from '../page/webview/WebPage';
// Login
import Login from '../page/login/Login';
// Register
import Register from '../page/login/Register';
// ResetPassword
import ResetPassword from '../page/login/ResetPassword';
// ModifyPassword
import ModifyPassword from '../page/login/ModifyPassword';
// Practice
import Practice from '../page/practive/Practice'
// 人才库
import TalentPool from '../page/talentPool/TalentPool'
// 人才库 - 手动添加人才
import TalentAddManual from '../page/publish/TalentAddManual'

// 工作台
import WorkBench from '../page/workBench/WorkBench'



const TabNavigator = createBottomTabNavigator({
    TalentPool: {
        screen: TalentPool,
        navigationOptions: tabOptions({
            title: '人才库',
            normalIcon: Images.icon_tabbar_talent_pool,
            selectedIcon: Images.icon_tabbar_talent_pool_cur
        }),
    },
    WorkBench: {
        screen: WorkBench,
        navigationOptions: tabOptions({
            title: '工作台',
            normalIcon: Images.icon_tabbar_work_bench,
            selectedIcon: Images.icon_tabbar_work_bench_cur
        }),
    },

}, {
        initialRouteName: 'TalentPool',
        // initialRouteName: 'WorkBench',
        tabBarOptions: {
            showIcon: true,
            indicatorStyle: { height: 0 },
            inactiveTintColor: '#999',
            activeTintColor: "#0085da",
            style: {
                backgroundColor: "#fff"
            },
            tabStyle: {
                margin: 2,
            },
            keyboardHidesTabBar: true,
        },
        lazy: false, //懒加载
        swipeEnabled: false,
        animationEnabled: false, //关闭安卓底栏动画
        tabBarPosition: "bottom",
        tabBarComponent: TabBottomBar
    });


const StackNavigator = createStackNavigator({
    Tab: { screen: TabNavigator },
    WebPage: { screen: WebPage },
    Login: { screen: Login },
    Register: { screen: Register },
    ResetPassword: { screen: ResetPassword },
    ModifyPassword: { screen: ModifyPassword },
    TalentAddManual: { screen: TalentAddManual },
}, {
        initialRouteName: 'Tab',
        initialRouteParams: {},
        defaultNavigationOptions: {
            header: null,
            gesturesEnabled: !__ANDROID__
        },
        cardStyle: {
            backgroundColor: Theme.pageBackgroundColor,
        },
        cardShadowEnabled: false,
        cardOverlayEnabled: true,
        transitionConfig: transitionConfig
    });

const AppNavigationContainer = createAppContainer(StackNavigator)

export { AppNavigationContainer }
