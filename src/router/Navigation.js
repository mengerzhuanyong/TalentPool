'use strict';
import React from 'react'
import { AppNavigationContainer } from './RouterConfig'
import RouterHelper from './RouterHelper';

/**
 * @高阶导航
 */
export default class Navigation extends React.PureComponent {

    constructor(props) {
        super(props)

    }

    _onNavigationStateChange = (prevState, newState, action) => {
        const navition = this.navigatorRef._navigation
        RouterHelper.setRouter(navition)
    }

    _captureRef = (navigator) => {
        if (navigator) {
            this.navigatorRef = navigator
            const navition = this.navigatorRef._navigation
            RouterHelper.initRouter(navition)
        } else {
            this.navigatorRef = null
        }
    }

    render() {
        return (
            <AppNavigationContainer
                ref={this._captureRef}
                onNavigationStateChange={this._onNavigationStateChange}
            />
        );
    }
}

