package com.todotalentpool;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.multidex.MultiDex;

import com.facebook.react.ReactApplication;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.reactlibrary.RNSyanImagePickerPackage;
import com.reactnativecommunity.viewpager.RNCViewPagerPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;

import cn.jiguang.analytics.android.api.JAnalyticsInterface;
import cn.jpush.reactnativejanalytics.JAnalyticsPackage;

import com.opensettings.OpenSettingsPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.puti.paylib.PayReactPackage;
import com.horcrux.svg.SvgPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.zmxv.RNSound.RNSoundPackage;
import com.rnim.rn.audio.ReactNativeAudioPackage;
import com.ocetnik.timer.BackgroundTimerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;


import com.imagepicker.ImagePickerPackage;
import com.rnfs.RNFSPackage;
import com.github.yamill.orientation.OrientationPackage;
import com.brentvatne.react.ReactVideoPackage;

import org.devio.rn.splashscreen.SplashScreenReactPackage;


import cn.jiguang.share.reactnative.JSharePackage;
import cn.jpush.reactnativejpush.JPushPackage;
import cn.jiguang.share.android.api.JShareInterface;

import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    // 不跟随系统字体大小进行变化
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.fontScale != 1)
            getResources();
        super.onConfigurationChanged(newConfig);
    }

    // 不跟随系统字体大小进行变化
    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        if (res.getConfiguration().fontScale != 1) {
            Configuration newConfig = new Configuration();
            newConfig.setToDefaults();
            res.updateConfiguration(newConfig, res.getDisplayMetrics());
        }
        return res;
    }

    // 是否关闭 Log，默认不关闭
    private static boolean SHUTDOWN_LOG = !BuildConfig.DEBUG;
    // 是否关闭 toast，默认不关闭
    private static boolean SHUTDOWN_TOAST = !BuildConfig.DEBUG;

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
                    new RNCViewPagerPackage(),
                    new AsyncStoragePackage(),
                    new NetInfoPackage(),
                    new OpenSettingsPackage(),
                    new RNFetchBlobPackage(),
                    new SvgPackage(),
                    new FastImageViewPackage(),
                    new RNGestureHandlerPackage(),
                    new LottiePackage(),
                    new RNSoundPackage(),
                    new ReactNativeAudioPackage(),
                    new BackgroundTimerPackage(),
                    new RNDeviceInfo(),
                    new PayReactPackage(),
                    new ImagePickerPackage(),
                    new RNSyanImagePickerPackage(),
                    new RNFSPackage(),
                    new OrientationPackage(),
                    new ReactVideoPackage(),
                    new SplashScreenReactPackage(),
                    new JSharePackage(SHUTDOWN_TOAST, SHUTDOWN_LOG),
                    new JPushPackage(SHUTDOWN_TOAST, SHUTDOWN_LOG),
                    new JAnalyticsPackage(SHUTDOWN_LOG, SHUTDOWN_TOAST));
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
        JShareInterface.setDebugMode(!SHUTDOWN_LOG);
        JShareInterface.init(this);
        JAnalyticsInterface.setDebugMode(!SHUTDOWN_LOG);
        JAnalyticsInterface.init(this);
    }
}
