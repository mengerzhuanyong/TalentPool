'use strict';
import './src/config/Global'
import React from 'react'
import { Provider } from 'mobx-react'
import stores from './src/mobx/index'
import Main from './src/page/Main'

class App extends React.PureComponent {

    render() {
        return (
            <Provider {...stores}>
                <Main />
            </Provider>
        );
    }
}

export default App;