package com.todo.emclient;

import android.widget.RelativeLayout;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.hyphenate.chat.EMClient;
import com.hyphenate.media.EMCallSurfaceView;
import com.superrtc.sdk.VideoView;

public class RCTHyphenateViewManager extends SimpleViewManager<EMCallSurfaceView> {

    public EMCallSurfaceView localSurfaceView = null;
    public EMCallSurfaceView remoteSurfaceView = null;
    public RelativeLayout surfaceLayout;

    @Override
    public String getName() {
        return "RCTHyphenateView";
    }

    @Override
    protected EMCallSurfaceView createViewInstance(ThemedReactContext reactContext) {
        EMCallSurfaceView callSurfaceView = new EMCallSurfaceView(reactContext);
        callSurfaceView.setScaleMode(VideoView.EMCallViewScaleMode.EMCallViewScaleModeAspectFill);
        return callSurfaceView;
    }

    @ReactProp(name = "showLocalView")
    public void setShowLocalView(final EMCallSurfaceView callSurfaceView, boolean showLocalVideo) {
        if (showLocalVideo){
            localSurfaceView = callSurfaceView;
        }else {
            remoteSurfaceView = callSurfaceView;
        }
        EMClient.getInstance().callManager().setSurfaceView(localSurfaceView, remoteSurfaceView);
    }
}
