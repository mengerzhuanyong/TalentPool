package com.todo.emclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera;
import android.media.AudioManager;
import android.util.Log;
import android.view.WindowManager;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.hyphenate.EMCallBack;
import com.hyphenate.EMChatRoomChangeListener;
import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMError;
import com.hyphenate.EMMessageListener;
import com.hyphenate.EMValueCallBack;
import com.hyphenate.chat.EMCallOptions;
import com.hyphenate.chat.EMCallStateChangeListener;
import com.hyphenate.chat.EMChatRoom;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMMessageBody;
import com.hyphenate.chat.EMOptions;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.chat.EMVoiceMessageBody;
import com.hyphenate.exceptions.EMNoActiveCallException;
import com.hyphenate.exceptions.EMServiceNotReadyException;
import com.hyphenate.exceptions.HyphenateException;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

public class HyphenateModule extends ReactContextBaseJavaModule implements LifecycleEventListener {

    private ReactApplicationContext context;

    private ConnectionListener connectListener = null;
    private CallStateListener  callStateListener = null;
    private CallReceiver callReceiverListener = null;
    private MessageListener messageListener = null;
    private ChatRoomChangeListener roomChangeListener = null;

    public HyphenateModule(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;
        context.addLifecycleEventListener(this);
    }

    @Override
    public void onHostResume() {

    }

    @Override
    public void onHostPause() {

    }

    @Override
    public void onHostDestroy() {
        if (messageListener != null) {
            EMClient.getInstance().chatManager().removeMessageListener(messageListener);
            messageListener = null;
        }
        if (connectListener != null){
            EMClient.getInstance().removeConnectionListener(connectListener);
            connectListener = null;
        }
        if (callStateListener != null) {
            EMClient.getInstance().callManager().removeCallStateChangeListener(callStateListener);
            callStateListener = null;
        }
        if (callReceiverListener != null) {
            context.getApplicationContext().unregisterReceiver(callReceiverListener);
            callReceiverListener = null;
        }
        if (roomChangeListener != null) {
            EMClient.getInstance().chatroomManager().removeChatRoomListener(roomChangeListener);
            roomChangeListener = null;
        }
    }

    @Override
    public String getName() {
        return "HyphenateModule";
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        return constants;
    }

    /**
     * 初始化环信
     */
    @ReactMethod
    public void init(ReadableMap map, Promise promise) {
        String appKey = map.getString("appKey");
        String apnsCertName = map.getString("apnsCertName");
        EMOptions options = new EMOptions();
        // 默认添加好友时，是不需要验证的，改成需要验证
        options.setAcceptInvitationAlways(false);
        // 是否自动将消息附件上传到环信服务器，默认为True是使用环信服务器上传下载，如果设为 false，需要开发者自己处理附件消息的上传和下载
        options.setAutoTransferMessageAttachments(true);
        // 是否自动下载附件类消息的缩略图等，默认为 true 这里和上边这个参数相关联
        options.setAutoDownloadThumbnail(true);
        //初始化
        EMClient.getInstance().init(context.getApplicationContext(), options);
        //在做打包混淆时，关闭debug模式，避免消耗不必要的资源
        EMClient.getInstance().setDebugMode(true);
        //返回给rn
        WritableMap writableMap = Arguments.createMap();
        writableMap.putString("code", "success");
        promise.resolve(writableMap);
    }


    /**
     * 登录环信
     */
    @ReactMethod
    public void login(ReadableMap map, final Promise promise) {
        String userName = map.getString("userName");
        String password = map.getString("password");
        final WritableMap writableMap = Arguments.createMap();
        EMClient.getInstance().login(userName, password, new EMCallBack() {//回调
            @Override
            public void onSuccess() {
                // 注册消息的监听
                messageListener = new MessageListener();
                EMClient.getInstance().chatManager().addMessageListener(messageListener);

                //注册呼入通话监听
                IntentFilter callFilter = new IntentFilter(EMClient.getInstance().callManager().getIncomingCallBroadcastAction());
                callReceiverListener = new CallReceiver();
                context.getApplicationContext().registerReceiver(callReceiverListener, callFilter);

                //注册通话状态监听
                callStateListener = new CallStateListener();
                EMClient.getInstance().callManager().addCallStateChangeListener(callStateListener);

                //注册一个监听连接状态的listener
                connectListener = new ConnectionListener();
                EMClient.getInstance().addConnectionListener(connectListener);

                // 注册聊天室监听
                roomChangeListener = new ChatRoomChangeListener();
                EMClient.getInstance().chatroomManager().addChatRoomChangeListener(roomChangeListener);


                writableMap.putString("code", "success");
                promise.resolve(writableMap);
            }

            @Override
            public void onProgress(int progress, String status) {

            }

            @Override
            public void onError(int code, String message) {
                Log.d("main", "登录聊天服务器失败！");
                writableMap.putInt("code", code);
                writableMap.putString("description", message);
                promise.resolve(writableMap);
            }
        });
    }

    /**
     * 登出环信
     */
    @ReactMethod
    public void logout(Boolean isUnbindDeviceToken, final Promise promise) {
        final WritableMap writableMap = Arguments.createMap();
        EMClient.getInstance().logout(isUnbindDeviceToken, new EMCallBack() {

            @Override
            public void onSuccess() {
                if (messageListener != null) {
                    EMClient.getInstance().chatManager().removeMessageListener(messageListener);
                    messageListener = null;
                }
                if (connectListener != null){
                    EMClient.getInstance().removeConnectionListener(connectListener);
                    connectListener = null;
                }
                if (callStateListener != null) {
                    EMClient.getInstance().callManager().removeCallStateChangeListener(callStateListener);
                    callStateListener = null;
                }
                if (callReceiverListener != null) {
                    context.getApplicationContext().unregisterReceiver(callReceiverListener);
                    callReceiverListener = null;
                }
                if (roomChangeListener != null) {
                    EMClient.getInstance().chatroomManager().removeChatRoomListener(roomChangeListener);
                    roomChangeListener = null;
                }
                writableMap.putString("code", "success");
                promise.resolve(writableMap);
            }

            @Override
            public void onProgress(int progress, String status) {

            }

            @Override
            public void onError(int code, String message) {
                Log.d("--main--", "登出失败-----------");
                writableMap.putInt("code", code);
                writableMap.putString("description", message);
                promise.resolve(writableMap);
            }
        });
    }

    /**
     * 配置环信通话
     */
    @ReactMethod
    public void setCallOptions(ReadableMap map) {
        EMCallOptions options = EMClient.getInstance().callManager().getCallOptions();
//        options.setVideoResolution(0, 0);
//        options.setMaxVideoKbps(0);
        options.setIsSendPushIfOffline(true);

    }

    /**
     * 加入聊天室
     */
    @ReactMethod
    public void joinChatroom(String roomId,final Promise promise) {
        final WritableMap writableMap = Arguments.createMap();
        //roomId为聊天室ID
        EMClient.getInstance().chatroomManager().joinChatRoom(roomId, new EMValueCallBack<EMChatRoom>() {
            @Override
            public void onSuccess(EMChatRoom value) {
                //加入聊天室成功
                writableMap.putString("code","success");
                promise.resolve(writableMap);
            }

            @Override
            public void onError(final int error, String errorMsg) {
                //加入聊天室失败
                writableMap.putInt("code",error);
                writableMap.putString("description",errorMsg);
                promise.resolve(writableMap);
            }
        });

    }

    /**
     * 离开聊天室
     */
    @ReactMethod
    public void leaveChatroom(String roomId, Promise promise) {
        EMClient.getInstance().chatroomManager().leaveChatRoom(roomId);
        WritableMap writableMap = Arguments.createMap();
        writableMap.putString("code","success");
        promise.resolve(writableMap);
    }

    /**
     * 获取聊天室详情
     */
    @ReactMethod
    public void getChatroomSpecificationWithId(String roomId, Promise promise) {
        WritableMap writableMap = Arguments.createMap();
        try {
            EMChatRoom chatRoom = EMClient.getInstance().chatroomManager().fetchChatRoomFromServer(roomId);
            writableMap.putString("code","success");
            writableMap.putMap("data", this.chatroomConversionDic(chatRoom));
        } catch (HyphenateException e) {
            e.printStackTrace();
            writableMap.putInt("code", e.getErrorCode());
            writableMap.putString("description", e.getDescription());
        }
        promise.resolve(writableMap);
    }


    /**
     * 发送文本消息
     */
    @ReactMethod
    public void sendTextMessage(String conversationId,Integer chatType,String content,String to,String ext, Promise promise) {
        // 创建一条文本消息，content为消息文字内容，toChatUsername为对方用户或者群聊的id，后文皆是如此
        EMMessage message = EMMessage.createTxtSendMessage(content, to);
        // 发送消息
        this.sendMessage(message,chatType,ext,promise);
    }

    /**
     * 发送语音消息
     */
    @ReactMethod
    public void sendVoiceMessage(String conversationId,Integer chatType,String path,String displayName, Integer duration, String to,String ext, Promise promise) {
        //filePath为语音文件路径，length为录音时间(秒)
        EMMessage message = EMMessage.createVoiceSendMessage(path, duration, to);
        EMVoiceMessageBody voiceBody =  (EMVoiceMessageBody)message.getBody();
        voiceBody.setFileName(displayName);
        message.addBody(voiceBody);
        // 发送消息
        this.sendMessage(message,chatType,ext,promise);
    }


    /**
     * 发起实时通话
     */
    @ReactMethod
    public void startCall(String type, String tartgetName, String ext, Promise promise) {
        L.d("startCall-------" + type + "---" + tartgetName);
        WritableMap writableMap = Arguments.createMap();
        try {//单参数
            if (type.equals("video")) { //视频通话
                EMClient.getInstance().callManager().makeVideoCall(tartgetName, ext);
                //恢复语音传输
                EMClient.getInstance().callManager().resumeVoiceTransfer();
                //恢复视频传输
                EMClient.getInstance().callManager().resumeVideoTransfer();
            } else { //语音通话
                EMClient.getInstance().callManager().makeVoiceCall(tartgetName, ext);
            }
            writableMap.putString("code", "success");
            writableMap.putString("callId",  EMClient.getInstance().callManager().getCurrentCallSession().getCallId());
            writableMap.putString("ext", ext);
        } catch (EMServiceNotReadyException e) {
            writableMap.putInt("code", e.getErrorCode());
            writableMap.putString("description", e.getDescription());
            e.printStackTrace();
        } catch (HyphenateException e) {
            writableMap.putInt("code", e.getErrorCode());
            writableMap.putString("description", e.getDescription());
            e.printStackTrace();
        } finally {
            promise.resolve(writableMap);
        }
    }

    /**
     * 接受通话
     */
    @ReactMethod
    public void answerIncomingCall(String callId, Promise promise) {
        WritableMap writableMap = Arguments.createMap();
        try {
            EMClient.getInstance().callManager().answerCall();
            writableMap.putString("code", "success");
        } catch (EMNoActiveCallException e) {
            writableMap.putInt("code", e.getErrorCode());
            writableMap.putString("description", e.getDescription());
            e.printStackTrace();
        }
        promise.resolve(writableMap);
    }

    /**
     * 结束通话(挂断)
     */
    @ReactMethod
    public void endCall(String callId, int reason, Promise promise) {
        WritableMap writableMap = Arguments.createMap();
        try {
            if (reason == 2) { // 拒接
                L.d("zzzzz" + reason);
                EMClient.getInstance().callManager().rejectCall();
            } else {
                EMClient.getInstance().callManager().endCall();
            }
            writableMap.putString("code", "success");
        } catch (EMNoActiveCallException e) {
            e.printStackTrace();
            writableMap.putInt("code", e.getErrorCode());
            writableMap.putString("description", e.getDescription());
        }
        promise.resolve(writableMap);
    }

    /**
     * 强制结束所有通话
     */
    @ReactMethod
    public void forceEndAllCall() {
        try {
            EMClient.getInstance().callManager().endCall();

        } catch (EMNoActiveCallException e) {
            e.printStackTrace();

        }
    }

    /**
     * 切换摄像头
     */
    @ReactMethod
    public void switchCameraPosition(Boolean isFrontCamera) {
        try {
            if (isFrontCamera) {
                EMClient.getInstance().callManager().setCameraFacing(Camera.CameraInfo.CAMERA_FACING_FRONT);
            } else {
                EMClient.getInstance().callManager().setCameraFacing(Camera.CameraInfo.CAMERA_FACING_BACK);
            }
        } catch (HyphenateException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置是否为扬声器
     *
     * @param isSpeaker 是否为扬声器
     */
    @ReactMethod
    public void setSpeaker(Boolean isSpeaker) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setSpeakerphoneOn(isSpeaker);
        if (!isSpeaker) {
            audioManager.setMode(AudioManager.MODE_NORMAL);
        }
    }

    /**
     * 屏幕常亮
     *
     * @param disabled 是否常亮
     */
    @ReactMethod
    public void setIdleTimerDisabled(final Boolean disabled) {
        context.runOnUiQueueThread(new Runnable() {
            @Override
            public void run() {
                if (disabled) {
                    getCurrentActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                } else {
                    getCurrentActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            }
        });

    }



    //实现ConnectionListener接口
    private class ConnectionListener implements EMConnectionListener {
        @Override
        public void onConnected() {
        }

        @Override
        public void onDisconnected(final int error) {
            context.runOnUiQueueThread(new Runnable() {
                @Override
                public void run() {
                    if (error == EMError.USER_REMOVED) {
                        // 显示帐号已经被移除
                    } else if (error == EMError.USER_LOGIN_ANOTHER_DEVICE) {
                        // 显示帐号在其他设备登录
                        WritableMap params = Arguments.createMap();
                        sendEvent("userAccountDidLoginFromOtherDevice", params);
                    } else {
                    }
                }
            });
        }
    }

    private class MessageListener implements EMMessageListener {

        @Override
        public void onMessageReceived(List<EMMessage> messages) {
            //收到消息
            WritableArray array = Arguments.createArray();
            for(EMMessage message:messages){
                array.pushMap(messageConversionDic(message));
            }
            L.d("onMessageReceived------");
            sendEvent("messagesDidReceive",array);
        }

        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {
            //收到透传消息
        }

        @Override
        public void onMessageRead(List<EMMessage> messages) {
            //收到已读回执
        }

        @Override
        public void onMessageDelivered(List<EMMessage> message) {
            //收到已送达回执
            L.d("onMessageDelivered------");
        }
        @Override
        public void onMessageRecalled(List<EMMessage> messages) {
            //消息被撤回
        }

        @Override
        public void onMessageChanged(EMMessage message, Object change) {
            //消息状态变动
        }

    }

    private class ChatRoomChangeListener implements EMChatRoomChangeListener {

        @Override
        public void onChatRoomDestroyed(String roomId, String roomName) {
            WritableMap writableMap = Arguments.createMap();
            writableMap.putInt("reason",1);
            sendEvent("didDismissFromChatroom",writableMap);
        }

        @Override
        public void onMemberJoined(String roomId, String participant) {
            WritableMap writableMap = Arguments.createMap();
            writableMap.putString("userName",participant);
            sendEvent("userDidJoinChatroom",writableMap);
        }

        @Override
        public void onMemberExited(String roomId, String roomName, String participant) {
            WritableMap writableMap = Arguments.createMap();
            writableMap.putString("userName",participant);
            sendEvent("userDidLeaveChatroom",writableMap);
        }


        @Override
        public void onMuteListAdded(final String chatRoomId, final List<String> mutes, final long expireTime) {

        }

        @Override
        public void onMuteListRemoved(final String chatRoomId, final List<String> mutes) {

        }

        @Override
        public void onAdminAdded(final String chatRoomId, final String admin) {

        }

        @Override
        public void onAdminRemoved(final String chatRoomId, final String admin) {

        }

        @Override
        public void onOwnerChanged(final String chatRoomId, final String newOwner, final String oldOwner) {

        }
        @Override
        public void onAnnouncementChanged(String chatRoomId, final String announcement) {

        }

        @Override
        public void onRemovedFromChatRoom (int reason, String roomId, String roomName, String participant){

        }

    }


    /**
     * 呼入监听
     */
    private class CallReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // call type
            String type = intent.getStringExtra("type");
            // 拨打方callId
            String callId = EMClient.getInstance().callManager().getCurrentCallSession().getCallId();
            //拓展字段
            String  ext = EMClient.getInstance().callManager().getCurrentCallSession().getExt();
            //通知RN
            WritableMap params = Arguments.createMap();
            params.putString("callId", callId);
            params.putString("type", type);
            params.putString("ext", ext);
            sendEvent( "callDidReceive", params);
        }
    }

    /**
     * 通话状态监听
     */
    private class CallStateListener implements EMCallStateChangeListener {

        @Override
        public void onCallStateChanged(CallState callState, CallError callError) {
            switch (callState) {
                case CONNECTING: // 正在呼叫对方,没见回调过

                    break;
                case CONNECTED: // 双方已经建立连接
                    //通话通道建立完成，用户A和用户B都会收到这个回调 通知RN
                    WritableMap params = Arguments.createMap();
                    sendEvent("callDidConnect", params);
                    break;
                case ACCEPTED: // 被同意
                    WritableMap params_accept = Arguments.createMap();
                    params_accept.putString("callId", EMClient.getInstance().callManager().getCurrentCallSession().getCallId());
                    params_accept.putString("ext", EMClient.getInstance().callManager().getCurrentCallSession().getExt());
                    sendEvent( "callDidAccept", params_accept);
                    break;
                case DISCONNECTED: // 通话已中断
                    String code = "success";
                    String description = "";
                    int reason = 0;
                    code = callError.toString();
                    description = callError.toString();
                    boolean isCaller = false;
                    L.d("通话已结束" + callError);
                    // 通话结束，重置通话状态
                    if (callError == CallError.ERROR_UNAVAILABLE) {
                        L.d("对方不在线" + callError);
                        isCaller = true;
                        reason = 6;
                    } else if (callError == CallError.ERROR_BUSY) {
                        L.d("对方正忙" + callError);
                        isCaller = true;
                        reason = 3;
                    } else if (callError == CallError.REJECTED) {
                        L.d("对方已拒绝" + callError);
                        isCaller = true;
                        reason = 2;
                    } else if (callError == CallError.ERROR_NORESPONSE) {
                        L.d("对方未响应，可能手机不在身边" + callError);
                        isCaller = true;
                        reason = 1;
                    } else if (callError == CallError.ERROR_TRANSPORT) {
                        L.d("连接建立失败" + callError);
                        reason = 0;
                    } else if (callError == CallError.ERROR_LOCAL_SDK_VERSION_OUTDATED) {
                        L.d("双方通讯协议不同" + callError);
                        reason = 0;
                    } else if (callError == CallError.ERROR_REMOTE_SDK_VERSION_OUTDATED) {
                        L.d("双方通讯协议不同" + callError);
                        reason = 0;
                    } else if (callError == CallError.ERROR_NO_DATA) {
                        L.d("没有通话数据" + callError);
                        reason = 0;
                    } else {
                        L.d("通话已结束-----????" + callError);
                        code = "success";
                        description = "";
                        reason = 0;
                    }
                    WritableMap params_dis_connected = Arguments.createMap();
                    params_dis_connected.putString("code", code);
                    params_dis_connected.putInt("reason", reason);
                    params_dis_connected.putBoolean("isCaller", isCaller);
                    params_dis_connected.putString("ext", "{}");
                    sendEvent("callDidEnd", params_dis_connected);
                    break;
                case NETWORK_DISCONNECTED:
                    L.d("对方网络不可用");
                    break;
                case NETWORK_NORMAL:
                    WritableMap params_dis_normal = Arguments.createMap();
                    sendEvent( "callStateDidChange", params_dis_normal);
                    L.d("网络恢复正常");
                    break;
                case NETWORK_UNSTABLE:
                    if (callError == CallError.ERROR_NO_DATA) {
                        L.d("没有通话数据" + callError);
                    } else {
                        L.d("网络不稳定" + callError);
                    }
                    break;
                case VIDEO_PAUSE:
                    L.d("视频传输已暂停");
                    break;
                case VIDEO_RESUME:
                    L.d("视频传输已恢复");
                    break;
                case VOICE_PAUSE:
                    L.d("语音传输已暂停");
                    break;
                case VOICE_RESUME:
                    L.d("语音传输已恢复");
                    break;
                default:
                    break;
            }
        }
    }

    private WritableMap chatroomConversionDic(EMChatRoom room){
        WritableMap writableMap = Arguments.createMap();
        writableMap.putString("chatroomId",room.getId());
        writableMap.putString("description",room.getDescription());
        writableMap.putString("owner",room.getOwner());
        writableMap.putInt("maxOccupantsCount",room.getMaxUsers());
        writableMap.putInt("occupantsCount",room.getMemberCount());
        return writableMap;
    }

    private WritableMap messageConversionDic(EMMessage message){
        WritableMap writableMap = Arguments.createMap();
        WritableMap bodyMap = Arguments.createMap();
        String chatType = "", status = "";
        switch (message.getChatType()) {
            case Chat:
                chatType = "Chat";
                break;
            case GroupChat:
                chatType = "GroupChat";
                break;
            case ChatRoom:
                chatType = "ChatRoom";
                break;
            default:
                break;
        }
        switch (message.status()) {
            case CREATE:
                status = "Pending";
                break;
            case SUCCESS:
                status = "Succeed";
                break;
            case FAIL:
                status = "Failed";
                break;
            case INPROGRESS:
                status = "Delivering";
                break;
            default:
                break;
        }
        EMMessageBody messageBody = message.getBody();
        switch (message.getType()) {
            case TXT:
                EMTextMessageBody textBody = (EMTextMessageBody)messageBody;
                WritableMap textDataMap = Arguments.createMap();
                textDataMap.putString("content",textBody.getMessage());
                bodyMap.putString("type","Text");
                bodyMap.putMap("data",textDataMap);
                break;
            case IMAGE:

                break;
            case VOICE:
                EMVoiceMessageBody voiceBody =  (EMVoiceMessageBody)messageBody;
                WritableMap voiceDataMap = Arguments.createMap();
                voiceDataMap.putString("remotePath",voiceBody.getRemoteUrl());
                voiceDataMap.putString("localPath",voiceBody.getLocalUrl());
                voiceDataMap.putDouble("fileLength",0);
                voiceDataMap.putDouble("duration",voiceBody.getLength());
                voiceDataMap.putString("displayName",voiceBody.displayName());
                bodyMap.putString("type","Voice");
                bodyMap.putMap("data",voiceDataMap);
                break;
            case VIDEO:

                break;
            case LOCATION:

                break;
            case FILE:

                break;
            case CMD:

                break;
            default:
                break;
        }
        writableMap.putString("messageId",message.getMsgId());
        writableMap.putString("conversationId",message.conversationId());
        writableMap.putString("direction",message.direct() == EMMessage.Direct.SEND ? "Send":"Receive");
        writableMap.putString("from",message.getFrom());
        writableMap.putString("to",message.getTo());
        writableMap.putDouble("timestamp", message.getMsgTime());
        writableMap.putDouble("localTime", message.localTime());
        writableMap.putString("chatType", chatType);
        writableMap.putString("status", status);
        writableMap.putBoolean("isReadAcked", message.isAcked());
        writableMap.putBoolean("isDeliverAcked", message.isDelivered());
        writableMap.putBoolean("isRead", message.isUnread());
        writableMap.putMap("body", bodyMap);
        WritableMap extWritableMap = Arguments.createMap();
        Map<String, Object> extMap = message.ext();
        for (Map.Entry<String, Object> entry : extMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof String) {
                extWritableMap.putString(key,String.valueOf(value));
            }
        }
        writableMap.putMap("ext", extWritableMap);
        return writableMap;
    }


    private void sendMessage(final EMMessage message,Integer chatType,String ext,final Promise promise) {
        final WritableMap writableMap = Arguments.createMap();
        if (chatType == 0) {
            message.setChatType(EMMessage.ChatType.Chat);
        } else if(chatType == 1){
            message.setChatType(EMMessage.ChatType.GroupChat);
        } else {
            message.setChatType(EMMessage.ChatType.ChatRoom);
        }
        message.setAttribute("data", ext);
        message.setMessageStatusCallback(new EMCallBack(){
            @Override
            public void onSuccess() {
                writableMap.putString("code","success");
                writableMap.putMap("data", messageConversionDic(message));
                promise.resolve(writableMap);
            }

            @Override
            public void onProgress(int progress, String status) {

            }

            @Override
            public void onError(int code, String message) {
                writableMap.putInt("code",code);
                writableMap.putString("description", message);
                promise.resolve(writableMap);
            }
        });
        EMClient.getInstance().chatManager().sendMessage(message);
    }


    /**
     * 发送事件封装
     */
    private void sendEvent(String eventName, @Nullable Object params) {
        context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, params);
    }
}

