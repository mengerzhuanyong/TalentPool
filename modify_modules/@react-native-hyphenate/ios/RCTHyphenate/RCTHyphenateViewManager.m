//
//  HyphenateViewManager.m
//  hyphenate
//
//  Created by jiasong on 2018/11/8.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import "RCTHyphenateViewManager.h"
#import <React/RCTBridge.h>
#import <React/RCTUIManager.h>
#import "RCTHyphenateView.h"
#import <Hyphenate/Hyphenate.h>

@interface RCTHyphenateViewManager()


@end

@implementation RCTHyphenateViewManager

RCT_EXPORT_MODULE(RCTHyphenateView);


-(void)dealloc {
 
}

+ (BOOL)requiresMainQueueSetup
{
  return YES;
}

- (UIView *)view {
  RCTHyphenateView *hyphenateView = [[RCTHyphenateView alloc] init];
  return hyphenateView;
}

RCT_CUSTOM_VIEW_PROPERTY(showLocalView, BOOL, RCTHyphenateView) {
  BOOL isLocal = [RCTConvert BOOL:json];
  [view setLocalView:isLocal];
}

@end
