//
//  CallSession.h
//  RCTHyphenate
//
//  Created by jiasong on 2019/4/1.
//  Copyright © 2019 jiasong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Hyphenate/Hyphenate.h>

NS_ASSUME_NONNULL_BEGIN

@interface CallSession : NSObject

+ (CallSession *)sharedSession;

-(void)setCallSession:(nullable EMCallSession *)callSession;

-(EMCallSession *)getCallSession;

@end

NS_ASSUME_NONNULL_END
