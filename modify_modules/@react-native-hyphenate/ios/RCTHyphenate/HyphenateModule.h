//
//  HyphenateModule.h
//  hyphenate
//
//  Created by jiasong on 2018/11/8.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTEventEmitter.h>
#import <React/RCTBridgeModule.h>

@interface HyphenateModule : RCTEventEmitter<RCTBridgeModule>

@end
