//
//  HyphenateView.m
//  hyphenate
//
//  Created by jiasong on 2018/11/8.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import "RCTHyphenateView.h"
#import <React/RCTBridge.h>
#import <React/RCTUIManager.h>
#import "CallSession.h"
#import <Hyphenate/Hyphenate.h>

@interface RCTHyphenateView()

@property (nonatomic,assign) BOOL isLocal;

@end

@implementation RCTHyphenateView

-(instancetype)init {
  if ([super init]) {
   
  }
  return self;
};

-(void)dealloc {
  EMCallSession *callSession = [[CallSession sharedSession] getCallSession];
  if (callSession.localVideoView || callSession.remoteVideoView) {
    if (_isLocal) {
      [callSession.localVideoView removeFromSuperview];
      callSession.localVideoView = nil;
    } else {
      [callSession.remoteVideoView removeFromSuperview];
      callSession.remoteVideoView = nil;
    }
  }
}

-(void)layoutSubviews {
  [super layoutSubviews];
  EMCallSession *callSession = [[CallSession sharedSession] getCallSession];
  NSLog(@"%@---layoutSubviews",NSStringFromCGRect(self.bounds));
  if (callSession.localVideoView || callSession.remoteVideoView) {
    if (_isLocal) {
      callSession.localVideoView.frame = self.bounds;
    } else {
      callSession.remoteVideoView.frame = self.bounds;
    }
  }
}

-(void)setLocalView:(BOOL)isLocal {
  EMCallSession *callSession = [[CallSession sharedSession] getCallSession];
  if (!callSession) {
    NSLog(@"callSession不存在");
    return;
  }
  for (UIView *subView in self.subviews) {
    if ([subView isMemberOfClass:[EMCallLocalView class]] || [subView isMemberOfClass:[EMCallRemoteView class]]) {
      NSLog(@"已经存在视图");
      return;
    }
  }
  _isLocal = isLocal;
  NSLog(@"%@---setLocalView",NSStringFromCGRect(self.bounds));
  if (isLocal) {
     callSession.localVideoView = [[EMCallLocalView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self addSubview:callSession.localVideoView];
  } else {
    //同意接听视频通话之后
    callSession.remoteVideoView = [[EMCallRemoteView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    //设置视频页面缩放方式
    callSession.remoteVideoView.scaleMode = EMCallViewScaleModeAspectFill;
    [self addSubview:callSession.remoteVideoView];
  }
}

@end
