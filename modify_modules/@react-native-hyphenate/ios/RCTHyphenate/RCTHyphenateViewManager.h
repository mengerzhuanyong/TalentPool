//
//  HyphenateViewManager.h
//  hyphenate
//
//  Created by jiasong on 2018/11/8.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <React/RCTViewManager.h>
#import <React/RCTBridgeModule.h>

@interface RCTHyphenateViewManager : RCTViewManager<RCTBridgeModule>

@end
