//
//  CallSession.m
//  RCTHyphenate
//
//  Created by jiasong on 2019/4/1.
//  Copyright © 2019 jiasong. All rights reserved.
//

#import "CallSession.h"

@interface CallSession()

@property (nonatomic, strong) EMCallSession *rtcCallSession;

@end

@implementation CallSession

static CallSession *session = nil;
static dispatch_once_t onceToken;

+ (CallSession *)sharedSession {
    dispatch_once(&onceToken, ^{
        session = [[CallSession alloc]init];
    });
    return session;
}

-(void)setCallSession:(nullable EMCallSession *)callSession {
    _rtcCallSession = callSession;
}

-(EMCallSession *)getCallSession {
    return _rtcCallSession;
}

@end
