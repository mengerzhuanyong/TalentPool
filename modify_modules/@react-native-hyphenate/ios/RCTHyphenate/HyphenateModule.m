//
//  HyphenateModule.m
//  hyphenate
//
//  Created by jiasong on 2018/11/8.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import "HyphenateModule.h"
#import <Hyphenate/Hyphenate.h>
#import <Hyphenate/EMMessage.h>
#import "CallSession.h"
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

@interface HyphenateModule()<EMClientDelegate,EMCallManagerDelegate,EMChatroomManagerDelegate,EMChatManagerDelegate>

@end

@implementation HyphenateModule

RCT_EXPORT_MODULE();

+ (BOOL)requiresMainQueueSetup {
return YES;
}

-(instancetype)init {
if ([super init]) {
    
}
return self;
}

-(void)dealloc {
  // 移除回调
  [[EMClient sharedClient] removeDelegate:self];
  //移除实时通话回调
  [[EMClient sharedClient].callManager removeDelegate:self];
  //移除聊天室回调
  [[EMClient sharedClient].roomManager removeDelegate:self];
  //移除消息回调
  [[EMClient sharedClient].chatManager removeDelegate:self];
  [self removeBackgroundNotificationCenter];
  [[CallSession sharedSession] setCallSession:nil];
    
}

-(NSArray<NSString *> *)supportedEvents {
return @[@"callDidReceive",
         @"callDidConnect",
         @"callDidAccept",
         @"callDidEnd",
         @"callStateDidChange",
         @"userAccountDidLoginFromOtherDevice",
         @"messagesDidReceive",
         @"userDidJoinChatroom",
         @"userDidLeaveChatroom",
         @"didDismissFromChatroom"
        ];
}

-(void)addBackgroundNotificationCenter {
[self removeBackgroundNotificationCenter];
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

-(void)removeBackgroundNotificationCenter  {
[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
}

-(void)didEnterBackground:(NSNotification *)notification {
UIApplication *application = notification.object;
[[EMClient sharedClient] applicationDidEnterBackground:application];
}

-(void)willEnterForeground:(NSNotification *)notification {
UIApplication *application = notification.object;
[[EMClient sharedClient] applicationWillEnterForeground:application];
}

-(void)sendMessageWithConversationId:(NSString *)conversationId chatType:(NSInteger)chatType body:(EMMessageBody *)body to:(NSString *)to  ext:(NSString *)messageExt resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject {
    NSString *from = [[EMClient sharedClient] currentUsername];
    EMMessage *message = [[EMMessage alloc] initWithConversationID:conversationId from:from to:to body:body ext:@{@"data":messageExt}];
    message.timestamp = [[NSDate date] timeIntervalSince1970];
    EMConversationType conversationType;
    if (chatType == 0) {
        message.chatType = EMChatTypeChat;
        conversationType = EMConversationTypeChat;
    } else if(chatType == 1){
        message.chatType = EMChatTypeGroupChat;
        conversationType = EMConversationTypeGroupChat;
    } else {
        message.chatType = EMChatTypeChatRoom;
        conversationType = EMConversationTypeChatRoom;
    }
    [[EMClient sharedClient].chatManager sendMessage:message progress:nil completion:^(EMMessage *aMessage, EMError *aError) {
        if (!aError) {
            resolve(@{@"code":@"success",@"data":[self messageConversionDic:aMessage]});
        } else {
            resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
        }
    }];
    //    EMConversation *conversation =  [[EMClient sharedClient].chatManager getConversation:message.conversationId type:conversationType createIfNotExist:YES];
    //    [conversation insertMessage:message error:nil];
}

-(NSDictionary *)chatroomConversionDic:(EMChatroom *)room {
    NSDictionary *dic =  @{@"chatroomId":room.chatroomId,
                           @"description":room.description,
                           @"owner":room.owner,
                           @"maxOccupantsCount":@(room.maxOccupantsCount),
                           @"occupantsCount":@(room.occupantsCount),
                           };
    return dic;
}

-(NSDictionary *)messageConversionDic:(EMMessage *)message {
    NSString *chatType = @"",*status = @"";
    NSDictionary *bodyDic = nil;
    switch (message.chatType) {
        case EMChatTypeChat:
             chatType = @"Chat";
            break;
        case EMChatTypeGroupChat:
            chatType = @"GroupChat";
            break;
        default:
            chatType = @"ChatRoom";
            break;
    }
    switch (message.status) {
        case EMMessageStatusPending:
            status = @"Pending";
            break;
        case EMMessageStatusDelivering:
            status = @"Delivering";
            break;
        case EMMessageStatusSucceed:
            status = @"Succeed";
            break;
        case EMMessageStatusFailed:
            status = @"Failed";
            break;
        default:
            break;
    }
    
    EMMessageBody *msgBody = message.body;
    switch (msgBody.type) {
        case EMMessageBodyTypeText:
        {
            // 收到的文字消息
            EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
            bodyDic = @{@"type":@"Text",@"data":@{@"content":textBody.text}};
        }
            break;
        case EMMessageBodyTypeImage:
        {
            // 得到一个图片消息body
            EMImageMessageBody *body = ((EMImageMessageBody *)msgBody);
            NSLog(@"大图remote路径 -- %@"   ,body.remotePath);
            NSLog(@"大图local路径 -- %@"    ,body.localPath); // // 需要使用sdk提供的下载方法后才会存在
            NSLog(@"大图的secret -- %@"    ,body.secretKey);
            NSLog(@"大图的W -- %f ,大图的H -- %f",body.size.width,body.size.height);
            NSLog(@"大图的下载状态 -- %u",body.downloadStatus);
            // 缩略图sdk会自动下载
            NSLog(@"小图remote路径 -- %@"   ,body.thumbnailRemotePath);
            NSLog(@"小图local路径 -- %@"    ,body.thumbnailLocalPath);
            NSLog(@"小图的secret -- %@"    ,body.thumbnailSecretKey);
            NSLog(@"小图的W -- %f ,大图的H -- %f",body.thumbnailSize.width,body.thumbnailSize.height);
            NSLog(@"小图的下载状态 -- %u",body.thumbnailDownloadStatus);
        }
            break;
        case EMMessageBodyTypeLocation:
        {
            EMLocationMessageBody *body = (EMLocationMessageBody *)msgBody;
            NSLog(@"纬度-- %f",body.latitude);
            NSLog(@"经度-- %f",body.longitude);
            NSLog(@"地址-- %@",body.address);
        }
            break;
        case EMMessageBodyTypeVoice:
        {
            // 音频sdk会自动下载
            EMVoiceMessageBody *voiceBody = (EMVoiceMessageBody *)msgBody;
       
            NSLog(@"音频remote路径 -- %@"      ,voiceBody.remotePath);
            NSLog(@"音频local路径 -- %@"       ,voiceBody.localPath); // 需要使用sdk提供的下载方法后才会存在（音频会自动调用）
            NSLog(@"音频的secret -- %@"        ,voiceBody.secretKey);
            NSLog(@"音频文件大小 -- %lld"       ,voiceBody.fileLength);
            NSLog(@"音频文件的下载状态 -- %u"   ,voiceBody.downloadStatus);
            NSLog(@"音频的时间长度 -- %d"      ,voiceBody.duration);
            bodyDic = @{@"type":@"Voice",
                        @"data":@{
                                @"remotePath":voiceBody.remotePath,
                                @"localPath":voiceBody.localPath,
                                @"fileLength":@(voiceBody.fileLength),
                                @"duration":@(voiceBody.duration),
                                @"displayName":voiceBody.displayName,
                                }};
        }
            break;
        case EMMessageBodyTypeVideo:
        {
            EMVideoMessageBody *body = (EMVideoMessageBody *)msgBody;
            NSLog(@"视频remote路径 -- %@"      ,body.remotePath);
            NSLog(@"视频local路径 -- %@"       ,body.localPath); // 需要使用sdk提供的下载方法后才会存在
            NSLog(@"视频的secret -- %@"        ,body.secretKey);
            NSLog(@"视频文件大小 -- %lld"       ,body.fileLength);
            NSLog(@"视频文件的下载状态 -- %u"   ,body.downloadStatus);
            NSLog(@"视频的时间长度 -- %d"      ,body.duration);
            NSLog(@"视频的W -- %f ,视频的H -- %f", body.thumbnailSize.width, body.thumbnailSize.height);
            // 缩略图sdk会自动下载
            NSLog(@"缩略图的remote路径 -- %@"     ,body.thumbnailRemotePath);
            NSLog(@"缩略图的local路径 -- %@"      ,body.thumbnailLocalPath);
            NSLog(@"缩略图的secret -- %@"        ,body.thumbnailSecretKey);
            NSLog(@"缩略图的下载状态 -- %u"      ,body.thumbnailDownloadStatus);
        }
            break;
        case EMMessageBodyTypeFile:
        {
            EMFileMessageBody *body = (EMFileMessageBody *)msgBody;
            NSLog(@"文件remote路径 -- %@"      ,body.remotePath);
            NSLog(@"文件local路径 -- %@"       ,body.localPath);
            NSLog(@"文件的secret -- %@"        ,body.secretKey);
            NSLog(@"文件文件大小 -- %lld"       ,body.fileLength);
            NSLog(@"文件文件的下载状态 -- %u"   ,body.downloadStatus);
        }
            break;
            
        default:
            break;
    }
   
    
    NSDictionary *dic = @{
                          @"messageId":message.messageId,
                          @"conversationId":message.conversationId,
                          @"direction":message.direction == EMMessageDirectionSend ? @"Send" : @"Receive",
                          @"from":message.from,
                          @"to":message.to,
                          @"timestamp":@(message.timestamp),
                          @"localTime":@(message.localTime),
                          @"chatType":chatType,
                          @"status":status,
                          @"isReadAcked":@(message.isReadAcked),
                          @"isDeliverAcked":@(message.isDeliverAcked),
                          @"isRead":@(message.isRead),
                          @"body":bodyDic,
                          @"ext":message.ext ? message.ext : @{@"data":@"{}"}
                          };
    return dic;
}


RCT_EXPORT_METHOD(init:(NSDictionary *)dic resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
 NSString *appKey = dic[@"appKey"];
 NSString *apnsCertName = dic[@"apnsCertName"];
 //AppKey:注册的AppKey，详细见下面注释。
 //apnsCertName:推送证书名（不需要加后缀），详细见下面注释。
 EMOptions *options = [EMOptions optionsWithAppkey:appKey];
 options.apnsCertName = apnsCertName;
 EMError *error = [[EMClient sharedClient] initializeSDKWithOptions:options];
 if (!error) {
    resolve(@{@"code":@"success"});
    [self addBackgroundNotificationCenter];
    NSLog(@"登录成功");
 }  else {
    resolve(@{@"code":@(error.code),@"description":error.errorDescription});
    NSLog(@"登录失败");
 }
 AVAudioSession *audioSession = [AVAudioSession sharedInstance];
 //默认情况下扬声器播放
 [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
 [audioSession setActive:YES error:nil];
}

RCT_EXPORT_METHOD(login:(NSDictionary *)dic resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
 NSString *userName = dic[@"userName"];
 NSString *password = dic[@"password"];
 [[EMClient sharedClient] loginWithUsername:userName password:password completion:^(NSString *aUsername, EMError *aError) {
    if (!aError) {
        resolve(@{@"code":@"success"});
        NSLog(@"登录成功");
        // 注册实时通话回调
        [[EMClient sharedClient].callManager addDelegate:self delegateQueue:nil];
        // 注册聊天室回调
        [[EMClient sharedClient].roomManager addDelegate:self delegateQueue:nil];
        // 注册消息回调
        [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
        // 注册回调
        [[EMClient sharedClient] addDelegate:self delegateQueue:nil];
    } else {
        resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
        NSLog(@"登录失败");
    }
 }];
}
/*!
*  \~chinese
*  退出
*
*  @param aIsUnbindDeviceToken 是否解除device token的绑定，解除绑定后设备不会再收到消息推送
*         如果传入YES, 解除绑定失败，将返回error

*
*/
RCT_EXPORT_METHOD(logout:(BOOL)aIsUnbindDeviceToken resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
[[EMClient sharedClient] logout:aIsUnbindDeviceToken completion:^(EMError *aError) {
    if (!aError) {
        resolve(@{@"code":@"success"});
        NSLog(@"退出成功");
        // 移除回调
        [[EMClient sharedClient] removeDelegate:self];
        // 移除实时通话回调
        [[EMClient sharedClient].callManager removeDelegate:self];
        // 移除聊天室回调
        [[EMClient sharedClient].roomManager removeDelegate:self];
        // 移除消息回调
        [[EMClient sharedClient].chatManager removeDelegate:self];
    } else {
        resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
        NSLog(@"退出失败");
    }
}];
}


RCT_EXPORT_METHOD(setCallOptions:(NSDictionary *)dic) {
  BOOL isSendPushIfOffline = dic[@"isSendPushIfOffline"];
  EMCallOptions *options = [[EMClient sharedClient].callManager getCallOptions];
  //当对方不在线时，是否给对方发送离线消息和推送，并等待对方回应
  options.isSendPushIfOffline = isSendPushIfOffline;
  options.offlineMessageText = @"问雅咨询";
  //设置视频分辨率：自适应分辨率、352 * 288、640 * 480、1280 * 720
  options.videoResolution = EMCallVideoResolutionAdaptive;
  //最大视频码率，范围 50 < videoKbps < 5000, 默认0, 0为自适应，建议设置为0
  options.maxVideoKbps = 0;
 //最小视频码率
 options.minVideoKbps = 0;
 //是否固定视频分辨率，默认为NO
 options.isFixedVideoResolution = NO;
 [[EMClient sharedClient].callManager setCallOptions:options];

 EMPushOptions *emoptions = [[EMClient sharedClient] pushOptions];
 //设置有消息过来时的显示方式:1.显示收到一条消息 2.显示具体消息内容
 emoptions.displayStyle = EMPushDisplayStyleSimpleBanner;
 [[EMClient sharedClient] updatePushOptionsToServer];

}



RCT_EXPORT_METHOD(sendTextMessage:(NSString *)conversationId chatType:(nonnull NSNumber *)chatType content:(NSString *)content to:(NSString *)to  ext:(NSString *)messageExt resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject)  {
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:content];
    [self sendMessageWithConversationId:conversationId chatType:chatType.integerValue body:body to:to ext:messageExt resolve:resolve rejecte:reject];
}

RCT_EXPORT_METHOD(sendImageMessage:(NSString *)conversationId chatType:(nonnull NSNumber *)chatType path:(NSString *)path displayName:(NSString *)displayName to:(NSString *)to  ext:(NSString *)messageExt resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject)  {
    NSData *data = [NSData dataWithContentsOfFile:path];
    EMImageMessageBody *body = [[EMImageMessageBody alloc] initWithData:data displayName:displayName];
    [self sendMessageWithConversationId:conversationId chatType:chatType.integerValue body:body to:to ext:messageExt resolve:resolve rejecte:reject];
}

RCT_EXPORT_METHOD(sendVoiceMessage:(NSString *)conversationId chatType:(nonnull NSNumber *)chatType path:(NSString *)path displayName:(NSString *)displayName duration:(nonnull NSNumber *)duration to:(NSString *)to  ext:(NSString *)messageExt resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
    NSData *data = [NSData dataWithContentsOfFile:path];
    EMVoiceMessageBody *body = [[EMVoiceMessageBody alloc] initWithData:data displayName:displayName];
    body.duration = duration.intValue;
   [self sendMessageWithConversationId:conversationId chatType:chatType.integerValue body:body to:to ext:messageExt resolve:resolve rejecte:reject];
}

RCT_EXPORT_METHOD(fetchHistoryMessagesWithConversationId:(NSString *)conversationId chatType:(nonnull NSNumber *)chatType startMessageId:(NSString *)startMessageId pageSize:(nonnull NSNumber *)pageSize resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
    EMConversationType conversationType;
    switch (chatType.integerValue) {
        case 0:
            conversationType = EMConversationTypeChat;
            break;
        case 1:
            conversationType = EMConversationTypeGroupChat;
            break;
        default:
            conversationType = EMConversationTypeChatRoom;
            break;
    }
    [[EMClient sharedClient].chatManager asyncFetchHistoryMessagesFromServer:conversationId conversationType:conversationType startMessageId:startMessageId pageSize:pageSize.intValue completion:^(EMCursorResult *aResult, EMError *aError) {
        if (!aError) {
            resolve(@{@"code":@"success"});
        } else {
            resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
        }
    }];
}



# pragma 聊天室接口-- 下箭头

/*!
*  \~chinese
*  获取一个会话
*
*  @param aConversationId  会话ID
*  @param aType            会话类型
*  @param aIfCreate        如果不存在是否创建
*
*/

RCT_EXPORT_METHOD(getConversation:(NSString *)conversationId type:(nonnull NSNumber *)aType) {
  EMConversationType _type ;
  if (aType.integerValue == 0) {
     _type = EMConversationTypeChat;
  } else if (aType.integerValue == 1) {
    _type = EMConversationTypeGroupChat;
  } else {
    _type = EMConversationTypeChatRoom;
  }
  [[EMClient sharedClient].chatManager getConversation:conversationId type:_type createIfNotExist:YES];
}

/*!
*  从服务器获取指定数目的聊天室
*
*  @param aPageNum              获取第几页
*  @param aPageSize             获取多少条

*/
RCT_EXPORT_METHOD(getChatroomsWithPage:(nonnull NSNumber *)aPageNum pageSize:(nonnull NSNumber *)aPageSize resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
[[EMClient sharedClient].roomManager getChatroomsFromServerWithPage:aPageNum.integerValue pageSize:aPageSize.integerValue completion:^(EMPageResult *aResult, EMError *aError) {
    if (!aError) {
        resolve(@{@"code":@"success",@"data":aResult.list});
    } else {
        resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
    }
}];
}

/*!
*  加入聊天室
*
*  @param aChatroomId           聊天室的ID
*/
RCT_EXPORT_METHOD(joinChatroom:(NSString *)aChatroomId resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
[[EMClient sharedClient].roomManager joinChatroom:aChatroomId completion:^(EMChatroom *aChatroom, EMError *aError) {
    if (!aError) {
        resolve(@{@"code":@"success"});
    } else {
        resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
    }
}];
}

/*!
*  退出聊天室
*
*  @param aChatroomId          聊天室ID
*/
RCT_EXPORT_METHOD(leaveChatroom:(NSString *)aChatroomId resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
[[EMClient sharedClient].roomManager leaveChatroom:aChatroomId completion:^(EMError *aError) {
    if (!aError) {
        resolve(@{@"code":@"success"});
    } else {
        resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
    }
}];
}

/*!
*  获取聊天室被禁言列表
*
*  @param aChatroomId      聊天室ID
*  @param aPageNum         获取第几页
*  @param aPageSize        获取多少条

*/
RCT_EXPORT_METHOD(getChatroomMuteListWithId:(NSString *)aChatroomId  pageNumber:(nonnull NSNumber *)aPageNum pageSize:(nonnull NSNumber *)aPageSize resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
 [[EMClient sharedClient].roomManager getChatroomMuteListFromServerWithId:aChatroomId pageNumber:aPageNum.integerValue pageSize:aPageSize.integerValue completion:^(NSArray *aList, EMError *aError) {
    if (!aError) {
        resolve(@{@"code":@"success",@"data":aList});
    } else {
        resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
    }
 }];
}

/*!
*  将一组成员禁言，需要Owner / Admin权限
*
*  @param aMuteMembers         要禁言的成员列表<NSString>
*  @param aMuteMilliseconds    禁言时长
*  @param aChatroomId          聊天室ID

*/
RCT_EXPORT_METHOD(muteMembers:(NSArray *)aMuteMembers muteMilliseconds:(nonnull NSNumber *)aMuteMilliseconds fromChatroom:(NSString *)aChatroomId resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
[[EMClient sharedClient].roomManager muteMembers:aMuteMembers muteMilliseconds:aMuteMilliseconds.integerValue fromChatroom:aChatroomId completion:^(EMChatroom *aChatroom, EMError *aError) {
    if (!aError) {
        resolve(@{@"code":@"success"});
    } else {
        resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
    }
}];
}


/*!
*  解除禁言，需要Owner / Admin权限
*
*  @param aMembers     被解除的列表<NSString>
*  @param aChatroomId      聊天室ID

*/
RCT_EXPORT_METHOD(unmuteMembers:(NSArray *)aMembers fromChatroom:(NSString *)aChatroomId resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
[[EMClient sharedClient].roomManager unmuteMembers:aMembers fromChatroom:aChatroomId completion:^(EMChatroom *aChatroom, EMError *aError) {
    if (!aError) {
        resolve(@{@"code":@"success"});
    } else {
        resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
    }
}];
}


/*!
*  获取聊天室详情
*
*  @param aChatroomId           聊天室ID
*
*/
RCT_EXPORT_METHOD(getChatroomSpecificationWithId:(NSString *)aChatroomId resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
[[EMClient sharedClient].roomManager getChatroomSpecificationFromServerWithId:aChatroomId completion:^(EMChatroom *aChatroom, EMError *aError) {
    if (!aError) {
        resolve(@{@"code":@"success",@"data":[self chatroomConversionDic:aChatroom]});
    } else {
        resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
    }
}];
}

/*!
*  获取聊天室成员列表
*
*  @param aChatroomId      聊天室ID
*  @param aCursor          游标
*  @param aPageSize        获取多少条

*/
RCT_EXPORT_METHOD(getChatroomMemberListWithId:(NSString *)aChatroomId  cursor:(NSString *)aCursor pageSize:(nonnull NSNumber *)aPageSize resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
[[EMClient sharedClient].roomManager getChatroomMemberListFromServerWithId:aChatroomId cursor:aCursor pageSize:aPageSize.integerValue completion:^(EMCursorResult *aResult, EMError *aError) {
    if (!aError) {
        resolve(@{@"code":@"success",@"data":aResult.list});
    } else {
        resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
    }
}];
}

// 收到消息的回调，带有附件类型的消息可以用 SDK 提供的下载附件方法下载（后面会讲到）
- (void)messagesDidReceive:(NSArray *)aMessages {
    NSMutableArray *array = [NSMutableArray array];
    for (EMMessage *message in aMessages) {
        NSDictionary *dic = [self messageConversionDic:message];
        [array addObject:dic];
    }
    [self sendEventWithName:@"messagesDidReceive" body:array];
}

/*!
 *  有用户加入聊天室
 *
 *  @param aChatroom    加入的聊天室
 *  @param aUsername    加入者
 */
- (void)userDidJoinChatroom:(EMChatroom *)aChatroom user:(NSString *)aUsername {
    [self sendEventWithName:@"userDidJoinChatroom" body:@{@"userName":aUsername}];
}

/*!
 *  有用户离开聊天室
 *
 *  @param aChatroom    离开的聊天室
 *  @param aUsername    离开者
 */
- (void)userDidLeaveChatroom:(EMChatroom *)aChatroom user:(NSString *)aUsername {
   [self sendEventWithName:@"userDidLeaveChatroom" body:@{@"userName":aUsername}];
}

/*!
 *  被踢出聊天室
 *
 *  @param aChatroom    被踢出的聊天室
 *  @param aReason      被踢出聊天室的原因
 */

- (void)didDismissFromChatroom:(EMChatroom *)aChatroom reason:(EMChatroomBeKickedReason)aReason {
     [self sendEventWithName:@"didDismissFromChatroom" body:@{@"reason":@(aReason)}];
}

/*!
 *  有成员被加入禁言列表
 *
 *  @param aChatroom        聊天室
 *  @param aMutes    被禁言的成员
 *  @param aMuteExpire      禁言失效时间，暂时不可用
 */
- (void)chatroomMuteListDidUpdate:(EMChatroom *)aChatroom addedMutedMembers:(NSArray *)aMutes muteExpire:(NSInteger)aMuteExpire {
    
}

/*!
 *  有成员被移出禁言列表
 *
 *  @param aChatroom        聊天室
 *  @param aMutes    移出禁言列表的成员
 */
- (void)chatroomMuteListDidUpdate:(EMChatroom *)aChatroom removedMutedMembers:(NSArray *)aMutes  {
    
}

/*!
 *  有成员被加入管理员列表
 *
 *  @param aChatroom    聊天室
 *  @param aAdmin       加入管理员列表的成员
 */
- (void)chatroomAdminListDidUpdate:(EMChatroom *)aChatroom addedAdmin:(NSString *)aAdmin  {
    
}

/*!
 *  有成员被移出管理员列表
 *
 *  @param aChatroom    聊天室
 *  @param aAdmin       移出管理员列表的成员
 */
- (void)chatroomAdminListDidUpdate:(EMChatroom *)aChatroom removedAdmin:(NSString *)aAdmin  {
    
}

/*!
 *  聊天室创建者有更新
 *
 *  @param aChatroom    聊天室
 *  @param aNewOwner    新群主
 *  @param aOldOwner    旧群主
 */
- (void)chatroomOwnerDidUpdate:(EMChatroom *)aChatroom newOwner:(NSString *)aNewOwner oldOwner:(NSString *)aOldOwner  {
    
}



/*!
 *  发起实时会话
 *
 *  @param aType            通话类型
 *  @param aRemoteName      被呼叫的用户（不能与自己通话）
 *  @param aExt             通话扩展信息，会传给被呼叫方

 */
RCT_EXPORT_METHOD(startCall:(NSString *)type remoteName:(NSString *)remoteName ext:(NSString *)ext resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
    EMCallType callType = EMCallTypeVoice;
    if ([type isEqualToString:@"voice"]) {
        callType = EMCallTypeVoice;
    } else if ([type isEqualToString:@"video"]) {
        callType = EMCallTypeVideo;
    }
    [[EMClient sharedClient].callManager startCall:callType remoteName:remoteName ext:ext completion:^(EMCallSession *aCallSession, EMError *aError) {
        if (!aError) {
            [[CallSession sharedSession] setCallSession:aCallSession];
            resolve(@{@"code":@"success",@"callId":aCallSession.callId});
        } else {
            resolve(@{@"code":@(aError.code),@"description":aError.errorDescription});
        }
    }];
 
}

/*!
 *  接收方同意通话请求
 *
 *  @param  aCallId     通话ID
 *
 *  @result 错误信息
 */
RCT_EXPORT_METHOD(answerIncomingCall:(NSString *)aCallId resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
    EMError *error = [[EMClient sharedClient].callManager answerIncomingCall:aCallId];
    if (!error) {
        resolve(@{@"code":@"success"});
    } else {
        resolve(@{@"code":@(error.code),@"description":error.errorDescription});
    }
}


/*!
 *  结束通话
 *
 *  @param aCallId     通话的ID
 *  @param aReason     结束原因
 *
 *  @result 错误
 */
RCT_EXPORT_METHOD(endCall:(NSString *)aCallId reason:(int)aReason resolve:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
    EMError *error = [[EMClient sharedClient].callManager endCall:aCallId reason:aReason];
    if (!error) {
        resolve(@{@"code":@"success"});
    } else {
        resolve(@{@"code":@(error.code),@"description":error.errorDescription});
    }
}

/*!
 *  得到通话状态
 *
 */
RCT_EXPORT_METHOD(getCallSessionStatus:(RCTPromiseResolveBlock)resolve rejecte:(RCTPromiseRejectBlock)reject) {
    if ([[CallSession sharedSession] getCallSession]) {
        resolve(@([[CallSession sharedSession] getCallSession].status));
    } else {
        resolve(@(-1));
    }
}

/*!
 *  强制结束所有通话
 *  使用场景：做了某些错误操作造成Call UI已经消失但是没有释放掉EMCallManager中维护的EMCallSession,造成再次调用方法[IEMCallManager startCall:remoteName:ext:completion:]返回错误EMErrorCallBusy，如果这时无法调用方法[IEMCallManager endCall:reason:]，可以调用该方法
 */
RCT_EXPORT_METHOD(forceEndAllCall) {
    [[EMClient sharedClient].callManager forceEndAllCall];
}

/*!
 *  设置使用前置摄像头还是后置摄像头,默认使用前置摄像头
 *
 *  @param  aIsFrontCamera    是否使用前置摄像头, YES使用前置, NO使用后置
 */
RCT_EXPORT_METHOD(switchCameraPosition:(BOOL)aIsFrontCamera) {
    [[[CallSession sharedSession] getCallSession] switchCameraPosition:aIsFrontCamera];
}

/*!
 *  设置是否为扬声器
 *
 *  @param  isSpeaker    是否为扬声器
 */
RCT_EXPORT_METHOD(setSpeaker:(BOOL)isSpeaker) {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    if (isSpeaker) {
        //        [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
        //        [audioSession setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
    } else {
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil ];
    }
    [audioSession setActive:YES error:nil];
}

/*!
 *  屏幕常亮
 *
 *  @param  disabled    是否常亮
 */
RCT_EXPORT_METHOD(setIdleTimerDisabled:(BOOL)disabled) {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (disabled && ![UIApplication sharedApplication].isIdleTimerDisabled) {
            [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        } else if (!disabled && [UIApplication sharedApplication].isIdleTimerDisabled) {
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
        }
    });
}


/*!
 *  用户A拨打用户B，用户B会收到这个回调
 *
 *  @param aSession  会话实例
 */
- (void)callDidReceive:(EMCallSession *)aSession {
    NSString *type = aSession.type == EMCallTypeVoice ? @"voice":@"video";
    NSDictionary *dic = @{@"callId":aSession.callId,@"type":type,@"ext":aSession.ext};
    [[CallSession sharedSession] setCallSession:aSession];
    [self sendEventWithName:@"callDidReceive" body:dic];
}

/*!
 *  通话通道建立完成，用户A和用户B都会收到这个回调
 *
 *  @param aSession  会话实例
 */
- (void)callDidConnect:(EMCallSession *)aSession {
    NSDictionary *dic = @{@"callId":aSession.callId,@"isCaller":@(aSession.isCaller),@"ext":aSession.ext};
    [[CallSession sharedSession] setCallSession:aSession];
    [self sendEventWithName:@"callDidConnect" body:dic];
}

/*!
 *  用户B同意用户A拨打的通话后，用户A会收到这个回调
 *
 *  @param aSession  会话实例
 */
- (void)callDidAccept:(EMCallSession *)aSession {
    NSDictionary *dic = @{@"callId":aSession.callId,@"isCaller":@(aSession.isCaller),@"ext":aSession.ext};
    [[CallSession sharedSession] setCallSession:aSession];
    [self sendEventWithName:@"callDidAccept" body:dic];
}

/*!
 *  1. 用户A或用户B结束通话后，对方会收到该回调
 *  2. 通话出现错误，双方都会收到该回调
 *
 *  @param aSession  会话实例
 *  @param aReason   结束原因
 *  @param aError    错误
 */
- (void)callDidEnd:(EMCallSession *)aSession reason:(EMCallEndReason)aReason error:(EMError *)aError {
    NSDictionary *dic = nil;
    if (aError) {
        dic = @{@"code":@(aError.code),@"description":aError.errorDescription};
    } else {
        NSString *type = aSession.type == EMCallTypeVoice ? @"voice":@"video";
        dic = @{@"code":@"success",@"callId":aSession.callId,@"type":type,@"isCaller":@(aSession.isCaller),@"reason":@(aReason),@"ext":aSession.ext};
    }
    [[CallSession sharedSession] setCallSession:aSession];
    [self sendEventWithName:@"callDidEnd" body:dic];
}

/*!
 *  用户A和用户B正在通话中，用户A中断或者继续数据流传输时，用户B会收到该回调
 *
 *  @param aSession  会话实例
 *  @param aType     改变类型
 */
- (void)callStateDidChange:(EMCallSession *)aSession type:(EMCallStreamingStatus)aType {
    NSDictionary *dic = @{@"callId":aSession.callId,@"isCaller":@(aSession.isCaller),@"type":@(aType),@"ext":aSession.ext};
    [self sendEventWithName:@"callStateDidChange" body:dic];
}
/*!
 *  \~chinese
 *  当前登录账号在其它设备登录时会接收到此回调
 */
-(void)userAccountDidLoginFromOtherDevice {
    NSDictionary *dic = @{};
    [self sendEventWithName:@"userAccountDidLoginFromOtherDevice" body:dic];
}

@end
