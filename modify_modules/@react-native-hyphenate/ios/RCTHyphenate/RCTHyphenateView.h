//
//  HyphenateView.h
//  hyphenate
//
//  Created by jiasong on 2018/11/8.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <React/RCTView.h>
#import <Hyphenate/Hyphenate.h>

@interface RCTHyphenateView :RCTView


-(void)setLocalView:(BOOL)isLocal;

@end
